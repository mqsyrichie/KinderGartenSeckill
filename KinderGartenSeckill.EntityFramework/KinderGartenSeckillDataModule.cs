﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using KinderGartenSeckill.EntityFramework;

namespace KinderGartenSeckill
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(KinderGartenSeckillCoreModule))]
    public class KinderGartenSeckillDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<KinderGartenSeckillDbContext>(null);
        }
    }
}
