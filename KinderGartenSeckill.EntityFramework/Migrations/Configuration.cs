using System.Data.Entity.Migrations;

namespace KinderGartenSeckill.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<KinderGartenSeckill.EntityFramework.KinderGartenSeckillDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "KinderGartenSeckill";
        }

        protected override void Seed(KinderGartenSeckill.EntityFramework.KinderGartenSeckillDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...
        }
    }
}
