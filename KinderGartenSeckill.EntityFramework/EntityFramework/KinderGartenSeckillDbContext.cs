﻿using Abp.EntityFramework;
using KinderGartenSeckill.Model;
using System.Data.Entity;

namespace KinderGartenSeckill.EntityFramework
{
    public class KinderGartenSeckillDbContext : AbpDbContext
    {
        //TODO: Define an IDbSet for each Entity...

        //Example:
        //public virtual IDbSet<User> Users { get; set; }
        public virtual IDbSet<bm_order> Orders { get; set; }

        public virtual IDbSet<bm_user> Users { get; set; }

        public virtual IDbSet<bm_child> Childs { get; set; }

        public virtual IDbSet<bm_child_parents> Child_BBMMs { get; set; }

        public virtual IDbSet<bm_user_child_mapping> Bm_User_Child_Mappings { get; set; }
        public virtual IDbSet<CityAndDistrict> CityAndDistricts { get; set; }

        public virtual IDbSet<bm_kindergarten> Kindergartens { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */



        public KinderGartenSeckillDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in KinderGartenSeckillDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of KinderGartenSeckillDbContext since ABP automatically handles it.
         */
        public KinderGartenSeckillDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
    }
}
