﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace KinderGartenSeckill.EntityFramework.Repositories
{
    public abstract class KinderGartenSeckillRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<KinderGartenSeckillDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected KinderGartenSeckillRepositoryBase(IDbContextProvider<KinderGartenSeckillDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class KinderGartenSeckillRepositoryBase<TEntity> : KinderGartenSeckillRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected KinderGartenSeckillRepositoryBase(IDbContextProvider<KinderGartenSeckillDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
