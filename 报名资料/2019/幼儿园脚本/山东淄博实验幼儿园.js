javascript: (function (c) { function e(a) { var b = a.document, f = a.jQuery, g = function (a) { h.forEach(function (b) { a["on" + b] = null; f && f(a).unbind(b); try { /frame/i.test(a.tagName) && e(a.contentWindow) } catch (c) { } }) };[a, b].forEach(g); a = 0; for (var b = b.all, c = b.length; a < c; a++) { var d = b[a]; d && 1 === d.nodeType && g(d) } } var h = "contextmenu dragstart mouseup copy beforecopy selectstart select keydown keyup keypress".split(" "); e(c); $("[onpaste='return false']").removeAttr("onpaste"); $("body").unbind("paste"); })(window);
$.fn.hasAttr = function (e) { return $(this).attr(e) != undefined; };

//姓名
$('#name').val("王冠宇");
//身份证号
$('#apply_infor #cardNo').val("370303201604057914");
//出生日期
$('#apply_infor #age').val('2016-04-05');
//幼儿户籍地址
$('#apply_infor #natives').val('淄博市张店区人民西路164号凯瑞景园27-1-702室');
//幼儿现居住地址
$('#apply_infor #address').val('淄博市张店区人民西路164号凯瑞景园27-1-702室');
//户主姓名
$('#apply_infor #hzxm').val('王蒙');
//户主关系
$('#apply_infor #relationship').val('父子');
//备注
$('#mark').val('无');


if(window.addrow){
	if($('#fsub select').length==0)
		window.addrow("cardno");
	window.addrow("cardno");
}

var trs=$('#fsub tr');
var bbTr=trs.eq(trs.length-2);
var mmTr=$('#fsub tr:last');
if(bbTr.length>0){
	var selbb=bbTr.find("select");
	selbb.find("option").each(function(i,n){
		if($(n).text().indexOf("爸爸")>-1){
			selbb.val($(n).val());
			return false;
		}
	});
	
	var txt=bbTr.find("input");
	txt.eq(0).val('王蒙');
	txt.eq(1).val('青岛动车段');
	txt.eq(2).val('15314270923');
	txt.eq(3).val('是');
	txt.eq(4).val('370102198109232533');
}

if(mmTr.length>0){
	selbb=mmTr.find("select");
	selbb.find("option").each(function(i,n){
		if($(n).text().indexOf("妈妈")>-1){
			selbb.val($(n).val());
			return false;
		}
	});
	

	var mmTxt=mmTr.find("input");
	mmTxt.eq(0).val('刘辉');
	mmTxt.eq(1).val('大众公司');
	mmTxt.eq(2).val('13583362622');
	mmTxt.eq(3).val('是');
	mmTxt.eq(4).val('370304198005276228');
}

//亲子教育
$('#historySchool').val('有');
//有哪方面特长
$('#historyStudy').val('无');
//有何药物过敏或者过敏原
$('#historyGm').val('无');
//有何特殊情况与要求
$('#yq').val('无');
//是否有出生证
$('#birth').val('有');
//是否有预防接种证
$('#yfjz').val('有');
//患过何种传染病
$('#crb').val('无');
//家庭是否有房产证
$('#room').val('有');
