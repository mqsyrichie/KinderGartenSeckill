//解除页面禁止F12/右键/粘贴等限制
javascript: (function (c) { function e(a) { var b = a.document, f = a.jQuery, g = function (a) { h.forEach(function (b) { a["on" + b] = null; f && f(a).unbind(b); try { /frame/i.test(a.tagName) && e(a.contentWindow) } catch (c) { } }) };[a, b].forEach(g); a = 0; for (var b = b.all, c = b.length; a < c; a++) { var d = b[a]; d && 1 === d.nodeType && g(d) } } var h = "contextmenu dragstart mouseup copy beforecopy selectstart select keydown keyup keypress".split(" "); e(c); $("[onpaste='return false']").removeAttr("onpaste"); $("body").unbind("paste"); })(window);
//姓名
try { $("label:contains('孩子姓名')").next("div").find("input[type=text]").eq(0).val("张澜馨").blur(); } catch (err) { }
try { $("label:contains('幼儿姓名')").next("div").find("input[type=text]").eq(0).val("张澜馨").blur(); } catch (err) { }
try { $("div:contains('孩子姓名')").next("div").find("input[type=text]").eq(0).val("张澜馨").blur(); } catch (err) { }
try { $("div:contains('幼儿姓名')").next("div").find("input[type=text]").eq(0).val("张澜馨").blur(); } catch (err) { }
//身份证号
try {
    var idCard = $('#idcard');
    if (idCard.length > 0) {
        idCard.val('120106201605080122').blur();
    }
} catch (err) { }
//申报幼儿园
try { $("div.tc_list_con ul li").find(":contains('红桥区第一幼儿园(五爱道园区)')").click(); } catch (err) { }
//街道
try { $("div.td_select_tc").find(":contains('丁字沽')").click(); } catch (err) { }
//地址
try {
    $.each($('input'), function (i, n) {
        var $t = $(this);
        var ph = $t.attr('placeholder');
        if (ph != null && ph != undefined && $t.attr("placeholder").indexOf('户口本') > -1 && $(n).attr("placeholder").indexOf('地址') > -1) {
            $t.val('丁字沽一号路风光里28门704号');
            return false;
        }
    });
} catch (err) { }
//try { $("div.td_select_tc").find(":contains('红桥')").click(); } catch (err) { }

var ele = ["td", "th", "label", "li", "ul","div","span","a"];

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('儿童姓名'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿姓名'):last");
        if (a.length == 0) a = $(n + ":contains('姓名'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("叶语桐").blur();
}
catch (e) { }


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('幼儿生日'):last");
        if (a.length == 0) a = $(n + ":contains('儿童生日'):last");
        if (a.length == 0) a = $(n + ":contains('孩子生日'):last");
        if (a.length == 0) a = $(n + ":contains('出生日期'):last");
        if (a.length == 0) a = $(n + ":contains('生日'):last");
        if (a.length > 0) return false;
    });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) { txt.eq(0).val("2016-05-30"); }
}
catch (e) { }


try {
    var mz = "男";
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('幼儿性别'):last");
        if (a.length == 0) a = $(n + ":contains('儿童性别'):last");
        if (a.length == 0) a = $(n + ":contains('孩子性别'):last");
        if (a.length == 0) a = $(n + ":contains('性别'):last");
        if (a.length > 0) return false;
    })

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");

    if (txt.length > 0) {
        txt.eq(0).val(mz).blur();
    }
    else {
        //select
        var sel = a.find("select");
        if (sel.length == 0) sel = a.nextAll().find("select");
        if (sel.length == 0) sel = parent.find("select");
        if (sel.length == 0) sel = parent.nextAll().find("select");
        if (sel.length >= 0) {
            sel.find("option").each(function (i, n) {
                var $t = $(this);
                if ($t.text().indexOf(mz) > -1) {
                    $t.attr("selected", "selected");
                    return false;
                }
            })
        }
    }
}
catch (e) { }
 

try {
    var mz = "汉";
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('民族'):last");
        if (a.length == 0) a = $(n + ":contains('民族'):last");
        if (a.length > 0) return false;
    })

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");

    if (txt.length > 0) {
        txt.eq(0).val(mz).blur();
    }
    else {
        //select
        var sel = a.find("select");
        if (sel.length == 0) sel = a.nextAll().find("select");
        if (sel.length == 0) sel = parent.find("select");
        if (sel.length == 0) sel = parent.nextAll().find("select");
        if (sel.length >= 0) {
            sel.find("option").each(function (i, n) {
                var $t = $(this);
                if ($t.text().indexOf(mz) > -1) {
                    $t.attr("selected", "selected");
                    return false;
                }
            })
        }
    }
}
catch (e) { }


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('居住住址'):last");
        if (a.length == 0) a = $(n + ":contains('住址'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");

    if (txt.length > 0) {
        txt.eq(0).val("天津市河东区大桥道萦东温泉花园19号楼5门502").blur();
    }
}
catch (e) { }


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('第几胎'):last");
        if (a.length == 0) a = $(n + ":contains('几胎'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();

    var txt = a.find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");

    if (txt.length > 0) { txt.eq(0).val("1"); }
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('户主姓名'):last");
        if (a.length == 0) a = $(n + ":contains('户主'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("叶震").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('与户主关系'):last");
        if (a.length == 0) a = $(n + ":contains('户主关系'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("祖孙").blur();
    else {
        txt = parent.find("select");
        if (txt.length == 0) txt = parent.nextAll().find("select");
        if (txt.length > 0) {
            txt.find("option").each(function (i, e) {
                var text = $(this).text();
                if (text.indexOf("祖孙") >= 0) {
                    $(this).attr("selected", "selected");
                    return false;
                }
            });
        }
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('出生地址'):last");
        if (a.length == 0) a = $(n + ":contains('出生地'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("天津").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('国籍'):last");
        if (a.length == 0) a = $(n + ":contains('国家'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("中国").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('乳名'):last");
        if (a.length == 0) a = $(n + ":contains('昵称'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("嘟嘟").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('体重'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=number]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("13.2").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('身高'):last");
        if (a.length == 0) a = $(n + ":contains('身高'):last");
        if (a.length > 0) return false;
    })

    var parent = a.parent();
    var txt = a.find("input[type=number]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("94").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人姓名'):last");
        if (a.length == 0) a = $(n + ":contains('照看人姓名'):last");
        if (a.length == 0) a = $(n + ":contains('照看人'):last");
        if (a.length == 0) a = $(n + ":contains('主要看护人'):last");
        if (a.length == 0) a = $(n + ":contains('看护人'):last");
        if (a.length == 0) a = $(n + ":contains('照看人'):last");
        if (a.length == 0) a = $(n + ":contains('照顾人'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("叶震").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人手机号'):last");
        if (a.length == 0) a = $(n + ":contains('照看人手机号'):last");
        if (a.length == 0) a = $(n + ":contains('手机号'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("13920301667").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人身份证'):last");
        if (a.length == 0) a = $(n + ":contains('照看人身份证'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("120106198706225015").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('邮箱'):last");
        if (a.length > 0) return false;
    })
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("272205299@qq.com").blur();
}
catch (e) { }


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲姓名'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸姓名'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸名字'):last");
        if (a.length == 0) a = $(n + ":contains('父亲名字'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("叶震");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲年龄'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("32");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲学历'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("中专");
    }
} catch (e) { }


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲身份证'):last");
        if (a.length == 0) a = $(n + ":contains('父亲身份'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份证'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("120106198706225015");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲电话'):last");
        if (a.length == 0) a = $(n + ":contains('父亲手机号'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸电话'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸手机号'):last");
        if (a.length > 0) return false;
    });


    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("13920301667");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲姓名'):last");
        if (a.length == 0) a = $(n + ":contains('母亲名字'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈姓名'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈名字'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("叶震");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲年龄'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=number]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=number]");

    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");

    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("32");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲学历'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("中专");
    }
} catch (e) { }


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲身份证'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈身份证'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("120106198706225015");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲电话'):last");
        if (a.length == 0) a = $(n + ":contains('母亲手机号'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈电话'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈手机号'):last");

        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("13920301667");
    }
} catch (e) { }


//针对红桥的下拉框
var selEle = "";
$.each(ele, function (i, n) {
    if ($(n + ".td_select_tc").length > 0) {
        selEle = n;
        return false;
    }
});

$(selEle + ".td_select_tc").each(function (i, n) {
    $(n).find("a").each(function () {
        var $t = $(this);
        if ($t.text().indexOf("男") >= 0 || $t.text().indexOf("汉") >= 0 || $t.text().indexOf("否") >= 0
            || $t.text().indexOf("无") >= 0 || $t.text().indexOf("不是") >= 0) {
            $t.click();
            return false;
        }
    })
});


