var REGISTER={
	doSubmit:function() {
		$.post(ctx+"/user/regist",$.mySerialize($("#regForm")), function(data){
			if(data.status == '200'){
				REGISTER.login();
			} else if(data.status == '-2011'){
				//身份证已登记
				$.reviseRegi();
				//alert(data.errorMsg);
			} else if(data.status == '500'){
				REGISTER.loginFailed();
			} else {
				alert(data.errorMsg);
			}
		});
	},
	login:function() {
		 location.href = ctx+"/reg/success";
		 return false;
	},
	loginFailed:function() {
		 location.href = ctx+"/reg/failed";
		 return false;
	}
};

var LOGIN={
	login:function() {
		if (!$(this).hasClass("dis")&&!$(this).hasClass("btloading")) {
			$("#login").addClass("btloading");
		}

		$.post(ctx+"/user/login",$.mySerialize($("#loginForm")), function(data){
			$("#login").removeClass("btloading");
			if(data.status == '200'){
				LOGIN.loginSuccess();
			} else {
				var msg = data.errorMsg.substr(0,3);
				if(msg!="验证码"){
					$("#valCode").attr("src",ctx+"/loginValidateCode?rnd=" + Math.random());
				}
				alert(data.errorMsg);
			}
		});
	},
	loginSuccess:function() {
		 location.href = ctx+"/enroll/enrollBegin";
		 return false;
	}
};

var USR={
	updateUser:function() {
		$.post(ctx+"/enroll/updateUser",$.mySerialize($("#upUserForm")), function(data){
			if(data.status == '200'){
				$.reviseInfo(USR.updateUserSuccess);
			} else {
				alert(data.errorMsg);
			}
		});
	},
	updateUserSuccess:function() {
		 location.href = ctx+"/enroll/enrollBegin";
		 return false;
	}
};

//全局的ajax访问，处理ajax清求时sesion超时 
$.ajaxSetup({
    contentType : "application/x-www-form-urlencoded;charset=utf-8",
    complete : function(XMLHttpRequest, textStatus) {
        var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus"); // 通过XMLHttpRequest取得响应头，sessionstatus，
        if (sessionstatus == "timeout") {
            // 如果超时就处理 ，指定要跳转的页面
        	var url = XMLHttpRequest.getResponseHeader("url");
        	window.location.href = ctx+url;
        }
    }
});