﻿//用户登录
function Login() {
    var txtname = $("#txtname").val();
    var txtpwd = $("#txtpwd").val();
    if (txtname == "") {
        //alert("请输入您的账户！");
        swal("登录失败", "请输入您的账户！", "warning");
        return false;
    }
    if (txtpwd == "") {
        //alert("请输入密码！");
        swal("登录失败", "请输入密码！", "warning");
        return;
    }
    var ckeval = "0";
    if ($("input[name=ckeval]").attr("checked")) {
        ckeval = "1";
    }
    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "text",
        data: { action: "Login", username: escape(txtname), userpwd: escape(txtpwd), ckeval: ckeval },
        success: function (ReturnData) {
            if (ReturnData == "yes") {
                if (getUrlPara("rurl") == null) {
                    window.location.href = "/Member/UserIndex.aspx";
                }
                else {
                    window.location.href = unescape(getUrlPara("rurl"));
                }
            }
            //else if (ReturnData == "findpassword") {
            //    swal({ title: "登录失败", text: "新网站登录，请先找回密码后再登录！", type: "warning" }, function () { window.location.href = "/ForgotPwd.html"; });
            //    alert("登录失败，新网站登录，请先找回密码后再登录！");

            //}
            else {
                swal("登录失败", ReturnData, "warning");
                //alert(ReturnData);
            }
        }
    });
}


//发送验证码
function SendRegCode(obj) {
    var txtMobile = $("#txtmobile").val();
    var regnum = /^\d{11}$/;
    if (txtMobile.length == 0 || txtMobile == '' || !regnum.test(txtMobile)) {
        swal("发送失败", "请填写正确的手机号码！", "warning");
        return;
    }



    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "xml",
        data: { action: "SendRegCode", txtMobile: escape(txtMobile) },
        success: function (ReturnData) {
            var state = $("response state", ReturnData).text();
            if (state == "yes") {
                settime(obj);
            }
            else {
                swal("发送失败", $("response errorinfo", ReturnData).text(), "warning");
            }
        }
    });
}

//发送验证码
function SendCode(obj) {

    var txtMobile = $("#txtmobile").val();
    var regnum = /^\d{11}$/;
    if (txtMobile.length == 0 || txtMobile == '' || !regnum.test(txtMobile)) {
        swal("发送失败", "请填写正确的手机号码！", "warning");
        return;
    }

    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "xml",
        data: { action: "SendCode", txtmobile: escape(txtMobile) },
        success: function (ReturnData) {
            var state = $("response state", ReturnData).text();
            if (state == "yes") {

                settime(obj);
            }
            else {
                swal("发送失败", $("response errorinfo", ReturnData).text(), "warning");
            }
        }
    });
}
var wait = 60;
function settime(obj) {
    if (wait == 0) {
        obj.removeAttribute("disabled");
        obj.value = "获取验证码";
        wait = 60;
    } else {
        obj.setAttribute("disabled", true);
        obj.value = "获取验证码(" + wait + ")";
        wait--;
        setTimeout(function () {
            settime(obj);
        }, 1000)
    }
}
//找回密码
function ForgotPwd() {

    var txtmobile = $("#txtmobile").val();
    var txtmobilecode = $("#txtmobilecode").val();

    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "xml",
        data: { action: "ForgetPwdForEmail", txtmobile: escape(txtmobile), txtmobilecode: escape(txtmobilecode) },
        success: function (ReturnData) {
            var state = $("response state", ReturnData).text();
            if (state == "yes") {
                swal({ title: "找回成功", text: "请查看您的邮箱获取您的新密码，然后重新登录修改您的密码", type: "success" }, function () { window.location.href = "/Login.aspx"; });
            }
            else {
                swal("找回失败", $("response errorinfo", ReturnData).text(), "warning");
            }
        }
    });
}
//获取URL参数
function getUrlPara(paraName) {
    var sUrl = location.href;
    var sReg = "(?:\\?|&){1}" + paraName + "=([^&]*)"
    var re = new RegExp(sReg, "gi");
    if (re.exec(sUrl)) {
        return RegExp.$1.split('#')[0];
    }
    return null;
}
//用户信息修改
function EditUserInfo() {
    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "text",
        data: $("#form1").serialize(),
        success: function (ReturnData) {
            if (ReturnData == "yes") {
                swal("修改成功", "", "success");
            }
            else {
                swal("修改失败", ReturnData, "warning");
            }
        }
    });
}
//密码修改
function ModifyPwd() {
    var txtOldPwd = $("#txtOldPwd").val();
    var txtNewPwd = $("#txtNewPwd").val();
    var txtNewRePwd = $("#txtNewRePwd").val();

    if (txtNewPwd != txtNewRePwd) {
        swal("修改失败", "确认新密码不一致，请重新输入!", "warning");
        return;
    }

    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "text",
        data: { action: "ModifyPwd", txtOldpwd: escape(txtOldPwd), txtNewPwd: escape(txtNewPwd) },
        success: function (ReturnData) {
            if (ReturnData == "yes") {
                swal("修改成功", "", "success");
                $("#txtOldPwd").val("");
                $("#txtNewPwd").val("");
                $("#txtNewRePwd").val("");
            }
            else {
                swal("修改失败", ReturnData, "warning");
            }
        }
    });
}
//提交地址
function SaveAddress() {
    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "text",
        data: $("#aspnetForm").serialize(),
        success: function (ReturnData) {
            if (ReturnData == "yes") {
                if (getUrlPara("rurl") == null) {

                    swal({ title: "保存成功", text: "", type: "success" }, function () { window.location.href = "/Member/AddressList.aspx"; });
                }
                else {
                    window.location.href = unescape(getUrlPara("rurl"));
                }
            }
            else {
                swal("保存失败", ReturnData, "warning");
            }
        }
    });
}

//提交地址
function SaveAddressShow() {
    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "text",
        data: $("#aspnetForm").serialize(),
        success: function (ReturnData) {
            if (ReturnData == "yes") {
                window.parent.webcommon.closeFb('配送地址保存成功', 1, 2000, true);
            }
            else {
                alert(ReturnData)
            }
        }
    });
}

//用户签到
function AddUserSign(pid) {

    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "text",
        data: { action: "AddUserSign" },
        success: function (ReturnData) {
            if (ReturnData == "yes") {
                swal({ title: "签到成功", text: "签到成功！", type: "success" }, function () { location.reload(); });
                //swal("签到成功", "签到成功", "success");

            }
            else if (ReturnData == "already") {
                //swal("加入收藏夹失败", "该商品已经加入收藏夹", "warning");
                swal("签到失败", "您今天已经签到过了", "warning");
            }
            else {
                if (ReturnData == "no") {
                    swal({ title: "签到失败", text: "请先登录您的账号！", type: "warning" }, function () { window.location.href = "/Login.aspx?rurl=" + escape(window.location.href); });
                }
                else {
                    swal("签到失败", ReturnData, "warning");
                }
            }
        }
    });
}


//找回密码
function AddJoin() {

    var userCode = $("#txtuserCode").val();
    var txtmobilecode = $("#txtmobilecode").val();

    $.ajax({
        url: "/IAjax/Ajax.ashx",
        type: "POST",
        cache: false,
        dataType: "xml",
        data: { action: "AddJoin", userCode: escape(userCode), txtmobilecode: escape(txtmobilecode) },
        success: function (ReturnData) {
            var state = $("response state", ReturnData).text();
            if (state == "yes") {

                location.href = 'success.aspx';
            }
            else {
                swal("报名失败", $("response errorinfo", ReturnData).text(), "warning");
            }
        }
    });
}
