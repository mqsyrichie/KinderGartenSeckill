﻿//bootrap wizard set
$('#rootwizard').bootstrapWizard({
    'tabClass': 'bwizard-steps',
    'onTabShow': function (tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;
        var $percent = ($current / $total) * 100;
        $('#rootwizard .progress-bar').css({ width: $percent + '%' });
    },
    'onTabClick': function (tab, navigation, index, clickedIndex) {
        //注意验证的方式
        //如果不允许点击，则直接返回false
        //注意clickedIndex与index的使用,点击下一节点进行验证
        if (clickedIndex > index) {
            var $valid = $("#loginwizard").valid();
            if (!$valid) {
                return false;
            }
        }
    },
    'onNext': function (tab, navigation, index) {
        //注意验证的方式,$validator.focusInvalid()方式可做备选
        var $valid = $("#loginwizard").valid();
        if (!$valid) {
            //$validator.focusInvalid();
            return false;
        }
    },
});
//注意$(document).ready的处理时机
//$(document).ready(function () {
//    //备用验证方式，采用下面脚本的方式快捷
//    //var $validator = $("#loginwizard").validate({
//    //    rules: {
//    //        city: {
//    //            required: true,
//    //        },
//    //        nation: {
//    //            required: true,
//    //        }
//    //    }
//    //});
//    $('#rootwizard').bootstrapWizard({
//        'tabClass': 'bwizard-steps',
//        'onTabShow': function (tab, navigation, index) {
//            var $total = navigation.find('li').length;
//            var $current = index + 1;
//            var $percent = ($current / $total) * 100;
//            $('#rootwizard .progress-bar').css({ width: $percent + '%' });
//        },
//        'onTabClick': function (tab, navigation, index, clickedIndex) {
//            //注意验证的方式
//            //如果不允许点击，则直接返回false
//            //注意clickedIndex与index的使用,点击下一节点进行验证
//            if (clickedIndex > index) {
//                var $valid = $("#loginwizard").valid();
//                if (!$valid) {
//                    return false;
//                }
//            }
//        },
//        'onNext': function (tab, navigation, index) {
//            //注意验证的方式,$validator.focusInvalid()方式可做备选
//            var $valid = $("#loginwizard").valid();
//            if (!$valid) {
//                //$validator.focusInvalid();
//                return false;
//            }
//        },
//    });
//});

//active当前页面
document.getElementById("Loginfirst").className = "active";
//@*图片验证码脚本绑定*@
//var tc = new TouClick($('#captcha')[0], {
//            onSuccess: function (obj) {
//                $('#token').val(obj.token);
//                $('#checkAddress').val(obj.checkAddress);
//                $('#sid').val(obj.sid);
//            }
//        });
$(document).ready(function () {

    checkraidostatus();

    $('#isdiseasesradio').on('ifChanged', function (event) {

        if ($('#isdiseasesradio').is(':checked')) {
            $('#divdiseasesname').show();

        }
        else {
            $('#diseasesname').val('');
            $('#divdiseasesname').hide();
        }
    });
    $('#isallergicradio').on('ifChanged', function (event) {

        if ($('#isallergicradio').is(':checked')) {
            $('#divallergicname').show();

        }
        else {
            $('#allergicname').val('');
            $('#divallergicname').hide();
        }
    });
    $('#isdisabilityradio').on('ifChanged', function (event) {

        if ($('#isdisabilityradio').is(':checked')) {
            $('#divdisabilitytype').show();

        }
        else {
            $('#disabilitytype').combobox('clear');
            $('#divdisabilitytype').hide();
        }
    });


});
function checkraidostatus() {
    if ($('#isdiseasesradio').is(':checked')) {
        $('#divdiseasesname').show();       
    }
    else {
        $('#diseasesname').val('');
        $('#divdiseasesname').hide();       

    }

    if ($('#isallergicradio').is(':checked')) {
        $('#divallergicname').show();

    }
    else {
        $('#allergicname').val('');
        $('#divallergicname').hide();
    }

    if ($('#isdisabilityradio').is(':checked')) {
        $('#divdisabilitytype').show();

    }
    else {
        $('#disabilitytype').combobox('clear');
        $('#divdisabilitytype').hide();
    }
}

//普通验证码
function reloadcode() {
    var verify = document.getElementById('imgValidateCode');
    verify.setAttribute('src', 'CreateImageCode?' + Math.random());
}
//具体园所,此加载必须在幼儿园名称加载前，否则园所名称可能为空
$('#title').combobox({
    //url: '/titlemanager/loadtitlelist?kindergartenname=' + escape(kindergartenname),
    required: true,
    width: 280,
    height: 34,
    panelHeight: 100,
    editable: false,
    valueField: 'atitle',
    textField: 'atitle',
});
//model验证后若再次刷新保留园所名称，此语句不能写在 $('#kindergartenname').combobox函数里面
var titlename = $('#title').combobox('getText');
//具体幼儿园名称
$('#kindergartenname').combobox({
    url: '/titlemanager/loadkindergartenenrolllist',
    required: true,
    width: 280,
    height: 34,
    panelHeight: 160,
    editable: false,
    valueField: 'kindergartenname',
    textField: 'kindergartenname',
    onLoadSuccess: function () { //加载完成后,联动园所
        var kindergartenname = $('#kindergartenname').combobox('getText');
        if (kindergartenname != '') {
            var url = '/titlemanager/loadtitlelist?kindergartenname=' + escape(kindergartenname);//选择幼儿园，注意escape处理汉字编码，否则参数为乱码
            $('#title').combobox('reload', url);
            $('#title').combobox("select", titlename);
        }
    },
    //选择调整完成后,联动园所
    onSelect: function (rec) {
        var kindergartenname = rec.kindergartenname;
        var url = '/titlemanager/loadtitlelist?kindergartenname=' + escape(kindergartenname);//选择幼儿园，注意escape处理汉字编码，否则参数为乱码
        $('#title').combobox('clear');
        $('#title').combobox('reload', url);
    }
});


$('input').iCheck({
    handle: 'radio',
    radioClass: 'iradio_minimal-blue',
    increaseArea: '10%' // optional
});

//$('#isonechild').combobox({

//    required: true,
//    width: 280,
//    height: 34,
//    panelHeight: 80,
//    editable: false,
//    valueField: 'label',
//    textField: 'value',
//    data: [{
//        label: '是',
//        value: '是'
//    }, {
//        label: '否',
//        value: '否'
//    }]
//});
//$('#relationwithguardian').combobox({

//    required: true,
//    width: 280,
//    height: 34,
//    panelHeight: 80,
//    editable: false,
//    valueField: 'label',
//    textField: 'value',
//    data: [{
//        label: '父亲',
//        value: '父亲'
//    }, {
//        label: '母亲',
//        value: '母亲'
//    }, {
//        label: '其他',
//        value: '其他'
//    }]
//});

//$('#isallergic').combobox({

//    required: true,
//    width: 280,
//    height: 34,
//    panelHeight: 60,
//    editable: false,
//    valueField: 'label',
//    textField: 'value',
//    data: [{
//        label: '是',
//        value: '是'
//    }, {
//        label: '否',
//        value: '否'
//    }]
//});
$('#relationwithhousemaster').combobox({

    required: true,
    width: 280,
    height: 34,
    panelHeight: 160,
    editable: false,
    valueField: 'label',
    textField: 'value',
    data: [{
        label: '父亲',
        value: '父亲'
    }, {
        label: '母亲',
        value: '母亲'
    }, {
        label: '祖父',
        value: '祖父'
    }, {
        label: '祖母',
        value: '祖母'
    }, {
        label: '外祖父',
        value: '外祖父'
    }, {
        label: '外祖母',
        value: '外祖母'
    }]
});

$('#relationwithleaseholder').combobox({

    required: true,
    width: 280,
    height: 34,
    panelHeight: 160,
    editable: false,
    valueField: 'label',
    textField: 'value',
    data: [{
        label: '父亲',
        value: '父亲'
    }, {
        label: '母亲',
        value: '母亲'
    }, {
        label: '祖父',
        value: '祖父'
    }, {
        label: '祖母',
        value: '祖母'
    }, {
        label: '外祖父',
        value: '外祖父'
    }, {
        label: '外祖母',
        value: '外祖母'
    }]
});

//$('#isdisability').combobox({

//    required: true,
//    width: 280,
//    height: 34,
//    panelHeight: 60,
//    editable: false,
//    valueField: 'label',
//    textField: 'value',
//    data: [{
//        label: '是',
//        value: '是'
//    }, {
//        label: '否',
//        value: '否'
//    }]
//});

$('#disabilitytype').combobox({

    required: true,
    width: 280,
    height: 34,
    panelHeight: 160,
    editable: false,
    valueField: 'label',
    textField: 'value',
    data: [{
        label: '视力残疾',
        value: '视力残疾'
    }, {
        label: '听力残疾',
        value: '听力残疾'
    }, {
        label: '言语残疾',
        value: '言语残疾'
    }, {
        label: '肢体残疾',
        value: '肢体残疾'
    }, {
        label: '智力残疾',
        value: '智力残疾'
    }, {
        label: '精神残疾',
        value: '精神残疾'
    }, {
        label: '多重残疾',
        value: '多重残疾'
    }, {
        label: '其他残疾',
        value: '其他残疾'
    }]
});


//$('#health').combobox({
//    required: true,
//    width: 280,
//    height: 34,
//    panelHeight: 120,
//    editable: false,
//    valueField: 'label',
//    textField: 'value',
//    data: [
//        {
//            label: '健康或良好',
//            value: '健康或良好'
//        }, {
//            label: '一般或较弱',
//            value: '一般或较弱'
//        }, {
//            label: '有慢性病',
//            value: '有慢性病'
//        }, {
//            label: '残疾',
//            value: '残疾'
//        }]
//});
