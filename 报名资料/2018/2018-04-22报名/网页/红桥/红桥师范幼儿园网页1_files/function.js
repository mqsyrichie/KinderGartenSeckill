$(document).ready(function() {
	//input输入的交互
	$(".td_inp input").bind("input propertychange", function() {
		if ($(this).val() == "") {
			$(this).next().hide();
		} else {
			$(this).next().show();
		}
	});
	//input获取焦点和失去焦点交互
	$(".td_inp input").focus(function() {
		$(this).parent().addClass("on");
	}).blur(function() {
		$(this).parent().removeClass("on");
	});
	//input清空
	$(".td_inp a").click(function() {
		$(this).prev().val("");
		$(this).prev().focus();
		$(this).hide();
	});
	//下拉列表交互
	$.selectHide = function() {
		$(".td_select_tc").hide();
		$(".td_select ").removeClass("on");
		$(document).unbind("click", $.selectHide);
		$(".td_select").css("z-index", "1");
	};
	$(".td_select .td_select_jg").click(function(e) {
		e.stopPropagation()
		if ($(this).next().is(":hidden")) {
			$(".td_select").css("z-index", "1");
			$(this).parents(".td_select").css("z-index", "5");
			$(".td_select_tc").hide();
			$(".td_select").removeClass("on");
			$(this).parent().addClass("on");
			$(this).next().show();
			$(document).unbind("click", $.selectHide).bind("click", $.selectHide);
		} else {
			
			$(document).unbind("click", $.selectHide);
			$(this).parent().removeClass("on");
			$(this).next().hide();
			$(this).parents(".td_select").css("z-index", "1");
		}
	});
	$(".td_select").on("click", ".td_select_tc a,.td_select_tc li", function(e) {
		e.stopPropagation()
		if ($(this).parents(".td_select").hasClass("list") || $(this).parents(".td_select").hasClass("list_down")) {
			if (!$(this).hasClass("dis")) {
				$(this).parents(".td_select_tc").prev().find("span").text($(this).find("span").eq(0).text()).attr("data-v", $(this).attr("data-v"));
				$(this).parents(".td_select_tc").hide();
				$(this).parents(".td_select").removeClass("on").addClass("checked");
				$(document).unbind("click", $.selectHide);
			} else {
				alert("该园名额已满，请选择其他幼儿园")
			}
		} else {
			$(this).parents(".td_select").css("z-index", "1");
			$(this).parent().prev().find("span").text($(this).text()).attr("data-v", $(this).attr("data-v"));
			$(this).parent().hide();
			
			$(this).parents(".td_select").removeClass("on").addClass("checked");
			$(document).unbind("click", $.selectHide);
			if ($(this).text() == "其他" && $(this).parents(".td_select").next(".td_inp")) {
				$(this).parents(".td_select").next(".td_inp").removeClass("dis").find("input").removeAttr('disabled');
			} else if ($(this).text() == "红桥区" && $(this).parents(".td_select").next(".td_inp")) {
				$(this).parents(".td_select").next(".td_inp").addClass("dis").find("input").attr('disabled', true);
			}
		}
	});
	//下拉列表初始化滚动条
	$(".tc_list_con,.list_con").niceScroll({
		cursorcolor: "#0898df",
		autohidemode: false,
		cursorborder: "1px solid transparent",
		cursorwidth: "6px",
		railpadding: {
			right: 2
		}
	});
	$(".td-select-tc").niceScroll({
		cursorcolor: "#0898df",
		autohidemode: false,
		cursorborder: "1px solid transparent",
		cursorwidth: "6px",
		railpadding: {
			right: 2
		}
	});

	//验证生日
	$.checkBirthday = function(idCard) {
		var birthday = $("#datepicker").val();
		if (birthday && idCard.length == 18) {
			idCard = idCard.slice(6, 14);
			birthday = birthday.replace(/-/g, "");
			if (idCard === birthday) {
				return true
			} else {
				return false
			}
		} else {
			return false
		}
	};
	//关闭Tips
	$("body").on("click", ".sorry-btn", function() {
		$(".sorry").hide();
		$(".jz-zzc").hide();
	});
	//身份证已经登记过信息提示
	$.tipsShow = function() {
		$(".sorry").show();
		$(".jz-zzc").show();
	};
	//获取验证码倒计时
	// $.timeout = function() {
	// 	var reg = /^1(3|4|5|7|8)\d{9}$/;
	// 	if (reg.test($("#phoneNumber").val())) {
	// 		$(".td_yzm a").unbind("click", $.timeout);
	// 		$(".yzm_input").removeClass("dis").find("input").removeAttr("disabled");
	// 		var ins,
	// 			nub = 60;
	// 		$(".td_yzm").addClass("on");
	// 		ins = setInterval(function() {
	// 			nub = nub - 1;
	// 			$(".td_yzm a").text("重新发送（" + nub + "s）")
	// 			if (nub == 0) {
	// 				clearInterval(ins);
	// 				$(".td_yzm").removeClass("on");
	// 				$(".td_yzm a").text("点击获取验证码");
	// 				$(".td_yzm a").bind("click", $.timeout);
	// 			}
	// 		}, 1000)
	// 	} else {
	// 		alert("请输入正确的手机号")
	// 	}
	// };
	// $(".td_yzm a").bind("click", $.timeout);
	//招生简章交互
	$(".pop_right").hover(function() {
		$(this).find(".pop_list").show();
		$(".list_con").getNiceScroll().resize()
	}, function() {
		$(this).find(".pop_list").hide()
	});
	//修改密码成功
	$.revisePwd = function(fn) {
		var zzc = $('<div class="zzc"></div>');
		var tips = $('<div class="tips pwd"><em title="关闭"></em><p><a>确认并返回登录</a></p></div>');
		$("body").append(zzc, tips);
		if (fn && $.type(fn) === "function") {
			tips.find("a,em").bind("click", fn)
		}
	};
	//修改登记信息成功
	$.reviseInfo = function(fn) {
		var zzc = $('<div class="zzc"></div>');
		var tips = $('<div class="tips pwd"><em title="关闭"></em><p><a>确定</a></p></div>');
		$("body").append(zzc, tips);
		if (fn && $.type(fn) === "function") {
			tips.find("a,em").bind("click", fn)
		}
	};
	//报名信息折叠交互
	$(".con_tit em").click(function() {
		if ($(this).hasClass("on")) {
			$(this).removeClass("on");
			$(this).parent().next().show();
		} else {
			$(this).addClass("on");
			$(this).parent().next().hide();
		}
	});
	//表单非空验证
	$.checkForm = function() {
		var lock = true;
		$(".main-box .td_inp input,.main-box .td_select,.td_date input").each(function(index, el) {
			if(!$(this).hasClass("no_check")){
				if ($(this).hasClass("td_select")) {
					if (!$(this).hasClass("checked")) {
						var msg = $(this).attr("data-msg");
						alert("请选择" + msg);
						lock = false;
						return false;
					}
				} else {
					if ($(this).val() == "" && !$(this).hasClass("yzm_inp")) {
						var msg = $(this).attr("data-msg");
						alert("请填写" + $(this).attr("data-msg"));
						lock = false;
						return false;
					}
				}
			}
		});
		if (lock) {
			return true;
		} else {
			return false;
		}
	};
	//密码验证
	$.checkPassword = function() {
		var pwd = $("#pwd").val();
		var pwd_confirm = $("#pwd_confirm").val();
		if (pwd.length < 6) {
			alert("密码最少6位字母、数字组合。");
			return false;
		} else if (pwd.length > 18) {
			alert("密码最多18位字母、数字组合。");
			return false;
		}
		if (pwd_confirm && pwd !== pwd_confirm) {
			alert("确认密码与密码不一致，请重新输入确认密码。");
			return false;
		} else {
			return true;
		}
	};
	//身份证验证
	$.checkIdcard = function() {
		var idcard = $("#idcard").val();
		var reg = /^(\d{18}$|^\d{17}(\d|X|x))$/;
		var re = /\d{6}([12]\d{3})([01]\d)([0123]\d)/;
		var id = re.exec(idcard);
		var birthday;
		if (reg.test(idcard) && idcard.length == 18 && id) {
			return true;
		} else {
			alert("请输入正确的身份证号。")
			return false;
		}
	};
	//序列化表单
	$.mySerialize = function(t) {
		var data = t.serialize();
		if (t.find(".td_select").length > 0) {
			t.find(".td_select").each(function(index, el) {
				var k = $(this).find(".td_select_jg span").eq(0).attr("name") ? $(this).find(".td_select_jg span").eq(0).attr("name") : null;
				var v = $(this).find(".td_select_jg span").eq(0).attr("data-v") ? $(this).find(".td_select_jg span").eq(0).attr("data-v") : "";
				if (k) {
					var td = '&' + k + '=' + v;
					data = data + td
				}
			});
			return data
		}
		return data
	};

	//验证邮箱
	$.checkMail = function(val) {
		var reg = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/
		if (reg.test(val)) {
			return true
		} else {
			return false
		}
	};
	//loading移除
	$.removeLoading = function() {
		$(".load").remove();
	}
})