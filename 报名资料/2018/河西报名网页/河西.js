try {
    var youeryuan = $("th:contains('幼儿园选择')").nextAll('td').find("select").eq(0);
    if (youeryuan.length == 0)
        youeryuan = $("td:contains('幼儿园选择')").nextAll('td').find("select").eq(0);
    youeryuan.find("option").each(function (n, i) {
        var text = $(this).text();
        if ((text.indexOf("四") >= 0 || text.indexOf("4") >= 0) && text.indexOf("总园") >= 0) {
            youeryuan.val(text);
            return false;
        }
    })
} catch (e) { }


//承诺书如果跟网页不一样，赶紧手录
$("th:contains('承诺书')").nextAll('td').find("textarea").eq(0).val("本人承诺所填写的2018年河西区幼儿园网上报名申请表中相关内容准确属实，已经阅读了报名须知和报名流程，如有不符，愿意承担一切后果。");

var sfz = "120103201412190027".split("");
$("td:contains('儿童身份证')").nextAll('td').find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(sfz[n]);
    if (i == sfz.length - 1)
        $t.blur();
})

var hk = $("td:contains('户口所在地')").nextAll('td').eq(0);
if (hk.length > 0) {
    var dz = hk.find("input[type=text]");
    dz.eq(0).val("天津");
    dz.eq(1).val("河西");
    dz.eq(2).val("天津市河西区中裕园8号楼3门309-312").blur();
}


var etbm = "12020901514292".split("");
$("td:contains('儿童编码')").nextAll('td').find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(etbm[n]);
})

$("td:contains('民族')").nextAll('td').find("input[type=text]").eq(0).val("汉族");
var sr = "20141219".split("");
$("td:contains('出生日期')").nextAll('td').eq(0).find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(sr[n]);
})
$("td:contains('儿童姓名')").nextAll('td').find("input[type=text]").eq(0).val("张琬宁");
$("td:contains('儿童性别')").nextAll('td').find("input[type=text]").eq(0).val("女");
$("td:contains('户主姓名')").nextAll('td').find("input[type=text]").eq(0).val("张平生");
$("td:contains('儿童国籍')").nextAll('td').find("input[type=text]").eq(0).val("中国");

try {
    var hzgx = $("td:contains('与户主关系')");
    if (hzgx.length == 0)
        hzgx = $("th:contains('与户主关系')");
    if (hzgx.length > 0) {
        var txt = hzgx.nextAll('td').find("input[type=text]");
        if (txt.length > 0)
            txt.val("祖孙");
        else {
            txt = hzgx.nextAll('td').find("select");
            if (txt.length > 0) {
                txt.find("option").each(function (i, e) {
                    var text = $(this).text();
                    if (text.indexOf("祖孙") >= 0) {
                        txt.val(text);
                        return false;
                    }
                });
            }
        }
    }
} catch (e) { }

$("td:contains('出生地')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区");
//$("td:contains('国籍')").nextAll('td').find("input[type=text]").val("中国");
$("td:contains('乳名')").nextAll('td').find("input[type=text]").eq(0).val("菓菓").blur();
$("td:contains('昵称')").nextAll('td').find("input[type=text]").eq(0).val("菓菓").blur();
$("td:contains('体重')").nextAll('td').find("input[type=text]").eq(0).val("14").blur();
$("td:contains('身高')").nextAll('td').find("input[type=text]").eq(0).val("101").blur();
$("td:contains('血型')").nextAll('td').find("input[type=radio]").eq(0).click().blur();
try {
    var dijitai = $("th:contains('第几胎')").nextAll('td').find("select").eq(0);
    if (dijitai.length == 0) {
        dijitai = $("td:contains('第几胎')").nextAll('td').find("select").eq(0);
    }
    dijitai.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("头") >= 0 || text.indexOf("一") >= 0) {
            dijitai.get(0).selectedIndex = i;
            return false;
        }
    });
} catch (e) { }

try {
    //低保儿童
    var dibao = $("th:contains('低保儿童')").nextAll('td').find("select").eq(0);
    if (dibao.length == 0) {
        dibao = $("td:contains('低保儿童')").nextAll('td').find("select").eq(0);
    }
    if (dibao.length > 0) {
        dibao.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0 || text.indexOf("没有") >= 0) {
                dibao.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        dibao = $("th:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (dibao.length == 0)
            dibao = $("td:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        dibao.val("否").blur();
    }
} catch (e) { }


try {
    //残疾儿童
    var canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    if (canjiertong.length == 0) {
        canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    }
    if (canjiertong.length > 0) {
        canjiertong.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0 || text.indexOf("没有") >= 0) {
                canjiertong.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (canjiertong.length == 0)
            canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        canjiertong.val("否").blur();
    }
} catch (e) { }

try {
    var bingshi = $("th:contains('家族病史')").nextAll('td').find("select").eq(0);
    if (bingshi.length == 0) {
        bingshi = $("td:contains('家族病史')").nextAll('td').find("select").eq(0);
    }
    if (bingshi.length > 0) {
        bingshi.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("没有") >= 0) {
                //bingshi.val(text);
                bingshi.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        bingshi = $("th:contains('家族病史')").nextAll('td').find("input[type=text]").eq(0);
        if (bingshi.length == 0)
            bingshi = $("td:contains('家族病史')").nextAll('td').find("input[type=text]").eq(0);
        bingshi.val("无").blur();
    }
} catch (e) { }

////**************通用脚本开始***************/
$("select").find("option[value^='汉']").attr("selected", true);
$("input[type='text']").removeAttr("readonly").removeAttr("disabled");
$("body").delegate("input[type=radio]", "click", function () {
    $("input[type='text']").removeAttr("readonly").removeAttr("disabled");
});

$("input[type=radio][value='男']").attr("checked", true);
$("select").find("option[value^='男']").attr("selected", true);

$("input[type=radio][value='非农业']").attr("checked", true);
$("select").find("option[value^='非农业']").attr("selected", true);

$("input[type=radio][value='无残疾']").attr("checked", true);
$("select").find("option[value^='无残疾']").attr("selected", true).change();

$("select").find("option[value^='否']").attr("selected", true).change();

$("td").each(function (n, i) {
    var $t = $(this);
    if ($t.text() == "独生子女") {
        $t.next("td").find("select").val("是").change();
        return false;
    }
});
////**************通用脚本结束***************/

try {
    var guomintizhi = $("th:contains('过敏体质')").nextAll('td').find("select").eq(0);
    if (guomintizhi.length == 0) {
        guomintizhi = $("td:contains('过敏体质')").nextAll('td').find("select").eq(0);
    }
    if (guomintizhi.length > 0) {
        guomintizhi.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("没有") >= 0) {
                guomintizhi.get(0).selectedIndex = i;
                //guomintizhi.val(text);
                //guomintizhi.val(guomintizhi.options[i].value);
                return false;
            }
        });
    }
    else {
        guomintizhi = $("th:contains('过敏体质')").nextAll('td').find("input[type=text]").eq(0);
        if (guomintizhi.length == 0)
            guomintizhi = $("td:contains('过敏体质')").nextAll('td').find("input[type=text]").eq(0);
        guomintizhi.val("否").blur();
    }
} catch (e) { }


try {
    //是否有依恋物
    var yilianwu = $("th:contains('依恋物')").nextAll('td').find("select").eq(0);
    if (yilianwu.length == 0) {
        yilianwu = $("td:contains('依恋物')").nextAll('td').find("select").eq(0);
    }
    if (yilianwu.length > 0) {
        yilianwu.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("没有") >= 0) {
                yilianwu.get(0).selectedIndex = i;
                return false;
            }
        });
    }
} catch (e) { }

try {
    //低保儿童
    var dibao = $("th:contains('低保儿童')").nextAll('td').find("select").eq(0);
    if (dibao.length == 0) {
        dibao = $("td:contains('低保儿童')").nextAll('td').find("select").eq(0);
    }
    if (dibao.length > 0) {
        dibao.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                dibao.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        dibao = $("th:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (dibao.length == 0)
            dibao = $("td:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        dibao.val("否").blur();
    }
} catch (e) { }


try {
    //残疾儿童
    var canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    if (canjiertong.length == 0) {
        canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    }
    if (canjiertong.length > 0) {
        canjiertong.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                canjiertong.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (canjiertong.length == 0)
            canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        canjiertong.val("否").blur();
    }
} catch (e) { }


try {
    //残疾类型
    var canji = $("th:contains('残疾类')").nextAll('td').find("select").eq(0);
    if (canji.length == 0) {
        canji = $("td:contains('残疾类')").nextAll('td').find("select").eq(0);
    }
    if (canji.length > 0) {
        canji.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("没有") >= 0) {
                canji.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        canji = $("th:contains('残疾类')").nextAll('td').find("input[type=text]").eq(0);
        if (canji.length == 0)
            canji = $("td:contains('残疾类')").nextAll('td').find("input[type=text]").eq(0);
        canji.val("无残疾").blur();
    }
} catch (e) { }

try {
    //政治面目
    var zzmm = $("th:contains('政治面目')").nextAll('td').find("select").eq(0);
    if (zzmm.length == 0) {
        zzmm = $("td:contains('政治面目')").nextAll('td').find("select").eq(0);
    }
    if (zzmm.length > 0) {
        zzmm.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("群众") >= 0) {
                zzmm.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        zzmm = $("th:contains('政治面目')").nextAll('td').find("input[type=text]").eq(0);
        if (zzmm.length == 0)
            zzmm = $("td:contains('政治面目')").nextAll('td').find("input[type=text]").eq(0);
        zzmm.val("群众").blur();
    }
} catch (e) { }

try {
    //3岁前看护人
    var kanhu = $("th:contains('看护人')").nextAll('td').find("select").eq(0);
    if (kanhu.length == 0) {
        kanhu = $("td:contains('看护人')").nextAll('td').find("select").eq(0);
    }
    if (kanhu.length > 0) {
        kanhu.get(0).selectedIndex = 1;
    }
    else {
        kanhu = $("th:contains('看护人')").nextAll('td').find("input[type=text]").eq(0);
        if (kanhu.length == 0)
            kanhu = $("td:contains('看护人')").nextAll('td').find("input[type=text]").eq(0);
        kanhu.val("奶奶").blur();
    }
} catch (e) { }

try {
    //3岁前照看人
    var zhaokan = $("th:contains('照看人')").nextAll('td').find("select").eq(0);
    if (zhaokan.length == 0) {
        zhaokan = $("td:contains('照看人')").nextAll('td').find("select").eq(0);
    }
    if (zhaokan.length > 0) {
        zhaokan.get(0).selectedIndex = 1;
    }
    else {
        zhaokan = $("th:contains('照看人')").nextAll('td').find("input[type=text]").eq(0);
        if (zhaokan.length == 0)
            zhaokan = $("td:contains('照看人')").nextAll('td').find("input[type=text]").eq(0);
        zhaokan.val("奶奶").blur();
    }
} catch (e) { }

try {
    //wugong
    var wugong = $("th:contains('务工')").nextAll('td').find("select").eq(0);
    if (wugong.length == 0) {
        wugong = $("td:contains('务工')").nextAll('td').find("select").eq(0);
    }
    if (wugong.length > 0) {
        wugong.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
                wugong.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        wugong = $("th:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
        if (wugong.length == 0)
            wugong = $("td:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
        wugong.val("否").blur();
    }
} catch (e) { }

try {
    var jhrsfz = "120103198610303518".split("");
    var tdsfz = $("td:contains('监护人身份证号码')");
    if (tdsfz.length == 0)
        tdsfz = $("th:contains('监护人身份证号码')");
    tdsfz.nextAll('td').find("input[type=text]").each(function (n, i) {
        var $t = $(this);
        $t.val(jhrsfz[n]);
    })
} catch (e) { }

try {
    var tdyegx = $("td:contains('与幼儿关系')");
    if (tdyegx.length == 0)
        tdyegx = $("th:contains('与幼儿关系')");
    var txt = tdyegx.nextAll('td').find("input[type=text]");
    if (txt.length > 0)
        txt.val("父女");
    else {
        txt = tdyegx.nextAll('td').find("select");
        if (txt.length > 0) {
            txt.find("option").each(function (n, i) {
                var text = $(this).text();
                if (text.indexOf("父女") >= 0) {
                    txt.val(text);
                    txt.val(txt.options[i].value);
                    return false;
                }
            })
        }
    }
} catch (e) { }

$("td:contains('监护人姓名')").nextAll('td').find("input[type=text]").eq(0).val("张庆");
$("td:contains('监护人年龄')").nextAll('td').find("input[type=text]").eq(0).val("32");
$("td:contains('监护人性别')").nextAll('td').find("input[type=text]").eq(0).val("男");
$("td:contains('监护人学历')").nextAll('td').find("input[type=text]").eq(0).val("本科");
$("td:contains('政治面目')").nextAll('td').find("input[type=text]").eq(0).val("群众");

$("td:contains('监护人手机号码')").nextAll('td').find("input[type=text]").eq(0).val("13612106813");
var sjh = "13612106813".split("");   //爸爸电话
$("td:contains('监护人手机号码')").nextAll('td').find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(sjh[n]);
})

$("td:contains('工作单位')").nextAll('td').find("input[type=text]").eq(0).val("中交一航局第一工程有限公司");
$("td:contains('监护人职务')").nextAll('td').find("input[type=text]").eq(0).val("职员");


$("td:contains('兴趣爱好')").nextAll('td').find("input[type=text]").eq(0).val("唱歌");
$("td:contains('性格')").nextAll('td').find("input[type=text]").eq(0).val("外向");
$("td:contains('水果')").nextAll('td').find("input[type=text]").eq(0).val("苹果");
$("td:contains('颜色')").nextAll('td').find("input[type=text]").eq(0).val("蓝色");
$("td:contains('游戏')").nextAll('td').find("input[type=text]").eq(0).val("躲猫猫");
$("td:contains('玩具')").nextAll('td').find("input[type=text]").eq(0).val("机器猫");
$("td:contains('原因')").nextAll('td').find("input[type=text]").eq(0).val("离家近");
$("td:contains('良好习惯')").nextAll('td').find("input[type=text]").eq(0).val("按时吃饭");
$("td:contains('多长时间')").nextAll('td').find("input[type=text]").eq(0).val("三小时");
$("td:contains('生活')").nextAll('td').find("input[type=text]").eq(0).val("饮食");
$("td:contains('沟通')").nextAll('td').find("input[type=text]").eq(0).val("微信");
$("td:contains('兴趣活动')").nextAll('td').find("input[type=text]").eq(0).val("躲猫猫");

$("td:contains('看护人')").nextAll('td').find("input[type=checkbox]").eq(0).click();

try {
    var jiedao = $("th:contains('街道')").nextAll('td').find("select").eq(0);
    if (jiedao.length == 0)
        jiedao = $("td:contains('街道')").nextAll('td').find("select").eq(0);
    jiedao.find("option").each(function (n, i) {
        var text = $(this).text();
        if (text.indexOf("越秀路街") >= 0) {
            jiedao.val(text);
            return false;
        }
    })
} catch (e) { }



$("td:contains('进行预防接种')").nextAll('td').find("input[type=radio]").eq(0).click().blur();
$("td:contains('容易生病')").nextAll('td').find("input[type=radio]").eq(1).click().blur();
$("td:contains('水痘')").nextAll('td').find("input[type=radio]").eq(0).click().blur();
$("td:contains('肥胖')").nextAll('td').find("input[type=radio]").eq(1).click().blur();
$("td:contains('过敏体质')").nextAll('td').find("input[type=radio]").eq(1).click().blur();
$("td:contains('心脏健康筛查')").nextAll('td').find("input[type=radio]").eq(0).click().blur();


var bingshi = $("td:contains('住院')").nextAll('td').find("select").eq(0);
if (bingshi.length > 0) {
    bingshi.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("没有") >= 0) {
            //bingshi.val(text);
            bingshi.get(0).selectedIndex = i;
            return false;
        }
    });
}

var bingshi = $("td:contains('视力障碍')").nextAll('td').find("select").eq(0);
if (bingshi.length > 0) {
    bingshi.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("没有") >= 0) {
            //bingshi.val(text);
            bingshi.get(0).selectedIndex = i;
            return false;
        }
    });
}


var td = $("span:contains('游戏活动')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}

var td = $("span:contains('颜色')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}

var td = $("span:contains('玩具')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=text]");
chks.val("帮帮龙");
}


var td = $("span:contains('主食')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}


var td = $("span:contains('自理能力')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}

var td = $("span:contains('蔬菜')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}


var td = $("span:contains('阅读')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}

var td = $("span:contains('水果')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}



var td = $("span:contains('兴趣爱好')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}

var td = $("span:contains('卡通形象')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=text]");
     chks.val("大头儿子和小头爸爸");
}


var td = $("span:contains('分歧')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}


var td = $("span:contains('教育期望')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}


var td = $("span:contains('陌生人')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
    chks.eq(4).click();
}

var td = $("span:contains('发展')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
    chks.eq(2).click();
}

var td = $("span:contains('性格')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click();
}

var td = $("span:contains('幼儿园老师沟通')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=text]");
    chks.val("微信和电话");
}

var td = $("span:contains('早教活动')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=text]");
    chks.val("性格活泼");
}

var td = $("span:contains('良好习惯')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click
    chks.eq(2).click();
}

var td = $("span:contains('引导孩子')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=checkbox]");
    chks.eq(0).click();
    chks.eq(1).click
    chks.eq(2).click();
}

var td = $("span:contains('引导孩子')").eq(0);
if (td.length > 0) {
    var chks = td.nextAll("div").find("input[type=radio]");
    chks.eq(1).click
}

var xuexing = $("span:contains('血型')");
if (xuexing.length > 0) {
    xuexing.parents("td").next("td").eq(0).find("input[type=radio]").eq(0).click();
}
