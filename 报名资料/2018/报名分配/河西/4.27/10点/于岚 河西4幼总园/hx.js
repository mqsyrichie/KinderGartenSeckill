/**************通用脚本开始***************/
$("select").find("option[value^='汉']").attr("selected", true);
$("input[type='text']").removeAttr("readonly").removeAttr("disabled");
$("body").delegate("input[type=radio]","click",function(){
$("input[type='text']").removeAttr("readonly").removeAttr("disabled");
});

$("input[type=radio][value='男']").attr("checked",true);
$("select").find("option[value^='男']").attr("selected", true);

$("input[type=radio][value='非农业']").attr("checked",true);
$("select").find("option[value^='非农业']").attr("selected", true);

$("input[type=radio][value='无残疾']").attr("checked",true);
$("select").find("option[value^='无残疾']").attr("selected", true).change();

$("select").find("option[value^='否']").attr("selected", true).change();

$("td").each(function (n,i) {
    var $t = $(this);
    if ($t.text() == "独生子女") {
        $t.next("td").find("select").val("是").change();
        return false;
    }
});
/**************通用脚本结束***************/

$("select option").each(function(i,e){
	var text=$(this).text();
    if (text.indexOf("第四") >= 0 || text.indexOf("第4") >= 0) {
        $("select").get(0).selectedIndex = i;
        return false;
    }
})

//基本信息
var table_jbxx = $("table:contains('孩子的基本信息')");
table_jbxx.find("td:contains('身份证号')").nextAll('td').find("input[type=text]").eq(0).val("12010320131102015X").blur();
table_jbxx.find("td:contains('户口所在')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区爱国道爱国里7门601").blur();
table_jbxx.find("td:contains('房屋所有权地址')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区爱国道爱国里7门601").blur();
table_jbxx.find("td:contains('接种证')").nextAll('td').find("input[type=text]").eq(0).val("12020901320885").blur();

table_jbxx.find("th:contains('身份证号')").nextAll('td').find("input[type=text]").eq(0).val("12010320131102015X").blur();
table_jbxx.find("th:contains('户口所在')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区爱国道爱国里7门601").blur();
table_jbxx.find("th:contains('房屋所有权地址')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区爱国道爱国里7门601").blur();
table_jbxx.find("th:contains('接种证')").nextAll('td').find("input[type=text]").eq(0).val("12020901320885").blur();

//附加信息
var table_fjxx = $("table:contains('附加信息')");
table_fjxx.find("td:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("龚宣屹").blur();
table_fjxx.find("td:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();
table_fjxx.find("td:contains('民族')").nextAll('td').find("input[type=text]").eq(0).val("汉族").blur();
table_fjxx.find("td:contains('出生日期')").nextAll('td').find("input[type=text]").eq(0).val("2013-11-02").blur();
table_fjxx.find("td:contains('出生地')").nextAll('td').find("input[type=text]").eq(0).val("天津").blur();
table_fjxx.find("td:contains('国籍')").nextAll('td').find("input[type=text]").eq(0).val("中国").blur();
table_fjxx.find("td:contains('户主姓名')").nextAll('td').find("input[type=text]").eq(0).val("于建民").blur();
table_fjxx.find("td:contains('幼儿与户主关系')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();
table_fjxx.find("td:contains('房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("于建民").blur();
table_fjxx.find("td:contains('与房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();

table_fjxx.find("th:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("龚宣屹").blur();              
table_fjxx.find("th:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                  
table_fjxx.find("th:contains('民族')").nextAll('td').find("input[type=text]").eq(0).val("汉族").blur();                
table_fjxx.find("th:contains('出生日期')").nextAll('td').find("input[type=text]").eq(0).val("2013-11-02").blur();      
table_fjxx.find("th:contains('出生地')").nextAll('td').find("input[type=text]").eq(0).val("天津").blur();              
table_fjxx.find("th:contains('国籍')").nextAll('td').find("input[type=text]").eq(0).val("中国").blur();                
table_fjxx.find("th:contains('户主姓名')").nextAll('td').find("input[type=text]").eq(0).val("于建民").blur();          
table_fjxx.find("th:contains('幼儿与户主关系')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();      
table_fjxx.find("th:contains('房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("于建民").blur();
table_fjxx.find("th:contains('与房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();

//扩展信息
var table_kzxx = $("table:contains('扩展信息')");
table_kzxx.find("td:contains('乳名')").nextAll('td').find("input[type=text]").eq(0).val("龙龙").blur();
table_kzxx.find("td:contains('昵称')").nextAll('td').find("input[type=text]").eq(0).val("龙龙").blur();
table_kzxx.find("td:contains('体重')").nextAll('td').find("input[type=text]").eq(0).val("16").blur();
table_kzxx.find("td:contains('身高')").nextAll('td').find("input[type=text]").eq(0).val("103").blur();

table_kzxx.find("th:contains('乳名')").nextAll('td').find("input[type=text]").eq(0).val("龙龙").blur();
table_kzxx.find("th:contains('昵称')").nextAll('td').find("input[type=text]").eq(0).val("龙龙").blur();
table_kzxx.find("th:contains('体重')").nextAll('td').find("input[type=text]").eq(0).val("16").blur();  
table_kzxx.find("th:contains('身高')").nextAll('td').find("input[type=text]").eq(0).val("103").blur(); 

var dijitai = table_kzxx.find("th:contains('第几胎')").nextAll('td').find("select").eq(0);
if (dijitai.length = 0) {
    dijitai = table_kzxx.find("td:contains('第几胎')").nextAll('td').find("select").eq(0);
}
dijitai.find("option").each(function (i,e) {
    var text=$(this).text();
    if (text.indexOf("1") >= 0 || text.indexOf("一") >= 0) {
        dijitai.get(0).selectedIndex = i;
        return false;
    }
});

var bingshi = table_kzxx.find("th:contains('是否有家庭病史')").nextAll('td').find("select").eq(0);
if (bingshi.length == 0) {
    bingshi = table_kzxx.find("td:contains('是否有家庭病史')").nextAll('td').find("select").eq(0);
}
if (bingshi.length > 0) {
    bingshi.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
            bingshi.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    bingshi = table_kzxx.find("th:contains('是否有家庭病史')").nextAll('td').find("input[type=text]").eq(0);
    if (bingshi.length == 0)
        bingshi = table_kzxx.find("td:contains('是否有家庭病史')").nextAll('td').find("input[type=text]").eq(0);
    bingshi.val("无").blur();
}

var guomintizhi = table_kzxx.find("th:contains('是否为过敏体质')").nextAll('td').find("select").eq(0);
if (guomintizhi.length == 0) {
    guomintizhi = table_kzxx.find("td:contains('是否为过敏体质')").nextAll('td').find("select").eq(0);
}
if (guomintizhi.length > 0) {
    guomintizhi.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
            guomintizhi.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    guomintizhi = table_kzxx.find("th:contains('是否为过敏体质')").nextAll('td').find("input[type=text]").eq(0);
    if(guomintizhi.length==0)
        guomintizhi = table_kzxx.find("td:contains('是否为过敏体质')").nextAll('td').find("input[type=text]").eq(0);
    guomintizhi.val("否").blur();
}

//是否有依恋物
var yilianwu = table_kzxx.find("th:contains('是否有依恋物')").nextAll('td').find("select").eq(0);
if (yilianwu.length == 0) {
    yilianwu = table_kzxx.find("td:contains('是否有依恋物')").nextAll('td').find("select").eq(0);
}
yilianwu.get(0).selectedIndex = 1;

//低保儿童
var dibao = table_kzxx.find("th:contains('低保儿童')").nextAll('td').find("select").eq(0);
if (dibao.length == 0) {
    dibao = table_kzxx.find("td:contains('低保儿童')").nextAll('td').find("select").eq(0);
}
if (dibao.length > 0) {
    dibao.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
            dibao.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    dibao = table_kzxx.find("th:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
    if (dibao.length == 0)
        dibao = table_kzxx.find("td:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
    dibao.val("否").blur();
}

//残疾儿童
var canjiertong = table_kzxx.find("th:contains('残疾儿童')").nextAll('td').find("select").eq(0);
if (canjiertong.length == 0) {
    canjiertong = table_kzxx.find("td:contains('残疾儿童')").nextAll('td').find("select").eq(0);
}
if (canjiertong.length > 0) {
    canjiertong.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
            canjiertong.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    canjiertong = table_kzxx.find("th:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
    if (canjiertong.length == 0)
        canjiertong = table_kzxx.find("td:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
    canjiertong.val("否").blur();
}


//残疾类型
var canji = table_kzxx.find("th:contains('残疾类型')").nextAll('td').find("select").eq(0);
if (canji.length == 0) {
    canji = table_kzxx.find("td:contains('残疾类型')").nextAll('td').find("select").eq(0);
}
if (canji.length > 0) {
    canji.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("没有") >= 0) {
            canji.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    canji = table_kzxx.find("th:contains('残疾类型')").nextAll('td').find("input[type=text]").eq(0);
    if (canji.length == 0)
        canji = table_kzxx.find("td:contains('残疾类型')").nextAll('td').find("input[type=text]").eq(0);
    canji.val("无残疾").blur();
}

//3岁前看护人
var kanhu = table_kzxx.find("th:contains('主要看护人')").nextAll('td').find("select").eq(0);
if (kanhu.length == 0) {
    kanhu = table_kzxx.find("td:contains('主要看护人')").nextAll('td').find("select").eq(0);
}
if (kanhu.length > 0) {
    kanhu.get(0).selectedIndex = 1;
}
else {
    kanhu = table_kzxx.find("th:contains('主要看护人')").nextAll('td').find("input[type=text]").eq(0);
    if (kanhu.length == 0)
        kanhu = table_kzxx.find("td:contains('主要看护人')").nextAll('td').find("input[type=text]").eq(0);
    kanhu.val("妈妈").blur();
}

//wugong
var wugong = table_kzxx.find("th:contains('务工')").nextAll('td').find("select").eq(0);
if (wugong.length == 0) {
    wugong = table_kzxx.find("td:contains('务工')").nextAll('td').find("select").eq(0);
}
if (wugong.length > 0) {
    wugong.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
            wugong.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    wugong = table_kzxx.find("th:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
    if (wugong.length == 0)
        wugong = table_kzxx.find("td:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
    wugong.val("否").blur();
}

//其他信息
var table_qtxx = $("table:contains('其他信息')");
table_qtxx.find("td:contains('兴趣爱好')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("td:contains('性格')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("td:contains('身体')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("td:contains('喜欢吃')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("td:contains('自理能力')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);

table_qtxx.find("th:contains('兴趣爱好')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("th:contains('性格')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("th:contains('身体')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("th:contains('喜欢吃')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
table_qtxx.find("th:contains('自理能力')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);



var table_jhrxx = $("table:contains('监护人信息')");
table_jhrxx.find("td:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("龚正").blur();
table_jhrxx.find("td:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();
table_jhrxx.find("td:contains('学历')").nextAll('td').find("input[type=text]").eq(0).val("本科").blur();
table_jhrxx.find("td:contains('政治面貌')").nextAll('td').find("input[type=text]").eq(0).val("群众").blur();
table_jhrxx.find("td:contains('身份证号码')").nextAll('td').find("input[type=text]").eq(0).val("120102198409162315").blur();
table_jhrxx.find("td:contains('工作单位')").nextAll('td').find("input[type=text]").eq(0).val("天津航空股份有限公司").blur();
table_jhrxx.find("td:contains('职务')").nextAll('td').find("input[type=text]").eq(0).val("职员").blur();
table_jhrxx.find("td:contains('与幼儿的关系')").nextAll('td').find("input[type=text]").eq(0).val("父子").blur();
table_jhrxx.find("td:contains('手机号')").nextAll('td').find("input[type=text]").eq(0).val("13682003920").blur();

table_jhrxx.find("th:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("龚正").blur();                  
table_jhrxx.find("th:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                      
table_jhrxx.find("th:contains('学历')").nextAll('td').find("input[type=text]").eq(0).val("本科").blur();                    
table_jhrxx.find("th:contains('政治面貌')").nextAll('td').find("input[type=text]").eq(0).val("群众").blur();                
table_jhrxx.find("th:contains('身份证号码')").nextAll('td').find("input[type=text]").eq(0).val("120102198409162315").blur();
table_jhrxx.find("th:contains('工作单位')").nextAll('td').find("input[type=text]").eq(0).val("天津航空股份有限公司").blur();  
table_jhrxx.find("th:contains('职务')").nextAll('td').find("input[type=text]").eq(0).val("职员").blur();                    
table_jhrxx.find("th:contains('与幼儿的关系')").nextAll('td').find("input[type=text]").eq(0).val("父子").blur();            
table_jhrxx.find("th:contains('手机号')").nextAll('td').find("input[type=text]").eq(0).val("13682003920").blur();           
