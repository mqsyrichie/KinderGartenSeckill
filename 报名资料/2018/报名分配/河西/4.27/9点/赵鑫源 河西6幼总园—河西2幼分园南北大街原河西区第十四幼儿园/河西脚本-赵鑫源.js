/**************通用脚本开始***************/
$("select").find("option[value^='汉']").attr("selected", true);
$("input[type='text']").removeAttr("readonly").removeAttr("disabled");
$("body").delegate("input[type=radio]","click",function(){
$("input[type='text']").removeAttr("readonly").removeAttr("disabled");
});

$("input[type=radio][value='男']").attr("checked",true);
$("select").find("option[value^='男']").attr("selected", true);

$("input[type=radio][value='非农业']").attr("checked",true);
$("select").find("option[value^='非农业']").attr("selected", true);

$("input[type=radio][value='无残疾']").attr("checked",true);
$("select").find("option[value^='无残疾']").attr("selected", true).change();

$("select").find("option[value^='否']").attr("selected", true).change();

$("td").each(function (n,i) {
    var $t = $(this);
    if ($t.text() == "独生子女") {
        $t.next("td").find("select").val("是").change();
        return false;
    }
});
/**************通用脚本结束***************/

//选择幼儿园
$("select option").each(function(i,e){
	var text=$(this).text();
    if ((text.indexOf("第2") >= 0 || text.indexOf("第2") >= 0) && text.indexOf("分园") >= 0) {
        $("select").get(0).selectedIndex = i;
        return false;
    }
})

//基本信息
//var table_jbxx = $("table:contains('基本信息')");
//if(table_jbxx){
$("td:contains('身份证号')").nextAll('td').find("input[type=text]").eq(0).val("120103201404060011").blur();                
$("td:contains('户口所在')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区解放南路美好里66门602").blur();
$("td:contains('房屋所有权地址')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区解放南路美好里66门602").blur();
$("td:contains('接种证')").nextAll('td').find("input[type=text]").eq(0).val("12020901342926").blur();                      

$("th:contains('身份证号')").nextAll('td').find("input[type=text]").eq(0).val("120103201404060011").blur();                     
$("th:contains('户口所在')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区解放南路美好里66门602").blur();      
$("th:contains('房屋所有权地址')").nextAll('td').find("input[type=text]").eq(0).val("天津市河西区解放南路美好里66门602").blur();
$("th:contains('接种证')").nextAll('td').find("input[type=text]").eq(0).val("12020901342926").blur();                           
//}

//孩子附加信息
var table_fjxx = $("table:contains('附加信息')");
if(table_fjxx){
table_fjxx.find("td:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("赵逸辰").blur();              
table_fjxx.find("td:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                  
table_fjxx.find("td:contains('民族')").nextAll('td').find("input[type=text]").eq(0).val("汉族").blur();                
table_fjxx.find("td:contains('出生日期')").nextAll('td').find("input[type=text]").eq(0).val("2014-04-06").blur();      
table_fjxx.find("td:contains('出生地')").nextAll('td').find("input[type=text]").eq(0).val("天津").blur();              
table_fjxx.find("td:contains('国籍')").nextAll('td').find("input[type=text]").eq(0).val("中国").blur();                
table_fjxx.find("td:contains('户主姓名')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();          
table_fjxx.find("td:contains('幼儿与户主关系')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();      
table_fjxx.find("td:contains('房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();
table_fjxx.find("td:contains('与房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();
                                                                                                                       
table_fjxx.find("th:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("赵逸辰").blur();              
table_fjxx.find("th:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                  
table_fjxx.find("th:contains('民族')").nextAll('td').find("input[type=text]").eq(0).val("汉族").blur();                
table_fjxx.find("th:contains('出生日期')").nextAll('td').find("input[type=text]").eq(0).val("2014-04-06").blur();      
table_fjxx.find("th:contains('出生地')").nextAll('td').find("input[type=text]").eq(0).val("天津").blur();              
table_fjxx.find("th:contains('国籍')").nextAll('td').find("input[type=text]").eq(0).val("中国").blur();                
table_fjxx.find("th:contains('户主姓名')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();          
table_fjxx.find("th:contains('幼儿与户主关系')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();      
table_fjxx.find("th:contains('房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();
table_fjxx.find("th:contains('与房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();
}
else
{
	$("td:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("赵逸辰").blur();              
	$("td:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                  
	$("td:contains('民族')").nextAll('td').find("input[type=text]").eq(0).val("汉族").blur();                
	$("td:contains('出生日期')").nextAll('td').find("input[type=text]").eq(0).val("2014-04-06").blur();      
	$("td:contains('出生地')").nextAll('td').find("input[type=text]").eq(0).val("天津").blur();              
	$("td:contains('国籍')").nextAll('td').find("input[type=text]").eq(0).val("中国").blur();                
	$("td:contains('户主姓名')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();          
	$("td:contains('幼儿与户主关系')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();      
	$("td:contains('房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();
	$("td:contains('与房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();

	$("th:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("赵逸辰").blur();              
	$("th:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                  
	$("th:contains('民族')").nextAll('td').find("input[type=text]").eq(0).val("汉族").blur();                
	$("th:contains('出生日期')").nextAll('td').find("input[type=text]").eq(0).val("2014-04-06").blur();      
	$("th:contains('出生地')").nextAll('td').find("input[type=text]").eq(0).val("天津").blur();              
	$("th:contains('国籍')").nextAll('td').find("input[type=text]").eq(0).val("中国").blur();                
	$("th:contains('户主姓名')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();          
	$("th:contains('幼儿与户主关系')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();      
	$("th:contains('房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("孙秀华").blur();
	$("th:contains('与房屋所有权证持证人')").nextAll('td').find("input[type=text]").eq(0).val("祖孙").blur();
}


//扩展信息
var table_kzxx = $("table:contains('扩展信息')");
//if(table_kzxx){
$("td:contains('乳名')").nextAll('td').find("input[type=text]").eq(0).val("坦克").blur();
$("td:contains('昵称')").nextAll('td').find("input[type=text]").eq(0).val("坦克").blur();
$("td:contains('体重')").nextAll('td').find("input[type=text]").eq(0).val("17").blur();  
$("td:contains('身高')").nextAll('td').find("input[type=text]").eq(0).val("103").blur(); 

$("th:contains('乳名')").nextAll('td').find("input[type=text]").eq(0).val("坦克").blur();
$("th:contains('昵称')").nextAll('td').find("input[type=text]").eq(0).val("坦克").blur();
$("th:contains('体重')").nextAll('td').find("input[type=text]").eq(0).val("17").blur();  
$("th:contains('身高')").nextAll('td').find("input[type=text]").eq(0).val("103").blur(); 

var dijitai = $("th:contains('第几胎')").nextAll('td').find("select").eq(0);
if (dijitai.length = 0) {
    dijitai = $("td:contains('第几胎')").nextAll('td').find("select").eq(0);
}
dijitai.find("option").each(function (i,e) {
    var text=$(this).text();
    if (text.indexOf("1") >= 0 || text.indexOf("一") >= 0) {
        dijitai.get(0).selectedIndex = i;
        return false;
    }
});

/*********监护人性别与孩子性别一致（如果孩子性别为女，则监护人填妈妈信息）*********/
var xingbie = $("th:contains('性别')").nextAll('td').find("select").eq(0);
if (xingbie.length == 0) {
    xingbie = $("td:contains('性别')").nextAll('td').find("select").eq(0);
}
if (xingbie.length > 0) {
    xingbie.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("男") >= 0) { //监护人与孩子性别一致
            xingbie.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    xingbie = $("th:contains('性别')").nextAll('td').find("input[type=text]").eq(0);
    if (xingbie.length == 0)
        xingbie = $("td:contains('性别')").nextAll('td').find("input[type=text]").eq(0);
    xingbie.val("男").blur();//监护人与孩子性别一致
}
/*********监护人性别与孩子性别一致（如果孩子性别为女，则监护人填妈妈信息）*********/

var minzu = $("th:contains('民族')").nextAll('td').find("select").eq(0);
if (minzu.length == 0) {
    minzu = $("td:contains('民族')").nextAll('td').find("select").eq(0);
}
if (minzu.length > 0) {
    minzu.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("汉") >= 0) { //民族为汉
            minzu.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    minzu = $("th:contains('民族')").nextAll('td').find("input[type=text]").eq(0);
    if (minzu.length == 0)
        minzu = $("td:contains('民族')").nextAll('td').find("input[type=text]").eq(0);
    minzu.val("汉").blur();
}

//街道
var jiedao = $("th:contains('街道')").nextAll('td').find("select").eq(0);
if (jiedao.length == 0) {
    jiedao = $("td:contains('街道')").nextAll('td').find("select").eq(0);
}
if (jiedao.length > 0) {
    jiedao.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("挂甲寺") >= 0) { //所属街道
            jiedao.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    jiedao = $("th:contains('街道')").nextAll('td').find("input[type=text]").eq(0);
    if (jiedao.length == 0)
        jiedao = $("td:contains('街道')").nextAll('td').find("input[type=text]").eq(0);
    jiedao.val("挂甲寺街").blur();//所属街道
}

//派出所
var pcs = $("th:contains('派出所')").nextAll('td').find("select").eq(0);
if (pcs.length == 0) {
    pcs = $("td:contains('派出所')").nextAll('td').find("select").eq(0);
}
if (pcs.length > 0) {
    pcs.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("挂甲寺") >= 0) { //所属街道
            pcs.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    pcs = $("th:contains('派出所')").nextAll('td').find("input[type=text]").eq(0);
    if (pcs.length == 0)
        pcs = $("td:contains('派出所')").nextAll('td').find("input[type=text]").eq(0);
    pcs.val("挂甲寺街派出所").blur();//所属街道
}

var bingshi = $("th:contains('是否有家庭病史')").nextAll('td').find("select").eq(0);
if (bingshi.length == 0) {
    bingshi = $("td:contains('是否有家庭病史')").nextAll('td').find("select").eq(0);
}
if (bingshi.length > 0) {
    bingshi.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
            bingshi.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    bingshi = $("th:contains('是否有家庭病史')").nextAll('td').find("input[type=text]").eq(0);
    if (bingshi.length == 0)
        bingshi = $("td:contains('是否有家庭病史')").nextAll('td').find("input[type=text]").eq(0);
    bingshi.val("无").blur();
}

var guomintizhi = $("th:contains('过敏体质')").nextAll('td').find("select").eq(0);
if (guomintizhi.length == 0) {
    guomintizhi = $("td:contains('过敏体质')").nextAll('td').find("select").eq(0);
}
if (guomintizhi.length > 0) {
    guomintizhi.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
            guomintizhi.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    guomintizhi = $("th:contains('过敏体质')").nextAll('td').find("input[type=text]").eq(0);
    if(guomintizhi.length==0)
        guomintizhi = $("td:contains('过敏体质')").nextAll('td').find("input[type=text]").eq(0);
    guomintizhi.val("否").blur();
}

//是否有依恋物
var yilianwu = $("th:contains('依恋物')").nextAll('td').find("select").eq(0);
if (yilianwu.length == 0) {
    yilianwu = $("td:contains('依恋物')").nextAll('td').find("select").eq(0);
}
if (yilianwu.length > 0) yilianwu.get(0).selectedIndex = 1;

//低保儿童
var dibao = $("th:contains('低保儿童')").nextAll('td').find("select").eq(0);
if (dibao.length == 0) {
    dibao = $("td:contains('低保儿童')").nextAll('td').find("select").eq(0);
}
if (dibao.length > 0) {
    dibao.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
            dibao.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    dibao = $("th:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
    if (dibao.length == 0)
        dibao = $("td:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
    dibao.val("否").blur();
}

//残疾儿童
var canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("select").eq(0);
if (canjiertong.length == 0) {
    canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("select").eq(0);
}
if (canjiertong.length > 0) {
    canjiertong.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
            canjiertong.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
    if (canjiertong.length == 0)
        canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
    canjiertong.val("否").blur();
}


//残疾类型
var canji = $("th:contains('残疾类型')").nextAll('td').find("select").eq(0);
if (canji.length == 0) {
    canji = $("td:contains('残疾类型')").nextAll('td').find("select").eq(0);
}
if (canji.length > 0) {
    canji.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("没有") >= 0) {
            canji.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    canji = $("th:contains('残疾类型')").nextAll('td').find("input[type=text]").eq(0);
    if (canji.length == 0)
        canji = $("td:contains('残疾类型')").nextAll('td').find("input[type=text]").eq(0);
    canji.val("无残疾").blur();
}

//政治面目
var zzmm = $("th:contains('政治面目')").nextAll('td').find("select").eq(0);
if (zzmm.length == 0) {
    zzmm = $("td:contains('政治面目')").nextAll('td').find("select").eq(0);
}
if (zzmm.length > 0) {
    zzmm.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("群众") >= 0) {
            zzmm.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    zzmm = $("th:contains('政治面目')").nextAll('td').find("input[type=text]").eq(0);
    if (zzmm.length == 0)
        zzmm = $("td:contains('政治面目')").nextAll('td').find("input[type=text]").eq(0);
    zzmm.val("群众").blur();
}

//3岁前看护人
var kanhu = $("th:contains('看护人')").nextAll('td').find("select").eq(0);
if (kanhu.length == 0) {
    kanhu = $("td:contains('看护人')").nextAll('td').find("select").eq(0);
}
if (kanhu.length > 0) {
    kanhu.get(0).selectedIndex = 1;
}
else {
    kanhu = $("th:contains('看护人')").nextAll('td').find("input[type=text]").eq(0);
    if (kanhu.length == 0)
        kanhu = $("td:contains('看护人')").nextAll('td').find("input[type=text]").eq(0);
    kanhu.val("妈妈").blur();
}

//3岁前照看人
var zhaokan = $("th:contains('照看人')").nextAll('td').find("select").eq(0);
if (zhaokan.length == 0) {
    zhaokan = $("td:contains('照看人')").nextAll('td').find("select").eq(0);
}
if (zhaokan.length > 0) {
    zhaokan.get(0).selectedIndex = 1;
}
else {
    zhaokan = $("th:contains('照看人')").nextAll('td').find("input[type=text]").eq(0);
    if (zhaokan.length == 0)
        zhaokan = $("td:contains('照看人')").nextAll('td').find("input[type=text]").eq(0);
    zhaokan.val("妈妈").blur();
}

//wugong
var wugong = $("th:contains('务工')").nextAll('td').find("select").eq(0);
if (wugong.length == 0) {
    wugong = $("td:contains('务工')").nextAll('td').find("select").eq(0);
}
if (wugong.length > 0) {
    wugong.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
            wugong.get(0).selectedIndex = i;
            return false;
        }
    });
}
else {
    wugong = $("th:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
    if (wugong.length == 0)
        wugong = $("td:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
    wugong.val("否").blur();
}
//}

//其他信息
var table_qtxx = $("table:contains('其他信息')");
//if(table_qtxx){
$("td:contains('兴趣爱好')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true); 
$("td:contains('性格')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);     
$("td:contains('身体')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);     
$("td:contains('喜欢吃')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);   
$("td:contains('自理能力')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true); 

$("th:contains('兴趣爱好')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
$("th:contains('性格')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
$("th:contains('身体')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
$("th:contains('喜欢吃')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
$("th:contains('自理能力')").nextAll('td').find("input[type=checkbox]").eq(0).attr("checked",true);
//}


var table_jhrxx = $("table:contains('监护人信息')");
if(table_jhrxx){
table_jhrxx.find("td:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("赵鑫源").blur();                  
table_jhrxx.find("td:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                      
table_jhrxx.find("td:contains('学历')").nextAll('td').find("input[type=text]").eq(0).val("大专").blur();                    
table_jhrxx.find("td:contains('政治面貌')").nextAll('td').find("input[type=text]").eq(0).val("群众").blur();                
table_jhrxx.find("td:contains('身份证号码')").nextAll('td').find("input[type=text]").eq(0).val("120103198701174213").blur();
table_jhrxx.find("td:contains('工作单位')").nextAll('td').find("input[type=text]").eq(0).val("天津奥的斯有限公司").blur();  
table_jhrxx.find("td:contains('职务')").nextAll('td').find("input[type=text]").eq(0).val("职员").blur();                    
table_jhrxx.find("td:contains('与幼儿的关系')").nextAll('td').find("input[type=text]").eq(0).val("父子").blur();            
table_jhrxx.find("td:contains('手机号')").nextAll('td').find("input[type=text]").eq(0).val("13803041605").blur();           

table_jhrxx.find("th:contains('姓名')").nextAll('td').find("input[type=text]").eq(0).val("赵鑫源").blur();                  
table_jhrxx.find("th:contains('性别')").nextAll('td').find("input[type=text]").eq(0).val("男").blur();                      
table_jhrxx.find("th:contains('学历')").nextAll('td').find("input[type=text]").eq(0).val("大专").blur();                    
table_jhrxx.find("th:contains('政治面貌')").nextAll('td').find("input[type=text]").eq(0).val("群众").blur();                
table_jhrxx.find("th:contains('身份证号码')").nextAll('td').find("input[type=text]").eq(0).val("120103198701174213").blur();
table_jhrxx.find("th:contains('工作单位')").nextAll('td').find("input[type=text]").eq(0).val("天津奥的斯有限公司").blur();  
table_jhrxx.find("th:contains('职务')").nextAll('td').find("input[type=text]").eq(0).val("职员").blur();                    
table_jhrxx.find("th:contains('与幼儿的关系')").nextAll('td').find("input[type=text]").eq(0).val("父子").blur();            
table_jhrxx.find("th:contains('手机号')").nextAll('td').find("input[type=text]").eq(0).val("13803041605").blur();           
}
