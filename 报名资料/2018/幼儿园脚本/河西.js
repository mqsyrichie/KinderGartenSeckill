$("[onpaste='return false']").removeAttr("onpaste");

try {
    var youeryuan = $("th:contains('幼儿园选择')").nextAll('td').find("select").eq(0);
    if (youeryuan.length == 0)
        youeryuan = $("td:contains('幼儿园选择')").nextAll('td').find("select").eq(0);
    // youeryuan.find("option").each(function (n, i) {
        // var text = $(this).text();
        // if ((text.indexOf("第十七") >= 0 || text.indexOf("第17") >= 0) && text.indexOf("总园") >= 0) {
            // youeryuan.val(text);
            // return false;
        // }
    // });
	youeryuan.find("option").each(function (n, i) {
        var text = $(this).text();
        if (text.indexOf("第十七") >= 0 || text.indexOf("第17") >= 0) {
            youeryuan.val(text);
            return false;
        }
    });
} catch (e) { }

//承诺书如果跟网页不一样，赶紧手录
$("th:contains('承诺书')").nextAll('td').find("textarea").eq(0).val("我已经详细阅读报名须知，承诺所填信息准确属实，如有不符愿意承担一切后果。");

var sfz = "120103201409190106".split("");
$("td:contains('儿童身份证号')").nextAll('td').find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(sfz[n]);
})
$("td:contains('儿童户口所在地址')").nextAll('td').find("input[type=text][title='儿童户口所在地址']").val("天津市河西区环湖中路环湖东里21门501")
var etbm = "12020901475364".split("");
$("td:contains('接种证号')").nextAll('td').find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(etbm[n]);
})

$("td:contains('民族')").nextAll('td').find("input[type=text]").val("汉族");
var sr = "20140919".split("");
$("td:contains('出生日期')").nextAll('td').find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(sr[n]);
})
$("td:contains('儿童姓名')").nextAll('td').find("input[type=text]").val("韩婧祎");
$("td:contains('儿童性别')").nextAll('td').find("input[type=text]").val("女");
$("td:contains('户主姓名')").nextAll('td').find("input[type=text]").val("韩开疆");

try {
    var hzgx = $("td:contains('与户主关系')");
    if (hzgx.length == 0)
        hzgx = $("th:contains('与户主关系')");
    if (hzgx.length > 0) {
        var txt = hzgx.nextAll('td').find("input[type=text]");
        if (txt.length > 0)
            txt.val("祖孙");
        else {
            txt = hzgx.nextAll('td').find("select");
            if (txt.length > 0) {
                txt.find("option").each(function (i, e) {
                    var text = $(this).text();
                    if (text.indexOf("祖孙") >= 0) {
                        txt.val(text);
                        return false;
                    }
                });
            }
        }
    }
} catch (e) { }

$("td:contains('出生地')").nextAll('td').find("input[type=text]").val("天津市和平区");
$("td:contains('国籍')").nextAll('td').find("input[type=text]").val("中国");
$("td:contains('乳名')").nextAll('td').find("input[type=text]").eq(0).val("心恬").blur();
$("td:contains('昵称')").nextAll('td').find("input[type=text]").eq(0).val("心恬").blur();
$("td:contains('体重')").nextAll('td').find("input[type=text]").eq(0).val("18").blur();
$("td:contains('身高')").nextAll('td').find("input[type=text]").eq(0).val("110").blur();

try {
    var dijitai = $("th:contains('第几胎')").nextAll('td').find("select").eq(0);
    if (dijitai.length == 0) {
        dijitai = $("td:contains('第几胎')").nextAll('td').find("select").eq(0);
    }
    dijitai.find("option").each(function (i, e) {
        var text = $(this).text();
        if (text.indexOf("头") >= 0 || text.indexOf("一") >= 0 || text.indexOf("1") >= 0) {
            dijitai.get(0).selectedIndex = i;
            return false;
        }
    });
} catch (e) { }

try {
    //低保儿童
    var dibao = $("th:contains('低保儿童')").nextAll('td').find("select").eq(0);
    if (dibao.length == 0) {
        dibao = $("td:contains('低保儿童')").nextAll('td').find("select").eq(0);
    }
    if (dibao.length > 0) {
        dibao.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                dibao.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        dibao = $("th:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (dibao.length == 0)
            dibao = $("td:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        dibao.val("否").blur();
    }
} catch (e) { }


try {
    //残疾儿童
    var canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    if (canjiertong.length == 0) {
        canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    }
    if (canjiertong.length > 0) {
        canjiertong.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                canjiertong.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (canjiertong.length == 0)
            canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        canjiertong.val("否").blur();
    }
} catch (e) { }

try {
    var bingshi = $("th:contains('家族病史')").nextAll('td').find("select").eq(0);
    if (bingshi.length == 0) {
        bingshi = $("td:contains('家族病史')").nextAll('td').find("select").eq(0);
    }
    if (bingshi.length > 0) {
        bingshi.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
                bingshi.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        bingshi = $("th:contains('家族病史')").nextAll('td').find("input[type=text]").eq(0);
        if (bingshi.length == 0)
            bingshi = $("td:contains('家族病史')").nextAll('td').find("input[type=text]").eq(0);
        bingshi.val("无").blur();
    }
} catch (e) { }

////**************通用脚本开始***************/
$("select").find("option[value^='汉']").attr("selected", true);
$("input[type='text']").removeAttr("readonly").removeAttr("disabled");
$("body").delegate("input[type=radio]", "click", function () {
    $("input[type='text']").removeAttr("readonly").removeAttr("disabled");
});

$("input[type=radio][value='女']").attr("checked", true);
$("select").find("option[value^='女']").attr("selected", true);

$("input[type=radio][value='非农业']").attr("checked", true);
$("select").find("option[value^='非农业']").attr("selected", true);

$("input[type=radio][value='无残疾']").attr("checked", true);
$("select").find("option[value^='无残疾']").attr("selected", true).change();

$("select").find("option[value^='否']").attr("selected", true).change();

$("td").each(function (n, i) {
    var $t = $(this);
    if ($t.text() == "独生子女") {
        $t.next("td").find("select").val("是").change();
        return false;
    }
});
////**************通用脚本结束***************/
try {
    var guomintizhi = $("th:contains('过敏体质')").nextAll('td').find("select").eq(0);
    if (guomintizhi.length == 0) {
        guomintizhi = $("td:contains('过敏体质')").nextAll('td').find("select").eq(0);
    }
    if (guomintizhi.length > 0) {
        guomintizhi.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
                guomintizhi.get(0).selectedIndex = i;
                //guomintizhi.val(text);
                //guomintizhi.val(guomintizhi.options[i].value);
                return false;
            }
        });
    }
    else {
        guomintizhi = $("th:contains('过敏体质')").nextAll('td').find("input[type=text]").eq(0);
        if (guomintizhi.length == 0)
            guomintizhi = $("td:contains('过敏体质')").nextAll('td').find("input[type=text]").eq(0);
        guomintizhi.val("否").blur();
    }
} catch (e) { }


try {
    //是否有依恋物
    var yilianwu = $("th:contains('依恋物')").nextAll('td').find("select").eq(0);
    if (yilianwu.length == 0) {
        yilianwu = $("td:contains('依恋物')").nextAll('td').find("select").eq(0);
    }
    if (yilianwu.length > 0){
		yilianwu.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
                guomintizhi.get(0).selectedIndex = i;
				//          yilianwu.val(text);
                return false;
            }
        });
	}
	else {
        yilianwu = $("th:contains('依恋物')").nextAll('td').find("input[type=text]").eq(0);
        if (yilianwu.length == 0)
            yilianwu = $("td:contains('依恋物')").nextAll('td').find("input[type=text]").eq(0);
        yilianwu.val("否").blur();
    }
} catch (e) { }

try {
    //低保儿童
    var dibao = $("th:contains('低保儿童')").nextAll('td').find("select").eq(0);
    if (dibao.length == 0) {
        dibao = $("td:contains('低保儿童')").nextAll('td').find("select").eq(0);
    }
    if (dibao.length > 0) {
        dibao.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                dibao.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        dibao = $("th:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (dibao.length == 0)
            dibao = $("td:contains('低保儿童')").nextAll('td').find("input[type=text]").eq(0);
        dibao.val("否").blur();
    }
} catch (e) { }


try {
    //残疾儿童
    var canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    if (canjiertong.length == 0) {
        canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("select").eq(0);
    }
    if (canjiertong.length > 0) {
        canjiertong.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                canjiertong.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        canjiertong = $("th:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        if (canjiertong.length == 0)
            canjiertong = $("td:contains('残疾儿童')").nextAll('td').find("input[type=text]").eq(0);
        canjiertong.val("否").blur();
    }
} catch (e) { }


try {
    //残疾类型
    var canji = $("th:contains('残疾类')").nextAll('td').find("select").eq(0);
    if (canji.length == 0) {
        canji = $("td:contains('残疾类')").nextAll('td').find("select").eq(0);
    }
    if (canji.length > 0) {
        canji.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("没有") >= 0) {
                canji.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        canji = $("th:contains('残疾类')").nextAll('td').find("input[type=text]").eq(0);
        if (canji.length == 0)
            canji = $("td:contains('残疾类')").nextAll('td').find("input[type=text]").eq(0);
        canji.val("无残疾").blur();
    }
} catch (e) { }

try {
    //政治面目
    var zzmm = $("th:contains('政治面目')").nextAll('td').find("select").eq(0);
    if (zzmm.length == 0) {
        zzmm = $("td:contains('政治面目')").nextAll('td').find("select").eq(0);
    }
    if (zzmm.length > 0) {
        zzmm.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("群众") >= 0) {
                zzmm.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        zzmm = $("th:contains('政治面目')").nextAll('td').find("input[type=text]").eq(0);
        if (zzmm.length == 0)
            zzmm = $("td:contains('政治面目')").nextAll('td').find("input[type=text]").eq(0);
        zzmm.val("群众").blur();
    }
} catch (e) { }

try {
    //3岁前看护人
    var kanhu = $("th:contains('看护人')").nextAll('td').find("select").eq(0);
    if (kanhu.length == 0) {
        kanhu = $("td:contains('看护人')").nextAll('td').find("select").eq(0);
    }
    if (kanhu.length > 0) {
        kanhu.get(0).selectedIndex = 1;
    }
    else {
        kanhu = $("th:contains('看护人')").nextAll('td').find("input[type=text]").eq(0);
        if (kanhu.length == 0)
            kanhu = $("td:contains('看护人')").nextAll('td').find("input[type=text]").eq(0);
        kanhu.val("母亲").blur();
    }
} catch (e) { }

try {
    //3岁前照看人
    var zhaokan = $("th:contains('照看人')").nextAll('td').find("select").eq(0);
    if (zhaokan.length == 0) {
        zhaokan = $("td:contains('照看人')").nextAll('td').find("select").eq(0);
    }
    if (zhaokan.length > 0) {
        zhaokan.get(0).selectedIndex = 1;
    }
    else {
        zhaokan = $("th:contains('照看人')").nextAll('td').find("input[type=text]").eq(0);
        if (zhaokan.length == 0)
            zhaokan = $("td:contains('照看人')").nextAll('td').find("input[type=text]").eq(0);
        zhaokan.val("母亲").blur();
    }
} catch (e) { }

try {
    //wugong
    var wugong = $("th:contains('务工')").nextAll('td').find("select").eq(0);
    if (wugong.length == 0) {
        wugong = $("td:contains('务工')").nextAll('td').find("select").eq(0);
    }
    if (wugong.length > 0) {
        wugong.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0) {
                wugong.get(0).selectedIndex = i;
                return false;
            }
        });
    }
    else {
        wugong = $("th:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
        if (wugong.length == 0)
            wugong = $("td:contains('务工')").nextAll('td').find("input[type=text]").eq(0);
        wugong.val("否").blur();
    }
} catch (e) { }

try {
    var jhrsfz = "120105198303304521".split("");
    var tdsfz = $("td:contains('监护人身份证号码')");
    if (tdsfz.length == 0)
        tdsfz = $("th:contains('监护人身份证号码')");
    tdsfz.nextAll('td').find("input[type=text]").each(function (n, i) {
        var $t = $(this);
        $t.val(jhrsfz[n]);
    })
} catch (e) { }

try {
    var tdyegx = $("td:contains('与幼儿关系')");
    if (tdyegx.length == 0)
        tdyegx = $("th:contains('与幼儿关系')");
    var txt = tdyegx.nextAll('td').find("input[type=text]");
    if (txt.length > 0)
        txt.val("母子");
    else {
        txt = tdyegx.nextAll('td').find("select");
        if (txt.length > 0) {
            txt.find("option").each(function (n, i) {
                var text = $(this).text();
                if (text.indexOf("母子") >= 0) {
                    txt.val(text);
                    return false;
                }
            })
        }
    }
} catch (e) { }

$("td:contains('监护人姓名')").nextAll('td').find("input[type=text]").val("卢雅丹");
$("td:contains('监护人性别')").nextAll('td').find("input[type=text]").val("女");
$("td:contains('监护人学历')").nextAll('td').find("input[type=text]").val("中专");
$("td:contains('政治面目')").nextAll('td').find("input[type=text]").val("群众");

$("td:contains('监护人手机号码')").nextAll('td').find("input[type=text]").val("13821981278");
var sjh = "13821981278".split("");   //妈妈电话
$("td:contains('监护人手机号码')").nextAll('td').find("input[type=text]").each(function (n, i) {
    var $t = $(this);
    $t.val(sjh[n]);
})

$("td:contains('工作单位')").nextAll('td').find("input[type=text]").eq(0).val("天津市供热发展有限公司");
$("td:contains('监护人职务')").nextAll('td').find("input[type=text]").val("收费主管");


$("td:contains('兴趣爱好')").nextAll('td').find("input[type=text]").val("唱歌");
$("td:contains('性格')").nextAll('td').find("input[type=text]").val("外向");
$("td:contains('水果')").nextAll('td').find("input[type=text]").val("苹果");
$("td:contains('颜色')").nextAll('td').find("input[type=text]").val("蓝色");
$("td:contains('游戏')").nextAll('td').find("input[type=text]").val("躲猫猫");
$("td:contains('玩具')").nextAll('td').find("input[type=text]").val("机器猫");
$("td:contains('原因')").nextAll('td').find("input[type=text]").val("离家近");
$("td:contains('良好习惯')").nextAll('td').find("input[type=text]").val("按时吃饭");
$("td:contains('多长时间')").nextAll('td').find("input[type=text]").val("三小时");
$("td:contains('生活')").nextAll('td').find("input[type=text]").val("饮食");
$("td:contains('沟通')").nextAll('td').find("input[type=text]").val("微信");
$("td:contains('兴趣活动')").nextAll('td').find("input[type=text]").val("躲猫猫");

$("td:contains('看护人')").nextAll('td').find("input[type=checkbox]").eq(0).click();

try {
    var jiedao = $("th:contains('街道')").nextAll('td').find("select").eq(0);
    if (jiedao.length == 0)
        jiedao = $("td:contains('街道')").nextAll('td').find("select").eq(0);
    jiedao.find("option").each(function (n, i) {
        var text = $(this).text();
        if (text.indexOf("天塔") >= 0) {
            jiedao.val(text);
            return false;
        }
    })
} catch (e) { }
