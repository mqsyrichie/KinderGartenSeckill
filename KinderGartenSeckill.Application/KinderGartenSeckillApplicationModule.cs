﻿using System.Reflection;
using Abp.Modules;

namespace KinderGartenSeckill
{
    [DependsOn(typeof(KinderGartenSeckillCoreModule))]
    public class KinderGartenSeckillApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
