﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill
{
    public interface IOrderService
    {
        Task<bm_order> GetOrder(string wangNo, string orderNo);

        void Insert(bm_order order);
    }
}
