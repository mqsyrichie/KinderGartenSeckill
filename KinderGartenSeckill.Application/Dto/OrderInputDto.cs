﻿using Abp.Application.Services.Dto;
using FluentValidation.Attributes;
using KinderGartenSeckill.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Dto
{
    [Validator(typeof(OrderInputValidator))]
    public class OrderInputDto
    {
        /// <summary>
        /// 旺旺号
        /// </summary>
        public string TaobaoWangWangNo { get; set; }

        /// <summary>
        /// 淘宝订单号
        /// </summary>
        public string TaobaoOrderNo { get; set; }

        /// <summary>
        /// 订单时间
        /// </summary>
        public DateTime TaobaoOrderTime { get; set; }
    }
}
