﻿using Abp.AutoMapper;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Dto
{
    public class MobileUserDto
    {        
        public string AlipayNo { get; set; }

        public string TaobaoWangWangNo { get; set; }

        public string TaobaoOrderNo { get; set; }
    }
}
