﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using KinderGartenSeckill.Model;

namespace KinderGartenSeckill.Dto
{
    [Serializable]
    [AutoMapFrom(typeof(bm_user))]
    public class UserInputDto
    {
        public string Mobile { get; set; }

        public string Password { get; set; }

        public string TaobaoWangWangNo { get; set; }

        public string TaobaoOrderNo { get; set; }

        public string TaobaoOrderTime { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string FirstKindergarten { get; set; }

        public string FirstRegistrationTime { get; set; }

        public string FirstUrl { get; set; }

        public string SecondKindergarten { get; set; }
        public string SecondRegistrationTime { get; set; }

        public string SecondUrl { get; set; }

        public string Remark { get; set; }

        public string AlipayNo { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public UserType UserType { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string VerificationCode { get; set; }

        /// <summary>
        /// 报名网站登录账号
        /// </summary>
        public string RegistWebAccount { get; set; }

        /// <summary>
        /// 报名网站登录密码
        /// </summary>
        public string RegistWebPassword { get; set; }
    }
}
