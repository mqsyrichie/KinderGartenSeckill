﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace KinderGartenSeckill.Dto
{
    [Serializable]
    public class UserListDto : EntityDto<int>
    {
        /// <summary>
        /// 参与人姓名
        /// </summary>
        public string ActorNames { get; set; }

        public string UserName { get; set; }

        public string Mobile { get; set; }

        public string AlipayNo { get; set; }

        public string TaobaoWangWangNo { get; set; }

        public string TaobaoOrderNo { get; set; }

        public string TaobaoOrderTime { get; set; }

        public string FirstKindergarten { get; set; }

        public DateTime? FirstRegistrationTime { get; set; }

        public string FirstRegistrationTimeFormat
        {
            get
            {
                if (!FirstRegistrationTime.HasValue) return "";
                return FirstRegistrationTime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public string FirstUrl { get; set; }

        public string SecondKindergarten { get; set; }

        public System.DateTime? SecondRegistrationTime { get; set; }

        public string SecondRegistrationTimeFormat
        {
            get
            {
                if (!SecondRegistrationTime.HasValue) return "";
                return SecondRegistrationTime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public string SecondUrl { get; set; }

        public string Remark { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string VerificationCode { get; set; }

        /// <summary>
        /// 报名网站登录账号
        /// </summary>
        public string RegistWebAccount { get; set; }

        /// <summary>
        /// 报名网站登录密码
        /// </summary>
        public string RegistWebPassword { get; set; }

        public decimal Amount { get; set; }
    }
}
