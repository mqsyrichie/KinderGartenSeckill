﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace KinderGartenSeckill.Dto
{
    public class CommonOutputDto<T>
    {
        public bool IsLogin {
            get { return HttpContext.Current.User.Identity.IsAuthenticated; }
        }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }

        public string Url { get; set; }
    }

    public class CommonOutputDto : CommonOutputDto<string> {

    }
}
