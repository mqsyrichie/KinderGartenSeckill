﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using KinderGartenSeckill.Model;
using System;

namespace KinderGartenSeckill.Dto
{
    [Serializable]
    [AutoMapFrom(typeof(bm_user))]
    public class ChildAssignmentDto : EntityDto<int>
    {
        public string UserName { get; set; }
    }
}
