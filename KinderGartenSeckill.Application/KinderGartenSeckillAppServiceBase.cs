﻿using Abp.Application.Services;

namespace KinderGartenSeckill
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class KinderGartenSeckillAppServiceBase : ApplicationService
    {
        protected KinderGartenSeckillAppServiceBase()
        {
            LocalizationSourceName = KinderGartenSeckillConsts.LocalizationSourceName;
        }
    }
}