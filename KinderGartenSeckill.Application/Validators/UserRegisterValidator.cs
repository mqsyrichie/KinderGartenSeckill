﻿using Abp.Domain.Repositories;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Castle.Windsor;
using Castle.DynamicProxy;
using Abp.Dependency;

namespace KinderGartenSeckill.Validators
{
    public class UserRegisterValidator : BaseValidator<UserInputDto>
    {
        private readonly IRepository<bm_order> _orderRepository;
        private readonly IRepository<bm_user> _userRepository;

        //public UserRegisterValidator(IRepository<bm_order> orderRepository, IRepository<bm_user> userRepository)
        public UserRegisterValidator()
        {
            _orderRepository = IocManager.Instance.Resolve<IRepository<bm_order>>();
            _userRepository= IocManager.Instance.Resolve<IRepository<bm_user>>();  

            RuleFor(x => x.Mobile).NotEmpty().WithMessage("手机号不能为空");
            RuleFor(x => x.Password).NotEmpty().WithMessage("密码不能为空");

            //RuleFor(x => x.TaobaoWangWangNo).NotEmpty().WithMessage("请输入淘宝旺旺号");
            //RuleFor(x => x.TaobaoOrderNo).NotEmpty().WithMessage("请输入淘宝订单号");
            //RuleFor(x => x.TaobaoOrderTime).NotEmpty().WithMessage("请输入淘宝订单时间");


            Custom(x =>
            {
                if (!string.IsNullOrWhiteSpace(x.Mobile))
                {
                    if (_userRepository.FirstOrDefault(p => p.LoginName.ToUpper() == x.Mobile.ToUpper() && p.UserType == (int)x.UserType) != null)
                        return new ValidationFailure("LoginName", "用户已存在，请更换！");
                }
                return null;
            });

            Custom(x => {
                if (!string.IsNullOrWhiteSpace(x.TaobaoOrderNo) && !string.IsNullOrWhiteSpace(x.TaobaoWangWangNo))
                {
                    var count = _userRepository.Count(p => p.TaobaoWangWangNo == x.TaobaoWangWangNo || p.TaobaoOrderNo == x.TaobaoOrderNo);
                    if (count > 0) {
                        return new ValidationFailure("TaobaoOrderNo", "淘宝订单号已存在或输入错误");
                    }
                }
                return null;
            });
        }
    }
}
