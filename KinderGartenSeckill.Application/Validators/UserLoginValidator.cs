﻿using Abp.Domain.Repositories;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Castle.Windsor;
using Castle.DynamicProxy;
using Abp.Dependency;

namespace KinderGartenSeckill.Validators
{
    public class UserLoginValidator : BaseValidator<UserLoginDto>
    {
        private readonly IRepository<bm_order> _orderRepository;
        private readonly IRepository<bm_user> _userRepository;

        //public UserRegisterValidator(IRepository<bm_order> orderRepository, IRepository<bm_user> userRepository)
        public UserLoginValidator()
        {

            RuleFor(x => x.LoginName).NotEmpty().WithMessage("登录名不能为空");
            RuleFor(x => x.Password).NotEmpty().WithMessage("密码不能为空");
        }
    }
}
