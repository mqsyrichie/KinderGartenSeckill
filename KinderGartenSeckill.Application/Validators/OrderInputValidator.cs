﻿using FluentValidation;
using KinderGartenSeckill.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Validators
{
    public class OrderInputValidator : BaseValidator<OrderInputDto>
    {
        public OrderInputValidator()
        {
            RuleFor(x => x.TaobaoWangWangNo).NotEmpty().WithMessage("请输入旺旺号");
            RuleFor(x => x.TaobaoOrderNo).NotEmpty().WithMessage("请输入淘宝订单号");
            RuleFor(x => x.TaobaoOrderTime).NotEmpty().WithMessage("订单时间");
        }
    }
}
