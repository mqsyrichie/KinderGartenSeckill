﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using FluentValidation;
using FluentValidation.Results;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Validators
{
    public class UserCompleteValidator:BaseValidator<UserCompleteDto>
    {
        private readonly IRepository<bm_user> _userRepository;
        private readonly IRepository<bm_order> _orderRepository;

        public UserCompleteValidator()
        {
            _orderRepository = IocManager.Instance.Resolve<IRepository<bm_order>>();
            _userRepository = IocManager.Instance.Resolve<IRepository<bm_user>>();
            
            RuleFor(x => x.UserName).NotEmpty().WithMessage("请输入联系姓名");
            RuleFor(x => x.AlipayNo).NotEmpty().WithMessage("请输入支付宝账号");
            RuleFor(x => x.TaobaoWangWangNo).NotEmpty().WithMessage("请输入淘宝旺旺号");
            RuleFor(x => x.TaobaoOrderNo).NotEmpty().WithMessage("请输入淘宝订单号");
            RuleFor(x => x.FirstKindergarten).NotEmpty().WithMessage("请输入首选幼儿园");
            //RuleFor(x => x.FirstRegistrationTime).NotEmpty().WithMessage("请输入报名时间");

            Custom(x => {
                if (!string.IsNullOrWhiteSpace(x.TaobaoOrderNo))
                {
                    var user = _userRepository.FirstOrDefault(p => p.TaobaoOrderNo == x.TaobaoOrderNo);
                    if (user == null)
                        return null;
                    
                    if(user.TaobaoWangWangNo != user.TaobaoWangWangNo)
                    {
                        return new ValidationFailure("TaobaoOrderNo", "淘宝订单号已存在或输入错误");
                    }
                }
                return null;
            });
        }
    }
}
