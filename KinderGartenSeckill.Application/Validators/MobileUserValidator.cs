﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using FluentValidation;
using FluentValidation.Results;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Validators
{
    public class MobileUserValidator: BaseValidator<MobileUserDto>
    {
        private readonly IRepository<bm_user> _userRepository;
        public MobileUserValidator()
        {
            _userRepository = IocManager.Instance.Resolve<IRepository<bm_user>>();

            RuleFor(x => x.AlipayNo).NotEmpty().WithMessage("支付宝账号");
            RuleFor(x => x.TaobaoWangWangNo).NotEmpty().WithMessage("请输入淘宝旺旺号");
            RuleFor(x => x.TaobaoOrderNo).NotEmpty().WithMessage("请输入淘宝订单号");

            Custom(x => {
                if (!string.IsNullOrWhiteSpace(x.TaobaoOrderNo) && !string.IsNullOrWhiteSpace(x.TaobaoWangWangNo))
                {
                    var user = _userRepository.FirstOrDefault(p => p.TaobaoOrderNo == x.TaobaoOrderNo);
                    if (user.TaobaoWangWangNo != x.TaobaoWangWangNo)
                    {
                        return new ValidationFailure("TaobaoOrderNo", "淘宝订单号已存在或输入错误");
                    }
                }
                return null;
            });
        }
    }
}
