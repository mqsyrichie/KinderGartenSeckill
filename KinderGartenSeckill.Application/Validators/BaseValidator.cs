﻿using FluentValidation;

namespace KinderGartenSeckill.Validators
{
    public abstract class BaseValidator<T> : AbstractValidator<T> where T : class
    {
        protected BaseValidator()
        {
            PostInitialize();
        }

        protected virtual void PostInitialize()
        {

        }
    }
}
