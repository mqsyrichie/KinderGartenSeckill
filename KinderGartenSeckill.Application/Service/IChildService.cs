﻿using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Service
{
    public interface IChildService 
    {
        bm_user GetUser(int userId);

        bm_child GetChild(int userId);

        bm_child_parents GetParents(int userId);

        Task<bm_child> InsertOrUpdateChild(bm_child child);

        Task<CommonOutputDto<bm_child>> InsertOrUpdateChildForMobile(bm_child child);

        Task<bm_child_parents> InsertOrUpdateChildParents(bm_child_parents parents);

        Task<CommonOutputDto<bm_child_parents>> InsertOrUpdateChildParentsForMobile(bm_child_parents parents);
    }
}
