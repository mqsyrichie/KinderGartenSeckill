﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinderGartenSeckill.Model;

namespace KinderGartenSeckill.Service
{
    public interface IKindergartenService
    {
        List<bm_kindergarten> GetKindergartens(int districtId = 0);
    }
}
