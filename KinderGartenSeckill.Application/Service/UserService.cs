﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Service
{
    public class UserService : KinderGartenSeckillAppServiceBase, IUserService
    {
        private readonly IRepository<bm_user, int> _userRepository; //原生自带
        private readonly IRepository<bm_user_child_mapping, int> _userAssignmentRepository; //原生自带

        public UserService(IRepository<bm_user, int> userRepository,
            IRepository<bm_user_child_mapping, int> userAssignmentRepository)
        {
            _userRepository = userRepository;
            _userAssignmentRepository = userAssignmentRepository;
        }

        public async Task<bm_user> GetUser(string loginName, string password)
        {
            password = password.ToMd5();
            return await _userRepository.FirstOrDefaultAsync(p => p.LoginName.ToUpper() == loginName.ToUpper() && p.Password.ToUpper() == password.ToUpper());
        }

        public async Task<bm_user> GetUserByMobile(string mobile)
        {
            return await _userRepository.FirstOrDefaultAsync(x => x.Mobile == mobile);
        }

        public bm_user GetUser(int id)
        {
            return _userRepository.FirstOrDefault(p => p.Id == id);
        }

        public List<UserListDto> GetUsers(int userId = 0, string username = null,
            string mobile = null,
            string taobaono = null,
            string garden = null,
            string ftime = null,
            string ttime = null,
            int actorUserId = 0)
        {
            var list = new List<UserListDto>();
            IQueryable<bm_user> user = _userRepository.GetAll().Where(x => x.UserType == (int)UserType.Customer);

            if (!string.IsNullOrWhiteSpace(username))
            {
                user = user.Where(x => x.UserName == username);
            }

            if (!string.IsNullOrWhiteSpace(mobile))
            {
                user = user.Where(x => x.Mobile == mobile);
            }

            if (!string.IsNullOrWhiteSpace(taobaono))
            {
                user = user.Where(x => x.TaobaoWangWangNo == taobaono);
            }

            if (!string.IsNullOrWhiteSpace(garden))
            {
                user = user.Where(x => x.FirstKindergarten.Contains(garden) || x.SecondKindergarten.Contains(garden));
            }

            if (!string.IsNullOrWhiteSpace(ftime) && string.IsNullOrWhiteSpace(ttime))
            {
                var ft = Convert.ToDateTime(ftime);
                user = user.Where(x => x.FirstRegistrationTime >= ft || x.SecondRegistrationTime >= ft);
            }
            if (string.IsNullOrWhiteSpace(ftime) && !string.IsNullOrWhiteSpace(ttime))
            {
                var tt = Convert.ToDateTime(ttime);
                user = user.Where(x => x.FirstRegistrationTime <= tt || x.SecondRegistrationTime <= tt);
            }
            if (!string.IsNullOrWhiteSpace(ftime) && !string.IsNullOrWhiteSpace(ttime))
            {
                var ft = Convert.ToDateTime(ftime);
                var tt = Convert.ToDateTime(ttime);
                user = user.Where(x => (x.FirstRegistrationTime >= ft && x.FirstRegistrationTime <= tt) || (x.SecondRegistrationTime >= ft && x.SecondRegistrationTime <= tt));
            }

            if (userId > 0)
            {
                var mappingUserIds = _userAssignmentRepository.GetAll().Where(p => p.UserId == userId)
                    .Select(p => p.ChildUserId);
                user = user.Where(p => mappingUserIds.Contains(p.Id));
            }

            var model = user.ToList();
            //var actorName = "";
            //if (actorUserId > 0)
            //{
            //    var actor = _userRepository.FirstOrDefault(p => p.Id == actorUserId);
            //    actorName = actor?.UserName;
            //}
            model.ForEach(p => list.Add(new UserListDto
            {
                //ActorName = actorName,
                Id = p.Id,
                UserName = p.UserName,
                Mobile = p.Mobile,
                AlipayNo = p.AlipayNo,
                TaobaoWangWangNo = p.TaobaoWangWangNo,
                TaobaoOrderNo = p.TaobaoOrderNo,
                TaobaoOrderTime = p.TaobaoOrderTime,
                FirstKindergarten = p.FirstKindergarten,
                FirstRegistrationTime = p.FirstRegistrationTime,
                FirstUrl = p.FirstUrl,
                SecondKindergarten = p.SecondKindergarten,
                SecondRegistrationTime = p.SecondRegistrationTime,
                SecondUrl = p.SecondUrl,
                Remark = p.Remark,
                VerificationCode = p.VerificationCode,
                RegistWebAccount = p.RegistWebAccount,
                RegistWebPassword = p.RegistWebPassword,
                Amount = p.Amount
            }));
            list.ForEach(p =>
            {
                var tempMappings = _userAssignmentRepository.GetAll().Where(x => x.ChildUserId == p.Id).Select(x => x.UserId);
                if (tempMappings.Any())
                {
                    var q = _userRepository.GetAll().Where(x => tempMappings.Contains(x.Id));
                    var tempUsers = q.Select(x => x.UserName).ToArray();
                    p.ActorNames = String.Join(",", tempUsers);
                }
            });
            list = list.OrderBy(x => x.FirstRegistrationTime.GetValueOrDefault()).ToList();
            return list;
        }


        public async Task<CommonOutputDto<bm_user>> Login(UserLoginDto loginDto)
        {
            var comm = new CommonOutputDto<bm_user>();
            var user = await GetUser(loginDto.LoginName, loginDto.Password);
            if (user == null)
            {
                comm.Message = "登录名或密码错误";
            }
            else
            {
                comm.IsSuccess = true;
                comm.Data = user;
            }
            return comm;
        }


        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="user"></param>
        public async Task<int> Insert(UserInputDto user)
        {

            var currUser = new bm_user
            {
                LoginName = user.Mobile,
                UserName = user.UserName,
                Mobile = user.Mobile,
                Password = user.Password.ToMd5(),
                AlipayNo = user.AlipayNo,
                TaobaoWangWangNo = user.TaobaoWangWangNo,
                TaobaoOrderNo = user.TaobaoOrderNo,
                TaobaoOrderTime = user.TaobaoOrderTime,
                Email = user.Email,
                FirstKindergarten = user.FirstKindergarten,
                FirstRegistrationTime = string.IsNullOrWhiteSpace(user.FirstRegistrationTime) ? new DateTime?() : DateTime.Parse(user.FirstRegistrationTime),
                SecondKindergarten = user.SecondKindergarten,
                SecondRegistrationTime = string.IsNullOrWhiteSpace(user.SecondRegistrationTime) ? new DateTime?() : DateTime.Parse(user.SecondRegistrationTime),
                Remark = user.Remark,
                UserType = (int)user.UserType,
                CreateDate = DateTime.Now,
                Amount = string.IsNullOrWhiteSpace(user.Amount) ? 0 : decimal.Parse(user.Amount)
            };
            await _userRepository.InsertAsync(currUser);
            await CurrentUnitOfWork.SaveChangesAsync();
            return currUser.Id;
        }

        public List<bm_user> GetList()
        {
            var list = _userRepository.GetAllList().Where(x => x.UserType != (int)UserType.Customer);
            list = list.OrderBy(x => x.TaobaoOrderTime).OrderBy(x => x.FirstRegistrationTime.GetValueOrDefault());

            return list.ToList();
        }

        public List<bm_user> GetActors()
        {
            var list = _userRepository.GetAllList().Where(x => x.UserType == (int)UserType.Actor);
            list = list.OrderBy(x => x.CreateDate);
            return list.ToList();
        }

        public bm_user Update(bm_user user)
        {
            return _userRepository.Update(user);
        }

        /// <summary>
        /// 查询未分配的用户列表
        /// </summary>
        /// <returns></returns>
        public List<ChildAssignmentDto> GetValidUsers()
        {
            var mapList = from map in _userAssignmentRepository.GetAll()
                          select map.ChildUserId;

            var validList = from user in _userRepository.GetAll().Where(p => p.UserType == 1)
                            where !mapList.Contains(user.Id)
                            select user;
            var items = new List<ChildAssignmentDto>();
            validList.ToList().ForEach(p =>
            {
                items.Add(new ChildAssignmentDto
                {
                    Id = p.Id,
                    UserName = p.UserName
                });
            });
            return items;
        }

        /// <summary>
        /// 查询操作员用户列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ChildAssignmentDto>> GetOperators()
        {
            var users = await _userRepository.GetAllListAsync(p => p.UserType == 2);
            var items = new List<ChildAssignmentDto>();
            users.ToList().ForEach(p =>
            {
                items.Add(new ChildAssignmentDto
                {
                    Id = p.Id,
                    UserName = p.UserName
                });
            });
            return items;
        }

        /// <summary>
        /// 根据用户查询已分配的人
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<ChildAssignmentDto> GetSelected(int userId)
        {
            var users = (from u in _userRepository.GetAll()
                         join m in _userAssignmentRepository.GetAll().Where(p => p.UserId == userId)
                             on u.Id equals m.ChildUserId
                         select u);
            var items = new List<ChildAssignmentDto>();
            users.ToList().ForEach(p =>
            {
                items.Add(new ChildAssignmentDto
                {
                    Id = p.Id,
                    UserName = p.UserName
                });
            });
            return items;
        }

        /// <summary>
        /// 分配用户
        /// </summary>
        /// <param name="id"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        public async Task<bool> Assignment(int id, List<int> users)
        {
            try
            {
                await _userAssignmentRepository.DeleteAsync(p => p.UserId == id);
                foreach (var user in users)
                    await
                        _userAssignmentRepository.InsertAsync(new bm_user_child_mapping
                        {
                            UserId = id,
                            ChildUserId = user
                        });
                await CurrentUnitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<bm_user> GetUsers(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var items = _userRepository.GetAll().Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return items.ToList();
        }
    }
}
