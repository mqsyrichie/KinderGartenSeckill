﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using KinderGartenSeckill.Model;

namespace KinderGartenSeckill.Service
{
    public class KindergartenService : KinderGartenSeckillAppServiceBase, IKindergartenService
    {
        private readonly IRepository<bm_kindergarten, int> _kindergartenRepository; //原生自带
        public KindergartenService(IRepository<bm_kindergarten, int> kindergartenRepository)
        {
            _kindergartenRepository = kindergartenRepository;
        }

        public List<bm_kindergarten> GetKindergartens(int districtId = 0)
        {
            var query = _kindergartenRepository.GetAllList().Where(p => p.Status);
            if (districtId > 0)
                query = query.Where(p => p.DistrictId == districtId);
            return query.ToList();
        }
    }
}
