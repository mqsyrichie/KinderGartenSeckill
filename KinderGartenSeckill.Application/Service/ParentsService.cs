﻿using Abp.Domain.Repositories;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Service
{
    public class ParentsService: KinderGartenSeckillAppServiceBase, IParentsService
    {
        private readonly IRepository<bm_child, int> _childRepository; //原生自带
        private readonly IRepository<bm_child_parents, int> _parentsRepository; //原生自带

        public ParentsService(IRepository<bm_child, int> childRepository, IRepository<bm_child_parents, int> parentsRepository)
        {
            _childRepository = childRepository;
            _parentsRepository = parentsRepository;
        }

        public bm_child_parents GetParents(int userId)
        {
            var model = _parentsRepository.FirstOrDefault(x => x.UserId == userId);
            return model == null ? new bm_child_parents { UserId = userId } : model;
        }

        public bm_child GetChild(int userId)
        {
            var model = _childRepository.FirstOrDefault(x => x.UserId == userId);
            return model == null ? new bm_child { UserId = userId } : model;
        }

        public void Insert(bm_child_parents parents)
        { }
    }
}
