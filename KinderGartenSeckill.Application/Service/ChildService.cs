﻿using Abp.Domain.Repositories;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Service
{
    public class ChildService : KinderGartenSeckillAppServiceBase, IChildService
    {
        private readonly IRepository<bm_child, int> _childRepository; //原生自带
        private readonly IRepository<bm_child_parents, int> _parentsRepository; //原生自带
        private readonly IRepository<bm_user, int> _userRepository; //原生自带

        public ChildService(IRepository<bm_child, int> childRepository, IRepository<bm_child_parents, int> parentsRepository, IRepository<bm_user, int> userRepository)
        {
            _childRepository = childRepository;
            _parentsRepository = parentsRepository;
            _userRepository = userRepository;
        }

        public bm_user GetUser(int userId)
        {
            var child = _userRepository.FirstOrDefault(p => p.Id == userId);
            return child == null ? new bm_user { Id = userId } : child;
        }

        public bm_child GetChild(int userId)
        {
            var child = _childRepository.FirstOrDefault(p => p.UserId == userId);
            return child == null ? new bm_child { UserId = userId } : child;
        }

        public bm_child_parents GetParents(int userId)
        {
            var parents =  _parentsRepository.FirstOrDefault(p => p.UserId == userId);
            return parents==null? new bm_child_parents { UserId = userId } : parents;
        }

        public async Task<bm_child> InsertOrUpdateChild(bm_child child)
        {
            if (child.Id == 0)
                await _childRepository.InsertAsync(child);
            else
                await _childRepository.UpdateAsync(child);
            await CurrentUnitOfWork.SaveChangesAsync();
            return _childRepository.Get(child.Id);
        }

        public async Task<CommonOutputDto<bm_child>> InsertOrUpdateChildForMobile(bm_child child)
        {
            var comm = new CommonOutputDto<bm_child>();
            try
            {
                if (child.Id == 0)
                    await _childRepository.InsertAsync(child);
                else
                    await _childRepository.UpdateAsync(child);
                await CurrentUnitOfWork.SaveChangesAsync();

                comm.IsSuccess = true;
                comm.Message = "提交成功";
                comm.Data = _childRepository.Get(child.Id);
            }
            catch (Exception)
            {
                comm.Message = "保存失败, 请联系管理员";
            }
            return comm;
        }

        public async Task<bm_child_parents> InsertOrUpdateChildParents(bm_child_parents parents)
        {
            if (parents.Id == 0)
                await _parentsRepository.InsertAsync(parents);
            else
                await _parentsRepository.UpdateAsync(parents);
            await CurrentUnitOfWork.SaveChangesAsync();
            return _parentsRepository.Get(parents.Id);
        }

        public async Task<CommonOutputDto<bm_child_parents>> InsertOrUpdateChildParentsForMobile(bm_child_parents parents)
        {
            var comm = new CommonOutputDto<bm_child_parents>();
            try
            {
                if (parents.Id == 0)
                    await _parentsRepository.InsertAsync(parents);
                else
                    await _parentsRepository.UpdateAsync(parents);
                await CurrentUnitOfWork.SaveChangesAsync();

                comm.IsSuccess = true;
                comm.Message = "提交成功";
                comm.Data = _parentsRepository.Get(parents.Id);
            }
            catch (Exception)
            {
                comm.Message = "保存失败, 请联系管理员";
            }

            return comm;
        }
    }
}
