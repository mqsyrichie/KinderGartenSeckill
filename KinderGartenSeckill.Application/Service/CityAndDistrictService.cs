﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using KinderGartenSeckill.Model;

namespace KinderGartenSeckill.Service
{
    public class CityAndDistrictService : KinderGartenSeckillAppServiceBase, ICityAndDistrictService
    {
        private readonly IRepository<CityAndDistrict, int> _cityAndDistrictRepository; //原生自带

        public CityAndDistrictService(IRepository<CityAndDistrict, int> cityAndDistrictRepository)
        {
            _cityAndDistrictRepository = cityAndDistrictRepository;
        }

        public List<CityAndDistrict> GetCityAndDistricts(string name)
        {
            var city = _cityAndDistrictRepository.FirstOrDefault(x => x.Name == name);
            if (city == null)
                return new List<CityAndDistrict>();

            var list = _cityAndDistrictRepository.GetAll().Where(x => x.ParentId == city.Id).ToList();
            return list;
        }
    }
}
