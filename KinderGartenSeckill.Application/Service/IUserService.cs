﻿using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Service
{
    public interface IUserService
    {

        Task<bm_user> GetUser(string loginName, string password);

        bm_user GetUser(int id);

        Task<bm_user> GetUserByMobile(string mobile);

        Task<CommonOutputDto<bm_user>> Login(UserLoginDto loginDto);

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="user"></param>
        Task<int> Insert(UserInputDto user);

        List<bm_user> GetList();

        List<bm_user> GetActors();

        bm_user Update(bm_user user);

        List<UserListDto> GetUsers(int userId = 0, string username = null,
            string mobile = null,
            string taobaono = null,
            string garden = null,
            string ftime = null,
            string ttime = null,
            int actorUserId = 0);

        /// <summary>
        /// 查询未分配的用户列表
        /// </summary>
        /// <returns></returns>
        List<ChildAssignmentDto> GetValidUsers();


        /// <summary>
        /// 查询操作员用户列表
        /// </summary>
        /// <returns></returns>
        Task<List<ChildAssignmentDto>> GetOperators();


        /// <summary>
        /// 根据用户查询已分配的人
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<ChildAssignmentDto> GetSelected(int userId);

        /// <summary>
        /// 分配用户
        /// </summary>
        /// <param name="id"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        Task<bool> Assignment(int id, List<int> users);

        List<bm_user> GetUsers(int pageIndex = 0, int pageSize = int.MaxValue);
    }
}
