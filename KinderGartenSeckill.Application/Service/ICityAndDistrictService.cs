﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinderGartenSeckill.Model;

namespace KinderGartenSeckill.Service
{
    public interface ICityAndDistrictService
    {
        List<CityAndDistrict> GetCityAndDistricts(string name);
    }
}
