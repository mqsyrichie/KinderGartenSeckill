﻿using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Service
{
    public interface IParentsService
    {
        bm_child_parents GetParents(int userId);

        bm_child GetChild(int userId);

        void Insert(bm_child_parents parents);
    }
}
