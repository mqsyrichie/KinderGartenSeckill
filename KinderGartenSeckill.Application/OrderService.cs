﻿using Abp.Domain.Repositories;
using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill
{
    public class OrderService : KinderGartenSeckillAppServiceBase,IOrderService
    {
        private readonly IRepository<bm_order, int> _orderRepository; //原生自带

        public OrderService(IRepository<bm_order, int> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<bm_order> GetOrder(string wangNo, string orderNo)
        {
            return await _orderRepository.FirstOrDefaultAsync(p => p.TaobaoWangWangNo.ToUpper() == wangNo.ToUpper() && p.TaobaoOrderNo.ToUpper() == orderNo);
        }

        public void Insert(bm_order order)
        {
            _orderRepository.Insert(order);
        }
    }
}
