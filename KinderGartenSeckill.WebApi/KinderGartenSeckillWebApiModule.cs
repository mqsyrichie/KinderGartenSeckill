﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Configuration.Startup;
using Abp.Modules;
using Abp.WebApi;

namespace KinderGartenSeckill
{
    [DependsOn(typeof(AbpWebApiModule), typeof(KinderGartenSeckillApplicationModule))]
    public class KinderGartenSeckillWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(KinderGartenSeckillApplicationModule).Assembly, "app")
                .Build();
        }
    }
}
