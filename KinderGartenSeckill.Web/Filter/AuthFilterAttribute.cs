﻿using KinderGartenSeckill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace KinderGartenSeckill.Web.Filter
{
    public class AuthFilterAttribute : AuthorizeAttribute
    {
        public AuthFilterAttribute(params UserType[] userTypes)
        {
            AllowUserType = UserType.Customer;
            AllowUserTypes = userTypes.ToList();
        }

        /// <summary>
        /// 允许访问的用户类型
        /// 0 管理员
        /// 1 注册用户
        /// </summary>
        public UserType AllowUserType { get; set; }

        public List<UserType> AllowUserTypes { get; set; } = new List<UserType>();

        public UserType CurrUserType
        {
            get
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    return UserType.Customer;
                }

                FormsIdentity id;
                try
                {
                    id = HttpContext.Current.User.Identity as FormsIdentity;
                }
                catch (Exception)
                {
                    return UserType.Customer;
                }
                return (UserType)Enum.Parse(typeof(UserType), id.Ticket.UserData);
            }
        }

        /// <summary>
        /// 验证登录的会员是否有权限访问
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!AllowUserTypes.Exists(p => p == AllowUserType))
            {
                AllowUserTypes.Add(AllowUserType);
            }

            var context = filterContext.HttpContext;
            if (!context.User.Identity.IsAuthenticated)
            {
                //var logger = LogManager.GetLogger("AuthFilterAttribute");
                //var referUrl = filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString();
                //logger.Error("222---请求地址：" + context.Request.RawUrl + "-----来源：" + referUrl);

                if (context.Request.IsAjaxRequest())
                {
                    filterContext.Result = new JsonResult()
                    {
                        Data = new
                        {
                            isSuccess = false,
                            message = "您没有权限访问该页面",
                            isLogin = false,
                            url = FormsAuthentication.LoginUrl
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    base.OnAuthorization(filterContext);
                }
            }
            else
            {
                //var checkResult = false;
                //var userType = CurrUserType;
                var checkResult = AllowUserTypes.Any(p => p == CurrUserType);

                //if (AllowUserType == UserType.Admin)
                //{
                //    checkResult = userType == UserType.Admin;
                //}
                //else if (AllowUserType == UserType.Customer)
                //{
                //    checkResult = userType == UserType.Customer;
                //}

                if (!checkResult)
                {
                    if (context.Request.IsAjaxRequest())
                    {
                        filterContext.Result = new JsonResult()
                        {
                            Data = new
                            {
                                IsSuccess = false,
                                Message = "您未登录或已超时，请重新登录后再操作！",
                                IsLogin = false,
                                Url = FormsAuthentication.LoginUrl + "?returnUrl=" + context.Request.UrlReferrer
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                    else
                    {
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                }
            }
            //base.OnAuthorization(filterContext);
        }
    }
}