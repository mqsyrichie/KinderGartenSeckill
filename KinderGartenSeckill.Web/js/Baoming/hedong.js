javascript: (function (c) { function e(a) { var b = a.document, f = a.jQuery, g = function (a) { h.forEach(function (b) { a["on" + b] = null; f && f(a).unbind(b); try { /frame/i.test(a.tagName) && e(a.contentWindow) } catch (c) { } }) };[a, b].forEach(g); a = 0; for (var b = b.all, c = b.length; a < c; a++) { var d = b[a]; d && 1 === d.nodeType && g(d) } } var h = "contextmenu dragstart mouseup copy beforecopy selectstart select keydown keyup keypress".split(" "); e(c); $("[onpaste='return false']").removeAttr("onpaste"); $("body").unbind("paste"); })(window);
var ele = ["td", "th", "label", "li", "ul"];
//身份证号
try {
    var sfzhm = "120102201603310160";
    var a;
    $.each(ele,
        function (i, n) {
            a = $(n + ":contains('儿童身份证号'):last");
            if (a.length == 0) a = $(n + ":contains('儿童身份'):last");
            if (a.length == 0) a = $(n + ":contains('幼儿身份证号'):last");
            if (a.length == 0) a = $(n + ":contains('幼儿身份'):last");
            if (a.length == 0) a = $(n + ":contains('身份证号'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
}
catch (e) { }

//幼儿姓名
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('孩子姓名'):last");
        if (a.length == 0) a = $(n + ":contains('孩子名字'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿姓名'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿名字'):last");
        if (a.length == 0) a = $(n + ":contains('儿童名字'):last");
        if (a.length == 0) a = $(n + ":contains('儿童姓名'):last");
        if (a.length > 0) return false;
    });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val("李欣忆");
    }
}
catch (e) { }

//民族
try {
    var mz = "汉";
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('民族'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val(mz);
    }
    else {
        //select
        var sel = parent.find("select");
        if (sel.length == 0) sel = parent.nextAll().find("select");
        if (sel.length >= 0) {
            var val;
            sel.find("option").each(function (i, n) {
                var $t = $(this);
                if ($t.text().indexOf(mz) > -1) {
                    $t.attr("selected", "selected");
                    val = $t.val();
                    return false;
                }
            })
            sel.val(val);
        } else {
            var mzCombo = a.find(".combobox-f");
            if (mzCombo.length == 0) a = parent.find(".combobox-f");
            if (mzCombo.length == 0) a = parent.nextAll().find(".combobox-f");
            if (mzCombo.length > 0) {
                var value = 0;
                var data = mzCombo.eq(0).combobox("getData");
                var comOption = mzCombo.eq(0).combobox("options");

                var valueField = comOption.valueField;
                var textField = comOption.textField;
                $.each(data, function (i, n) {
                    if (n[textField].indexOf("汉") != -1) {
                        value = n[valueField];
                        return false;
                    }
                });
                mzCombo.eq(0).combobox("setValue", value);
            }
        }
    }
}
catch (e) { }

//幼儿园
try {
    var yey;
    $.each(ele, function (i, n) {
        yey = $(n + ":contains('幼儿园选择'):last");
        if (yey.length == 0) yey = $(n + ":contains('幼儿园'):last");
        if (yey.length == 0) yey = $(n + ":contains('入园意向'):last");
        if (yey.length == 0) yey = $(n + ":contains('入园'):last");
        if (yey.length > 0) return false;
    });
    var parent = yey.parent();
    var yeyxz = yey.find(".combobox-f");
    if (yeyxz.length == 0) yeyxz = parent.find(".combobox-f");
    if (yeyxz.length == 0) yeyxz = parent.nextAll().find(".combobox-f");
    if (yeyxz.length > 0) {
        var value = 0;
        var data = yeyxz.eq(0).combobox("getData");
        var comOption = yeyxz.eq(0).combobox("options");

        var valueField = comOption.valueField;
        var textField = comOption.textField;

        $.each(data, function (i, n) {
            if (n[textField].indexOf("一幼") != -1)  //幼儿园
            {
                value = n[valueField];
                return false;
            }
        });
        yeyxz.eq(0).combobox("setValue", value);
    }
} catch (err) { }

//园址
try {
    var yz;
    $.each(ele, function (i, n) {
        yz = $(n + ":contains('园址'):last");
        if (yz.length == 0) yz = $(n + ":contains('园区'):last");
        if (yz.length > 0) return false;
    });
    var parent = yz.parent();
    var yeyxz = yz.find(".combobox-f");
    if (yeyxz.length == 0) yeyxz = parent.find(".combobox-f");
    if (yeyxz.length == 0) yeyxz = parent.nextAll().find(".combobox-f");
    if (yeyxz.length > 0) {
        var value = 0;
        var data = yeyxz.eq(0).combobox("getData");
        var comOption = yeyxz.eq(0).combobox("options");

        var valueField = comOption.valueField;
        var textField = comOption.textField;

        $.each(data, function (i, n) {
            if (n[textField].indexOf("总") != -1)  //园区
             {
                value = n[valueField];
                return false;
            }
        });
        yeyxz.eq(0).combobox("setValue", value);
    }
} catch (err) { }


//第几胎
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('第几胎'):last");
        if (a.length == 0) a = $(n + ":contains('几胎'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();

    var txt = a.find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");

    if (txt.length > 0) {
        txt.eq(0).val("1");
    } else {
        var tai = a.find(".combobox-f");
        if (tai.length == 0) a = parent.find(".combobox-f");
        if (tai.length == 0) a = parent.nextAll().find(".combobox-f");
        if (tai.length > 0) {
            var value = 0;
            var data = tai.eq(0).combobox("getData");
            var comOption = tai.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("一") != -1 || n[textField].indexOf("1") != -1 || n[textField].indexOf("头") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            tai.eq(0).combobox("setValue", value);
        }
    }
}
catch (e) { }

//独生子女
try {
    var dszn;
    $.each(ele, function (i, n) {
        dszn = $(n + ":contains('独生子女'):last");
        if (dszn.length == 0) dszn = $(n + ":contains('独生'):last");
        if (dszn.length == 0) dszn = $(n + ":contains('子女'):last");
        if (dszn.length > 0) return false;
    });
    var parent = dszn.parent();
    var dsznCombo = dszn.find(".combobox-f");
    if (dsznCombo.length == 0) dsznCombo = parent.find(".combobox-f");
    if (dsznCombo.length == 0) dsznCombo = parent.nextAll().find(".combobox-f");
    if (dsznCombo.length > 0) {
        var value = 0;
        var data = dsznCombo.eq(0).combobox("getData");
        var comOption = dsznCombo.eq(0).combobox("options");

        var valueField = comOption.valueField;
        var textField = comOption.textField;

        $.each(data, function (i, n) {
            if (n[textField].indexOf("是") != -1) 
            {
                value = n[valueField];
                return false;
            }
        });
        dsznCombo.eq(0).combobox("setValue", value);
    }
} catch (err) { }

//接种证号
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('接种证号'):last");
		if (a.length == 0) a = $(n + ":contains('预防接种'):last");
        if (a.length == 0) a = $(n + ":contains('儿童编码'):last");
        //if (a.length == 0) a = $(n + ":contains('免疫编码'):last");
        if (a.length > 0) return false;
    });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("12020901650833");
    }
}
catch (e) { }

//健康状况
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('健康状况'):last");
        if (a.length == 0) a = $(n + ":contains('健康'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).combobox('setValue', '健康或良好');
    } else {
        var jiankang = a.find(".combobox-f");
        if (jiankang.length == 0) a = parent.find(".combobox-f");
        if (jiankang.length == 0) a = parent.nextAll().find(".combobox-f");
        if (jiankang.length > 0) {
            var value = 0;
            var data = jiankang.eq(0).combobox("getData");
            var comOption = jiankang.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("健康") != -1 || n[textField].indexOf("良好") != -1 || n[textField].indexOf("好") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            jiankang.eq(0).combobox("setValue", value);
        }
    }
}
catch (e) { }

//过敏体质
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('过敏体质'):last");
        if (a.length == 0) a = $(n + ":contains('过敏'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).combobox('setValue', '否');
    } else {
        var guomin = a.find(".combobox-f");
        if (guomin.length == 0) a = parent.find(".combobox-f");
        if (guomin.length == 0) a = parent.nextAll().find(".combobox-f");
        if (guomin.length > 0) {
            var value = 0;
            var data = guomin.eq(0).combobox("getData");
            var comOption = guomin.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("无") != -1 || n[textField].indexOf("否") != -1 || n[textField].indexOf("没有") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            guomin.eq(0).combobox("setValue", value);
        }
    }
}
catch (e) { }

//是否残疾
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('是否残疾'):last");
        if (a.length == 0) a = $(n + ":contains('残疾'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).combobox('setValue', '否');
    } else {
        var canji = a.find(".combobox-f");
        if (canji.length == 0) a = parent.find(".combobox-f");
        if (canji.length == 0) a = parent.nextAll().find(".combobox-f");
        if (canji.length > 0) {
            var value = 0;
            var data = canji.eq(0).combobox("getData");
            var comOption = canji.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("无") != -1 || n[textField].indexOf("否") != -1 || n[textField].indexOf("没有") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            canji.eq(0).combobox("setValue", value);
        }
    }
}
catch (e) { }

//残疾类型
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('残疾类型'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).combobox('setValue', '无残疾');
    } else {
        var canjileixing = a.find(".combobox-f");
        if (canjileixing.length == 0) a = parent.find(".combobox-f");
        if (canjileixing.length == 0) a = parent.nextAll().find(".combobox-f");
        if (canjileixing.length > 0) {
            var value = 0;
            var data = canjileixing.eq(0).combobox("getData");
            var comOption = canjileixing.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("无") != -1 || n[textField].indexOf("否") != -1 || n[textField].indexOf("没有") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            canjileixing.eq(0).combobox("setValue", value);
        }
    }
}
catch (e) { }

//籍贯
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('籍贯'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val("天津");
    }
}
catch (e) { }

//国籍
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('国籍'):last");
        if (a.length == 0) a = $(n + ":contains('国籍'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val("中国");
    }
}
catch (e) { }

//出生地
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('出生地'):last");
        if (a.length == 0) a = $(n + ":contains('出生'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val("天津市河东区");
    }
}
catch (e) { }

//预防接种证号
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('预防接种'):last");
        if (a.length == 0) a = $(n + ":contains('儿童编码'):last");
        if (a.length == 0) a = $(n + ":contains('预防'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val("12020901650833");
    }
}
catch (e) { }

//派出所
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('派出所'):last");
        if (a.length == 0) a = $(n + ":contains('派出'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var pcs = a.find("input[type=text]");
    if (pcs.length == 0) pcs = parent.find("input[type=text]");
    if (pcs.length == 0) {
        pcs = parent.nextAll().find("input[type=text]");
    }
    if (pcs.length == 1) pcs.eq(0).val("天津市公安局河东分局春华派出所").blur();
    else {
        if (pcs.length > 0)
            pcs.eq(0).val("天津").blur();
        if (pcs.length > 1)
            pcs.eq(1).val("河东").blur();
        if (pcs.length > 2)
            pcs.eq(2).val("春华派出所").blur();
    }
} catch (err) { }

//现地址
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('居住地'):last");
        if (a.length == 0) a = $(n + ":contains('居住'):last");
        if (a.length == 0) a = $(n + ":contains('现居住住址'):last");
        if (a.length == 0) a = $(n + ":contains('现住址'):last");
        if (a.length == 0) a = $(n + ":contains('现地址'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var jzd = a.find("input[type=text]");
    if (jzd.length == 0) jzd = parent.find("input[type=text]");
    if (jzd.length == 0) {
        jzd = parent.nextAll().find("input[type=text]");
    }

    if (jzd.length == 1) jzd.eq(0).val("天津市河东区新开路万春花园3号楼2门501号").blur();
    else {
        if (jzd.length > 0)
            jzd.eq(0).val("天津").blur();
        if (jzd.length > 1)
            jzd.eq(1).val("河东").blur();
        if (jzd.length > 2)
            jzd.eq(2).val("新开路万春花园3号楼2门501号").blur();
    }
} catch (err) { }

//房屋所有权地址。
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('房屋所有权地址'):last");
        if (a.length == 0) a = $(n + ":contains('房本地址'):last");
        if (a.length == 0) a = $(n + ":contains('房产证地址'):last");
        //if (a.length == 0) a = $(n + ":contains('所有'):last");
        if (a.length > 0) return false;
    });


    var parent = a.parent();
    var jzd = a.find("input[type=text]");
	if (jzd.length == 0) jzd = a.nextAll().find("input[type=text]");
    if (jzd.length == 0) jzd = parent.find("input[type=text]");
    if (jzd.length == 0) {
        jzd = parent.nextAll().find("input[type=text]");
    }
    if (jzd.length == 1) jzd.eq(0).val("天津市河东区新开路万春花园3号楼2门501号").blur();
    else {
        if (jzd.length > 0)
            jzd.eq(0).val("天津").blur();
        if (jzd.length > 1)
            jzd.eq(1).val("河东").blur();
        if (jzd.length > 2)
            jzd.eq(2).val("新开路万春花园3号楼2门501号").blur();
    }
} catch (err) { }

//监护人姓名
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人姓名'):last");
        if (a.length == 0) a = $(n + ":contains('监护人姓名'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (jzd.length == 0) jzd = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val("李健");
    }
}
catch (e) { }

//监护人年龄
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人年龄'):last");
        if (a.length == 0) a = $(n + ":contains('监护人年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=number]");
	if (jzd.length == 0) jzd = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
	if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");

    if (txt.length > 0) {
        txt.eq(0).val("39");
    }
}
catch (e) { }

//监护人学历
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人学历'):last");
        if (a.length == 0) a = $(n + ":contains('学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("本科");
    else {
        var jhrxl = a.find(".combobox-f");
        if (jhrxl.length == 0) a = parent.find(".combobox-f");
        if (jhrxl.length == 0) a = parent.nextAll().find(".combobox-f");
        if (jhrxl.length > 0) {
            var value = 0;
            var data = jhrxl.eq(0).combobox("getData");
            var comOption = jhrxl.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("本科") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            jhrxl.eq(0).combobox("setValue", value);
        }
    }
} catch (e) { }

//监护人身份证
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人身份证'):last");
        if (a.length == 0) a = $(n + ":contains('监护人身份'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("120223198012170170");
} catch (e) { }

//监护人与幼儿关系
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人与幼儿关系'):last");
        if (a.length == 0) a = $(n + ":contains('监护人与幼儿'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿与监护人关系'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿与监护人'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("父亲");
		txt.eq(0).combobox("setValue", "父亲");
    } else {
        var jhrgx = a.find(".combobox-f");
        if (jhrgx.length == 0) a = parent.find(".combobox-f");
        if (jhrgx.length == 0) a = parent.nextAll().find(".combobox-f");
        if (jhrgx.length > 0) {
            var value = 0;
            var data = jhrgx.eq(0).combobox("getData");
            var comOption = jhrgx.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("父") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            jhrgx.eq(0).combobox("setValue", value);
        }
    }
} catch (e) { }

//户主姓名
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('户主姓名'):last");
        if (a.length == 0) a = $(n + ":contains('户主'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("张志军");
    }
} catch (e) { }

//户主与幼儿关系
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('户主与幼儿关系'):last");
        if (a.length == 0) a = $(n + ":contains('户主与幼儿'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿与户主关系'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿与户主'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("母亲");
		txt.eq(0).combobox("setValue", "母亲");
    } else {
        var hzgx = a.find(".combobox-f");
        if (hzgx.length == 0) a = parent.find(".combobox-f");
        if (hzgx.length == 0) a = parent.nextAll().find(".combobox-f");
        if (hzgx.length > 0) {
            var value = 0;
            var data = hzgx.eq(0).combobox("getData");
            var comOption = hzgx.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("母") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            hzgx.eq(0).combobox("setValue", value);
        }
    }
} catch (e) { }

//承租人姓名
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('承租人姓名'):last");
        if (a.length == 0) a = $(n + ":contains('承租人姓'):last");
        if (a.length == 0) a = $(n + ":contains('持证人'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("张志军");
    }
} catch (e) { }

//承租人与幼儿关系
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('承租人与幼儿关系'):last");
        if (a.length == 0) a = $(n + ":contains('承租人与幼儿'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("母亲");
		txt.eq(0).combobox("setValue", "母亲");
    } else {
        var czrgx = a.find(".combobox-f");
        if (czrgx.length == 0) a = parent.find(".combobox-f");
        if (czrgx.length == 0) a = parent.nextAll().find(".combobox-f");
        if (czrgx.length > 0) {
            var value = 0;
            var data = czrgx.eq(0).combobox("getData");
            var comOption = czrgx.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("母") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            czrgx.eq(0).combobox("setValue", value);
        }
    }
} catch (e) { }

//父亲名字
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲姓名'):last");
        if (a.length == 0) a = $(n + ":contains('父亲名字'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸姓名'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸名字'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("李健");
    }
} catch (e) { }

//父亲年龄
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲年龄'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=number]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("39");
    }
} catch (e) { }

//父亲学历
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲学历'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("本科");
    } else {
        var fqxl = a.find(".combobox-f");
        if (fqxl.length == 0) a = parent.find(".combobox-f");
        if (fqxl.length == 0) a = parent.nextAll().find(".combobox-f");
        if (fqxl.length > 0) {
            var value = 0;
            var data = fqxl.eq(0).combobox("getData");
            var comOption = fqxl.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("本科") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            fqxl.eq(0).combobox("setValue", value);
        }
    }
} catch (e) { }

//父亲身份证
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('父亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('父亲身份'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份证'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("120223198012170170");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲单位'):last");
		if (a.length == 0) a = $(n + ":contains('父亲工作单位'):last");
        if (a.length == 0) a = $(n + ":contains('父亲公司'):last");
		if (a.length == 0) a = $(n + ":contains('爸爸工作单位'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸工作单位'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸公司'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("天津市公安局河北分局");
    }
} catch (e) { }

//父亲电话
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲电话'):last");
        if (a.length == 0) a = $(n + ":contains('父亲电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('父亲手机号'):last");
        if (a.length == 0) a = $(n + ":contains('父亲手机'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸电话'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸手机号'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸手机'):last");
        if (a.length > 0) return false;
    });


    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("13212149397");
    }
} catch (e) { }

//母亲姓名
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲姓名'):last");
        if (a.length == 0) a = $(n + ":contains('母亲名字'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈姓名'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈名字'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("张志军");
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲单位'):last");
		if (a.length == 0) a = $(n + ":contains('母亲工作单位'):last");
        if (a.length == 0) a = $(n + ":contains('母亲公司'):last");
		if (a.length == 0) a = $(n + ":contains('妈妈工作单位''):last");
        if (a.length == 0) a = $(n + ":contains('妈妈单位'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈公司'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("城南供电分公司");
    }
} catch (e) { }

//母亲年龄
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲年龄'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
	var a=txt = a.find("input[type=number]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
	if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("37");
    }
} catch (e) { }

//母亲学历
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲学历'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("硕士");
    } else {
        var mqxl = a.find(".combobox-f");
        if (mqxl.length == 0) a = parent.find(".combobox-f");
        if (mqxl.length == 0) a = parent.nextAll().find(".combobox-f");
        if (mqxl.length > 0) {
            var value = 0;
            var data = mqxl.eq(0).combobox("getData");
            var comOption = mqxl.eq(0).combobox("options");

            var valueField = comOption.valueField;
            var textField = comOption.textField;
            $.each(data, function (i, n) {
                if (n[textField].indexOf("硕士") != -1) {
                    value = n[valueField];
                    return false;
                }
            });
            mqxl.eq(0).combobox("setValue", value);
        }
    }
} catch (e) { }

//母亲身份证
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('母亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('母亲身份'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈身份证'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈身份'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("120223198202240424");
    }
} catch (e) { }

//母亲电话
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲电话'):last");
        if (a.length == 0) a = $(n + ":contains('母亲电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('母亲手机号'):last");
        if (a.length == 0) a = $(n + ":contains('母亲手机'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈电话'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈手机号'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈手机'):last");

        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("13820460809");
    }
} catch (e) { }

//主要看护人
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('主要看护人'):last");
        if (a.length == 0) a = $(n + ":contains('看护人'):last");
        if (a.length == 0) a = $(n + ":contains('照看人'):last");
        if (a.length == 0) a = $(n + ":contains('照顾人'):last");

        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("妈妈");
    }
} catch (e) { }

//其它联系电话
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('其它联系电话'):last");
        if (a.length == 0) a = $(n + ":contains('联系电话'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("13820460809");
    }
} catch (e) { }


try {
    var yz1 = $(":contains('验证文字'):last");
    var yzwz = yz1.parent("div").prev("div").find("span").text();
    yz1.next("div").find("input[type=text]").eq(0).val(yzwz).blur();
} catch (err) { }

try {
    var yz1 = $(":contains('验证文字')").eq(1);
    var yzwz = yz1.parent("div").prev("div").find("span").text();
    yz1.next("div").find("input[type=text]").eq(0).val(yzwz).blur();
} catch (err) { }

try {
    var yz1 = $(":contains('验证文字')").eq(2);
    var yzwz = yz1.parent("div").prev("div").find("span").text();
    yz1.next("div").find("input[type=text]").eq(0).val(yzwz).blur();
} catch (err) { }






