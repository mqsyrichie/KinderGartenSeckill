javascript: (function (c) { function e(a) { var b = a.document, f = a.jQuery, g = function (a) { h.forEach(function (b) { a["on" + b] = null; f && f(a).unbind(b); try { /frame/i.test(a.tagName) && e(a.contentWindow) } catch (c) { } }) };[a, b].forEach(g); a = 0; for (var b = b.all, c = b.length; a < c; a++) { var d = b[a]; d && 1 === d.nodeType && g(d) } } var h = "contextmenu dragstart mouseup copy beforecopy selectstart select keydown keyup keypress".split(" "); e(c); $("[onpaste='return false']").removeAttr("onpaste"); $("body").unbind("paste"); })(window);
$.fn.hasAttr = function (e) { return $(this).attr(e) != undefined; };
var ele = ["label", "th", "td", "a", "li", "ul"];


try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('幼儿园选择'):last");
            if (a.length == 0) a = $(n + ":contains('幼儿园'):last");
            if (a.length == 0) a = $(n + ":contains('入园意向'):last");
            if (a.length == 0) a = $(n + ":contains('入园'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = a.find("select");
    if (txt.length == 0) txt = a.nextAll().find("select");
    if (txt.length == 0) txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function(i, n) {
            var $t = $(this);
            if (($t.text().indexOf("一幼") > -1 || $t.text().indexOf("16") > -1)&& $t.text().indexOf("总") > -1) {
                $t.attr("selected", "selected");
                val = $t.val();
                return false;
            }
        });
        txt.val(val);
    }
}
catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('孩子姓名'):last");
        if (a.length == 0) a = $(n + ":contains('孩子名字'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿姓名'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿名字'):last");
        if (a.length == 0) a = $(n + ":contains('儿童名字'):last");
        if (a.length == 0) a = $(n + ":contains('儿童姓名'):last");
        if (a.length > 0) return false;
    });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) {
        txt = parent.nextAll().find("input[type=text]");
    }
    if (txt.length > 0) {
        txt.eq(0).val("王禹涞");
    }
}
catch (e) { }

try {
    var sfzhm = "120103201510290099";
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('儿童身份证号'):last");
            if (a.length == 0) a = $(n + ":contains('儿童身份'):last");
            if (a.length == 0) a = $(n + ":contains('幼儿身份证号'):last");
            if (a.length == 0) a = $(n + ":contains('幼儿身份'):last");
            if (a.length == 0) a = $(n + ":contains('身份证号'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('户口所在地'):last");
            if (a.length == 0) a = $(n + ":contains('户口地址'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.each(function(i, n) {
            if (!$(this).hasAttr("readonly")) {
                $(this).val("平江道文玥北里6门508号").blur();
            }
        });
    }
}
catch (e) { }

try {
    var sfzhm = "12020901610284";
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('接种证号'):last");
            if (a.length == 0) a = $(n + ":contains('接种证'):last");
            if (a.length == 0) a = $(n + ":contains('儿童编码'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
}
catch (e) { }

try {
    var mz = "汉";
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('民族'):last");
            if (a.length == 0) a = $(n + ":contains('民族'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");

    if (txt.length > 0) {
        txt.eq(0).val(mz).blur();
    }
    else {
        //select
        var sel = a.find("select");
        if (sel.length == 0) sel = a.nextAll().find("select");
        if (sel.length == 0) sel = parent.find("select");
        if (sel.length == 0) sel = parent.nextAll().find("select");
        if (sel.length >= 0) {
            sel.find("option").each(function (i, n) {
                var $t = $(this);
                if ($t.text().indexOf(mz) > -1) {
                    $t.attr("selected", "selected");
                    return false;
                }
            })
        }
    }
}
catch (e) { }


try {
    var sfzhm = "20151029";
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('出生日期'):last");
            if (a.length == 0) a = $(n + ":contains('出生'):last");
            if (a.length == 0) a = $(n + ":contains('生日'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val("2015-10-29").blur();
        }
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('儿童性别'):last");
            if (a.length == 0) a = $(n + ":contains('幼儿性别'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("男").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('户主姓名'):last");
            if (a.length == 0) a = $(n + ":contains('户主'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("马平").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('与户主关系'):last");
            if (a.length == 0) a = $(n + ":contains('户主关系'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("外").blur();
    else {
        txt = parent.find("select");
        if (txt.length == 0) txt = parent.nextAll().find("select");
        if (txt.length > 0) {
            txt.find("option").each(function (i, e) {
                var text = $(this).text();
                if (text.indexOf("外孙") >= 0 || text.indexOf("外") >= 0) {
                    $(this).attr("selected", "selected");
                    return false;
                }
            });
        }
    }
} catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('出生地址'):last");
            if (a.length == 0) a = $(n + ":contains('出生地'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("天津").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('国籍'):last");
            if (a.length == 0) a = $(n + ":contains('国家'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("中国").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('乳名'):last");
            if (a.length == 0) a = $(n + ":contains('昵称'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("小鱼儿").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('体重'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = a.find("input[type=number]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("16").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('身高'):last");
            if (a.length == 0) a = $(n + ":contains('身高'):last");
            if (a.length > 0) return false;
        });
    var parent = a.parent();
    var txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) txt.eq(0).val("100").blur();
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('第几胎'):last");
            if (a.length == 0) a = $(n + ":contains('胎'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = a.find("select");
    if (txt.length == 0) txt = a.nextAll().find("select");
    if (txt.length == 0) txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("头") >= 0 || text.indexOf("一") >= 0 || text.indexOf("1") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    } else {
        var txt = a.find("input[type=text]");
        if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
        if (txt.length == 0) txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("头胎").blur();
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('低保儿童'):last");
            if (a.length == 0) a = $(n + ":contains('低保'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    }
    else {
        var txt = a.find("input[type=text]");
        if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
        if (txt.length == 0) txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("否").blur();
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('残疾儿童'):last");
            if (a.length == 0) a = $(n + ":contains('残疾'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    }
    else {
        var txt = a.find("input[type=text]");
        if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
        if (txt.length == 0) txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("否").blur();
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('家族病史'):last");
            if (a.length == 0) a = $(n + ":contains('病史'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    }
    else {
        if (txt.length == 0) txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("无").blur();
    }
}
catch (e) { }


try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('残疾类别'):last");
            if (a.length == 0) a = $(n + ":contains('残疾种类'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    }
    else {
        if (txt.length == 0) txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("无").blur();
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('务工'):last");
            if (a.length == 0) a = $(n + ":contains('随迁'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    }
    else {
        if (txt.length == 0) txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("无").blur();
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('过敏'):last");
            if (a.length == 0) a = $(n + ":contains('体质'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    } else {
        if (txt.length == 0) txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("否").blur();
    }
}
catch (e) { }

try {
    var a;
    $.each(ele,
        function(i, n) {
            a = $(n + ":contains('依恋物'):last");
            if (a.length == 0) a = $(n + ":contains('依恋'):last");
            if (a.length > 0) return false;
        });

    var parent = a.parent();
    var txt = parent.find("select");
    if (txt.length == 0) txt = parent.nextAll().find("select");
    if (txt.length > 0) {
        var val;
        txt = txt.eq(0);
        txt.find("option").each(function (i, e) {
            var text = $(this).text();
            if (text.indexOf("无") >= 0 || text.indexOf("否") >= 0 || text.indexOf("不是") >= 0) {
                val = $(this).val();
                return false;
            }
        });
        txt.val(val);
    }
    else {
        txt = parent.find("input[type=text]");
        if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
        if (txt.length > 0) txt.eq(0).val("否").blur();
    }
}
catch (e) { }

//父亲名字
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲姓名'):last");
        if (a.length == 0) a = $(n + ":contains('父亲名字'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸姓名'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸名字'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("王昊").blur();
    }
} catch (e) { }

//父亲年龄
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲年龄'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=number]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("36").blur();
    }
} catch (e) { }

//父亲学历
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲学历'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("本科");
    } else {
        txt = a.find("select");
		if (txt.length == 0) txt = a.nextAll().find("select");
		if (txt.length == 0) txt = parent.find("select");
		if (txt.length == 0) txt = parent.nextAll().find("select");
		if(txt.length>0){
			var val;
			txt.find("option").each(function(i, n) {
				var $t = $(this);
				if ($t.text().indexOf("本科") > -1 || $t.text().indexOf("本") > -1) {
					$t.attr("selected", "selected");
					val = $t.val();
					return false;
				}
			});
			txt.val(val);
		}
    }
} catch (e) { }

//父亲身份证
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('父亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('父亲身份'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份证'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸身份'):last");
        if (a.length > 0) return false;
    });
    
	var sfzhm="120102198301152916";
	var parent = a.parent();
	var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
} catch (e) { }

//父亲电话
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('父亲电话'):last");
        if (a.length == 0) a = $(n + ":contains('父亲电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('父亲手机号'):last");
        if (a.length == 0) a = $(n + ":contains('父亲手机'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸电话'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸手机号'):last");
        if (a.length == 0) a = $(n + ":contains('爸爸手机'):last");
        if (a.length > 0) return false;
    });


    var sfzhm="18902036655";
	var parent = a.parent();
	var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
} catch (e) { }

//母亲姓名
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲姓名'):last");
        if (a.length == 0) a = $(n + ":contains('母亲名字'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈姓名'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈名字'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("刘洋").blur();
    }
} catch (e) { }

//母亲年龄
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲年龄'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=number]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("33").blur();
    }
} catch (e) { }

//母亲学历
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲学历'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("专科");
    } else {
        txt = a.find("select");
		if (txt.length == 0) txt = a.nextAll().find("select");
		if (txt.length == 0) txt = parent.find("select");
		if (txt.length == 0) txt = parent.nextAll().find("select");
		if(txt.length>0){
			var val;
			txt.find("option").each(function(i, n) {
				var $t = $(this);
				if ($t.text().indexOf("专科") > -1 || $t.text().indexOf("专") > -1) {
					$t.attr("selected", "selected");
					val = $t.val();
					return false;
				}
			});
			txt.val(val);
		}
    }
} catch (e) { }

//母亲身份证
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('母亲身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('母亲身份'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈身份证'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈身份'):last");
        if (a.length > 0) return false;
    });

    var sfzhm="120103198606092922";
	var parent = a.parent();
	var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
} catch (e) { }

//母亲电话
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('母亲电话'):last");
        if (a.length == 0) a = $(n + ":contains('母亲电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('母亲手机号'):last");
        if (a.length == 0) a = $(n + ":contains('母亲手机'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈电话'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈电话号码'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈手机号'):last");
        if (a.length == 0) a = $(n + ":contains('妈妈手机'):last");

        if (a.length > 0) return false;
    });

    var sfzhm="13821525355";
	var parent = a.parent();
	var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
} catch (e) { }

//主要看护人
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('主要看护人'):last");
        if (a.length == 0) a = $(n + ":contains('看护人'):last");
        if (a.length == 0) a = $(n + ":contains('照看人'):last");
        if (a.length == 0) a = $(n + ":contains('照顾人'):last");

        if (a.length > 0) return false;
    });

    var parent = a.parent();
	var txt=a.find("input[type=checkbox]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=checkbox]");
	if (txt.length == 0) txt = parent.find("input[type=checkbox]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=checkbox]");
	if(txt.length>0) txt.eq(0).attr("checked",true);
	else {	
	    txt=a.find("input[type=radio]");
		if (txt.length == 0) txt = a.nextAll().find("input[type=radio]");
		if (txt.length == 0) txt = parent.find("input[type=radio]");
		if (txt.length == 0) txt = parent.nextAll().find("input[type=radio]");
	    if(txt.length>0) txt.eq(0).attr("checked",true);
		else {
			txt = a.find("input[type=text]");
			if (txt.length == 0) txt = parent.find("input[type=text]");
			if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
			if (txt.length > 0) 
				txt.eq(0).val("妈妈").blur();
        }
	}
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人身份证'):last");
        if (a.length == 0) a = $(n + ":contains('看护人身份证号'):last");
        if (a.length == 0) a = $(n + ":contains('照看人身份'):last");
        if (a.length > 0) return false;
    });

    var sfzhm="120103198606092922";
	var parent = a.parent();
	var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人电话'):last");
        if (a.length == 0) a = $(n + ":contains('看护人电话'):last");
        if (a.length == 0) a = $(n + ":contains('照看人电话'):last");
		if (a.length == 0) a=$(n + ":contains('监护人手机号'):last");
        if (a.length == 0) a = $(n + ":contains('看护人手机号'):last");
        if (a.length == 0) a = $(n + ":contains('照看人手机号'):last");
        if (a.length > 0) return false;
    });

    var sfzhm="13821525355";
	var parent = a.parent();
	var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
} catch (e) { }

//其它联系电话
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('其它联系电话'):last");
        if (a.length == 0) a = $(n + ":contains('联系电话'):last");
        if (a.length > 0) return false;
    });

    var sfzhm="13821525355";
	var parent = a.parent();
	var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        var sfz = txt.eq(0);
        var allSfz = sfz.parent().find("input[type=text]");
        if (allSfz.length > 1) {
            var sfzArr = sfzhm.split("");
            $.each(allSfz, function (i, n) {
                $(this).val(sfzArr[i]);
            });
        }
        else {
            sfz.val(sfzhm).blur();
        }
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人年龄'):last");
        if (a.length == 0) a = $(n + ":contains('看护人年龄'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=number]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = parent.find("input[type=number]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=number]");
    if (txt.length == 0) txt = a.find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("33").blur();
    }
} catch (e) { }

//学历
try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人学历'):last");
        if (a.length == 0) a = $(n + ":contains('看护人学历'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("专科").blur();
    } else {
        txt = a.find("select");
		if (txt.length == 0) txt = a.nextAll().find("select");
		if (txt.length == 0) txt = parent.find("select");
		if (txt.length == 0) txt = parent.nextAll().find("select");
		if(txt.length>0){
			var val;
			txt.find("option").each(function(i, n) {
				var $t = $(this);
				if ($t.text().indexOf("专科") > -1 || $t.text().indexOf("专") > -1) {
					$t.attr("selected", "selected");
					val = $t.val();
					return false;
				}
			});
			txt.val(val);
		}
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人姓名'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("刘洋").blur();
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人性别'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("女").blur();
    } else {
        txt = a.find("select");
		if (txt.length == 0) txt = a.nextAll().find("select");
		if (txt.length == 0) txt = parent.find("select");
		if (txt.length == 0) txt = parent.nextAll().find("select");
		if(txt.length>0){
			var val;
			txt.find("option").each(function(i, n) {
				var $t = $(this);
				if ($t.text().indexOf("女") > -1 || $t.text().indexOf("女") > -1) {
					$t.attr("selected", "selected");
					val = $t.val();
					return false;
				}
			});
			txt.val(val);
		}
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人工作单位'):last");
        if (a.length == 0) a = $(n + ":contains('监护人工作'):last");
        if (a.length == 0) a = $(n + ":contains('看护人工作'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("新合作一商邻里中心商业管理（天津）有限公司").blur();
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('监护人职务'):last");
        if (a.length == 0) a = $(n + ":contains('看护人职务'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("职员").blur();
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('政治面貌'):last");
        if (a.length == 0) a = $(n + ":contains('政治'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("群众").blur();
    }
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('与幼儿关系'):last");
        if (a.length == 0) a = $(n + ":contains('幼儿关系'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0) {
        txt.eq(0).val("母亲").blur();
    }
} catch (e) { }

/**************下拉框选择*************/
var selectValue;
var selects = $('select');

try {
    $.each(selects, function (i, n) {
        var sel = $(n);
        selectValue = "";
        $(n).find("option").each(function (i, n) {
            var $t = $(this);
            //幼儿园
            /****
             * ****
             * 河西区第二幼儿园总园。要分成两个条件。1.第二幼儿园 2.总园
             * 河西区第二十四幼儿园分园(怡林园)。1.第二十四幼儿园 2.怡林
             */
            if ($t.text().indexOf("第一") > -1 && $t.text().indexOf("总") > -1) {
                $t.attr("selected", "selected");
                selectValue = $t.val();
                return false;
            }
            //性别
            else if ($t.text().indexOf("男") > -1) {
                $t.attr("selected", "selected");
                selectValue = $t.val();
                return false;
            }
            //民族选择
            else if ($t.text().indexOf("汉族") > -1) {
                $t.attr("selected", "selected");
                selectValue = $t.val();
                return false;
            }
            //国籍
            else if ($t.text().indexOf("中国") > -1) {
                $t.attr("selected", "selected");
                selectValue = $t.val();
                return false;
            }
			//政治面貌
            else if ($t.text().indexOf("群众") > -1) {
                $t.attr("selected", "selected");
                selectValue = $t.val();
                return false;
            }
            //第几胎
            else if ($t.text().indexOf("头胎") > -1 || $t.text().indexOf("第一胎") > -1) {
                $t.attr("selected", "selected");
                selectValue = $t.val();
                return false;
            }
            //（是/否）、（有/无）下拉框，默认为否/无。脚本执行后请认真核对信息，避免信息有误。
            if ($t.text().indexOf("否") > -1 || $t.text().indexOf("无") > -1 || $t.text().indexOf("没有") > -1) {
                $t.attr("selected", "selected");
                selectValue = $t.val();
                return false;
            }
        })
        if (selectValue.length > 0) sel.val(selectValue);
    })
} catch (e) { }


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('水果'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("苹果").blur();
    
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('爱好'):last");
		if (a.length == 0) a = $(n + ":contains('兴趣'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("音乐舞蹈").blur();
    
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('游戏'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("足球").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('选择我园'):last");
		if (a.length == 0) a = $(n + ":contains('的原因'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("离家近").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('陪伴孩子'):last");
		if (a.length == 0) a = $(n + ":contains('多长时间'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("三小时").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('沟通'):last");
		if (a.length == 0) a = $(n + ":contains('什么方式'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("面对面").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('颜色'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("蓝色").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('性格'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("外向").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('玩具'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("托马斯火车").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('习惯'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("没有").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('关注'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("没有").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('参加'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("没有").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('沟通'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("微信、电话、面对面").blur();
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('早教'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=text]");
	if (txt.length == 0) txt = a.nextAll().find("input[type=text]");
    if (txt.length == 0) txt = parent.find("input[type=text]");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=text]");
    if (txt.length > 0)  txt.eq(0).val("学习能力").blur();
} catch (e) { }

$("input[type=radio][value='否']").click();
$("td").each(function(i,n){ $(n).find("input[type=checkbox]").each(function(j,m){  if(j<3){ $(m).attr("checked",true) }else{  return false }  }) })

$("td").each(function(i,n){ $(n).find("input[type=radio]").eq(1).attr("checked",true);})


try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('预防接种'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=radio][value='是']");
	if (txt.length == 0) txt = a.nextAll().find("input[type=radio][value='是']");
    if (txt.length == 0) txt = parent.find("input[type=radio][value='是']");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=radio][value='是']");
    if (txt.length > 0)   txt.attr("checked",true);
} catch (e) { }

try {
    var a;
    $.each(ele, function (i, n) {
        a = $(n + ":contains('心脏健康'):last");
        if (a.length > 0) return false;
    });

    var parent = a.parent();
    var txt = a.find("input[type=radio][value='是']");
	if (txt.length == 0) txt = a.nextAll().find("input[type=radio][value='是']");
    if (txt.length == 0) txt = parent.find("input[type=radio][value='是']");
    if (txt.length == 0) txt = parent.nextAll().find("input[type=radio][value='是']");
    if (txt.length > 0)   txt.attr("checked",true);
} catch (e) { }


