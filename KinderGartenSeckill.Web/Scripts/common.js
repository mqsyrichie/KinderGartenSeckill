﻿//function initDatePicker()
function MyAjax(url, d, s, b, type, c, e) {
    try {
        $.ajax({
            type: type || "POST",
            cache: false,
            contentType: "application/json;utf-8",
            url: url,
            data: d,
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (result) {
                s(result);
            },
            error: function (result, textStatus) {
                if (e) e();
            },
            complete: function () {
                if (c) c();
            }
        });
    }
    catch (e) {
        if (window.console)
            window.console.error('Error in _global.ajax :' + e.message);
    }
}


$.fn.extend({
    bindDatePicker: function () {
        $(this).datepicker({
            weekStart: 1,
            autoclose: true,
            format: 'yyyy-mm-dd',
            language: 'zh-CN',
            clearBtn: true,
            todayHighlight: true
        });
    }
});

$.fn.extend({
    bindDateTimePicker: function () {
        $(this).datetimepicker({
            weekStart: 1,
            autoclose: true,
            format: "yyyy-mm-dd hh:ii:ss",
            minView: 0,
            minuteStep: 1,
            language: 'zh-CN',
            clearBtn: true,
            todayHighlight: true
        });
    }
});

function onSuccess(data, status) {
    if ("success" == status && data.result.id > 0) {
        abp.message.success("保存成功！");
    }
    else {
        abp.message.error("保存失败！");
    }
}

function onBegin(selector) {
    abp.ui.block(selector); //或者直接使用选择器参数
}

function onComplete(selector) {
    abp.ui.unblock(selector); //指定元素解除覆盖
}

function onFailure(xhr, status, error) {
    if (error.length > 0) {
        abp.message.error('提交失败(F)，请联系管理员');
    }
}