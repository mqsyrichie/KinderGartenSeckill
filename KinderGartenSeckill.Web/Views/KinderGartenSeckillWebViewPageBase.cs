﻿using Abp.Web.Mvc.Views;

namespace KinderGartenSeckill.Web.Views
{
    public abstract class KinderGartenSeckillWebViewPageBase : KinderGartenSeckillWebViewPageBase<dynamic>
    {

    }

    public abstract class KinderGartenSeckillWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected KinderGartenSeckillWebViewPageBase()
        {
            LocalizationSourceName = KinderGartenSeckillConsts.LocalizationSourceName;
        }
    }
}