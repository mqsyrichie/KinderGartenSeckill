﻿using KinderGartenSeckill.EntityFramework.Repositories;
using KinderGartenSeckill.Model;
using KinderGartenSeckill.Service;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace KinderGartenSeckill.Web.Controllers
{
    public class HomeController : KinderGartenSeckillControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;

        public HomeController(IOrderService orderService, IUserService userService) : base(userService)
        {
            _orderService = orderService;
            _userService = userService;
        }

        public ActionResult Index()
        {
#if DEBUG
            //SetAuthCookie(2, 1);
#endif
            //SetAuthCookie(1, 0);
            //var userType = CurrUserType;
            //var order = new bm_order
            //{
            //    TaobaoWangWangNo = "rui842825466",
            //    TaobaoOrderNo = "3177896895865962",
            //    TaobaoOrderTime = DateTime.Parse("2017-02-24 14:18:04")
            //};
            //_orderService.Insert(order);
            return View();
        }

        public ActionResult SignTime()
        {
            return View();
        }

        public ActionResult Result()
        {
            return View();
        }
    }
}