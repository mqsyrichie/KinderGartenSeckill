﻿using KinderGartenSeckill.Service;
using System.Web.Mvc;

namespace KinderGartenSeckill.Web.Controllers
{
    public class AboutController : KinderGartenSeckillControllerBase
    {
        private readonly IUserService _userService;
        public AboutController(IUserService userService) : base(userService)
        {
            _userService = userService;
        }
        public ActionResult Index()
        {
            return View();
        }
	}
}