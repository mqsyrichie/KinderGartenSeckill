﻿using Abp.Extensions;
using Abp.Web.Mvc.Controllers;
using KinderGartenSeckill.Model;
using KinderGartenSeckill.Service;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace KinderGartenSeckill.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class KinderGartenSeckillControllerBase : AbpController
    {
        private readonly IUserService _userService;
        protected KinderGartenSeckillControllerBase(IUserService userService)
        {
            LocalizationSourceName = KinderGartenSeckillConsts.LocalizationSourceName;

            _userService = userService;
        }

        /// <summary>
        /// 当前登录人的经销商编号
        /// </summary>
        public string CurrUserId
        {
            get
            {
#if DEBUG
                //return "2";
#endif
                if (!IsLogin)
                    throw new Exception("未登录");
                else return HttpContext.User.Identity.Name;
            }
        }

        /// <summary>
        /// 是否已经登录
        /// </summary>
        public bool IsLogin
        {
            get
            {
                return HttpContext.User.Identity.IsAuthenticated;
            }
        }

        /// <summary>
        /// 当前网站的Url（后面带斜杠）
        /// </summary>
        protected string BaseUrl
        {
            get
            {
                var u = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) +
                        System.Web.HttpContext.Current.Request.ApplicationPath;
                if (!u.EndsWith("/"))
                    u += "/";
                return u;
            }
        }

        /// <summary>
        /// 拒绝访问页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Access_Denied(int id)
        {
            return RedirectToAction("AccessDenied", "Error", new { id = id });
        }

        /// <summary>
        /// 获取当前登录人员的用户类型
        /// 0:管理员 1:注册用户
        /// </summary>
        public UserType CurrUserType
        {
            get
            {
                try
                {
                    var user = _userService.GetUser(CurrUserId.To<int>());
                    return user != null ? (UserType)user.UserType : UserType.Customer;
                }
                catch (Exception ex)
                {
                    return UserType.Customer;
                }
            }
        }

        protected void SetAuthCookie(int userId, int uType)
        {
            var userType = "Customer";
            switch (uType)
            {
                case 2:
                    userType = UserType.Actor.ToString(); //报名者
                    break;
                case 1:
                    userType = UserType.Customer.ToString(); //注册用户
                    break;
                case 0:
                    userType = UserType.Admin.ToString(); //管理员
                    break;
            }
            var dTimeOut = FormsAuthentication.Timeout.TotalMinutes;
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, userId.ToString(), DateTime.Now, DateTime.Now.AddMinutes(dTimeOut), true, userType, FormsAuthentication.FormsCookiePath);
            var identity = new FormsIdentity(ticket);

            var gp = new GenericPrincipal(identity, new string[] { userType });
            HttpContext.User = gp;

            var cookieValue = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieValue)
            {
                Secure = FormsAuthentication.RequireSSL,
                Domain = FormsAuthentication.CookieDomain,
                Path = FormsAuthentication.FormsCookiePath
            };
            HttpContext.Response.Cookies.Remove(cookie.Name);
            HttpContext.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// 反射获取对象属性的值和DisplayName(key=value)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string GetClassObj(Object obj)
        {
            StringBuilder str = new StringBuilder();
            Type childType = obj.GetType();
            PropertyInfo[] pro = childType.GetProperties();
            foreach (PropertyInfo p in pro)
            {                
                // 获取指定属性的值
                var value = p.GetValue(obj, null);                
                // 获取属性的DispalyName
                var proName = childType.GetProperty(p.Name);
                var attr = proName.GetCustomAttribute<DisplayNameAttribute>();
                if (attr != null)
                {
                    str.Append($"{attr.DisplayName}={value}");
                    str.Append("\r\n");
                }                
            }
            return str.ToString();
        }

        public ArrayList GetClassObj1(Object obj)
        {
            var array = new ArrayList();
            StringBuilder str = new StringBuilder();
            Type childType = obj.GetType();
            PropertyInfo[] pro = childType.GetProperties();
            foreach (PropertyInfo p in pro)
            {
                // 获取指定属性的值
                var value = p.GetValue(obj, null);
                // 获取属性的DispalyName
                var proName = childType.GetProperty(p.Name);
                var attr = proName.GetCustomAttribute<DisplayNameAttribute>();
                if (attr != null)
                {
                    array.Add($"{attr.DisplayName}={value}");
                    //str.Append($"{attr.DisplayName}={value}");
                    //str.Append("\r\n");
                }
            }
            return array;
        }
    }
}