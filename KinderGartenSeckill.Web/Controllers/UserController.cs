﻿using Abp.Extensions;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using KinderGartenSeckill.Service;
using KinderGartenSeckill.Validators;
using KinderGartenSeckill.Web.Filter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ThoughtWorks.QRCode.Codec;

namespace KinderGartenSeckill.Web.Controllers
{

    public class UserController : KinderGartenSeckillControllerBase
    {
        private readonly IUserService _userService;
        private readonly ICityAndDistrictService _cityAndDistrictService;
        private readonly IKindergartenService _kindergartenService;

        public UserController(IUserService userService, ICityAndDistrictService cityAndDistrictService, IKindergartenService kindergartenService) : base(userService)
        {
            _userService = userService;
            _cityAndDistrictService = cityAndDistrictService;
            _kindergartenService = kindergartenService;
        }

        // GET: User
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(UserLoginDto user)
        {
            var data = new CommonOutputDto();
            var comm = await _userService.Login(user);
            data.IsSuccess = comm.IsSuccess;
            data.Message = comm.Message;
            if (comm.IsSuccess)
            {
                SetAuthCookie(comm.Data.Id, comm.Data.UserType);
                if(comm.Data.UserType == (int)UserType.Customer)
                    data.Url = Url.Action("Info", "Customer");
                data.Url = Url.Action("Index", "Home");
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthFilter(AllowUserType = UserType.Admin)]
        public ActionResult Regist()
        {
            return View();
        }

        [AuthFilter(AllowUserType = UserType.Admin)]
        [HttpPost]
        public async Task<ActionResult> Regist(UserInputDto user)
        {
            var data = new CommonOutputDto();
            var validator = new UserRegisterValidator();
            var result = await validator.ValidateAsync(user);
            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(error =>
                {
                    data.Message += error.ErrorMessage + "\r\n";
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var userId = await _userService.Insert(user);
                //SetAuthCookie(userId, 1);
                data.IsSuccess = true;
                data.Url = Url.Action("Index", "Home");
                return Json(data, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult RegistComplete()
        {
            var user = _userService.GetUser(CurrUserId.To<int>());
            if (user == null)
                return Redirect("~/Shared/Error.cshtml");
            if (user.Status == (int) UserStatus.Approve)
                return RedirectToAction("Info", "Customer");
            var districts = _cityAndDistrictService.GetCityAndDistricts("天津市");

            var model = new UserCompleteDto
            {
                Mobile = user.Mobile,
                Password = user.Password,
                UserName = user.UserName,
                AlipayNo = user.AlipayNo,
                TaobaoWangWangNo = user.TaobaoWangWangNo,
                TaobaoOrderNo = user.TaobaoOrderNo,
                TaobaoOrderTime = user.TaobaoOrderTime,
                Email = user.Email,
                FirstKindergarten = user.FirstKindergarten,
                FirstRegistrationTime = user.FirstRegistrationTime.HasValue ? user.FirstRegistrationTime.Value.ToString("yyyy-MM-dd") : string.Empty,                
                SecondKindergarten = user.SecondKindergarten,
                SecondRegistrationTime = user.SecondRegistrationTime.HasValue ? user.SecondRegistrationTime.Value.ToString("yyyy-MM-dd") : string.Empty,
                VerificationCode = user.VerificationCode,
                RegistWebAccount = user.RegistWebAccount,
                RegistWebPassword = user.RegistWebPassword
            };

            model.AvailableDistricts.Insert(0, new SelectListItem { Text = "--请选择--", Value = "0", Selected = true });
            districts.ForEach(p => model.AvailableDistricts.Add(new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString()
            }));
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> RegistComplete(UserCompleteDto model)
        {
            var data = new CommonOutputDto();
            var validator = new UserCompleteValidator();
            var result = await validator.ValidateAsync(model);
            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(error =>
                {
                    data.Message += error.ErrorMessage + "\r\n";
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var user = await _userService.GetUserByMobile(model.Mobile);
                if (user != null)
                {
                    try
                    {
                        user.UserName = model.UserName;
                        user.AlipayNo = model.AlipayNo;
                        user.TaobaoWangWangNo = model.TaobaoWangWangNo;
                        user.TaobaoOrderNo = model.TaobaoOrderNo;
                        user.Email = model.Email;
                        user.FirstKindergarten = model.FirstKindergarten;
                        user.FirstRegistrationTime = string.IsNullOrWhiteSpace(model.FirstRegistrationTime) ? new DateTime?() : DateTime.Parse(model.FirstRegistrationTime);
                        user.SecondKindergarten = model.SecondKindergarten;
                        user.SecondRegistrationTime = string.IsNullOrWhiteSpace(model.SecondRegistrationTime) ? new DateTime?() : DateTime.Parse(model.SecondRegistrationTime);
                        user.VerificationCode = model.VerificationCode;
                        user.RegistWebAccount = model.RegistWebAccount;
                        user.RegistWebPassword = model.RegistWebPassword;
                        user.Status = (int)UserStatus.Approve;

                        var userId = _userService.Update(user);
                        data.IsSuccess = true;
                        data.Url = Url.Action("Info", "Customer");
                    }
                    catch (Exception ex)
                    {
                        data.Message = "信息提交失败，请联系管理员！";
                    }
                }
                else
                    data.Message = "该用户不存在";
            }            

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Info()
        {
            var user = _userService.GetUser(CurrUserId.To<int>());
            return View(user);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult UserQRCode()
        {
            //var customer = _workContext.CurrentCustomer;
            var url = Url.Action("Login", "User");

            //初始化二维码生成工具
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
            qrCodeEncoder.QRCodeVersion = 0;
            qrCodeEncoder.QRCodeScale = 8;
            //将字符串生成二维码图片
            Bitmap image = qrCodeEncoder.Encode(url, Encoding.Default);
            //保存为PNG到内存流  
            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            string strUrl = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());
            image.Dispose();
            ms.Dispose();
            ViewBag.NativeUrl = strUrl;

            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult GetKinderGardensByDistrictId(int districtId)
        {
            var list = _kindergartenService.GetKindergartens(districtId);
            var kinderGardens = new List<SelectListItem>();
            list.ForEach(p => kinderGardens.Add(new SelectListItem
            {
                Text = p.KindergartenName,
                Value = p.KindergartenName
            }));
            return Json(kinderGardens, JsonRequestBehavior.AllowGet);
        }
    }
}