﻿using Abp.Extensions;
using KinderGartenSeckill.Dto;
using KinderGartenSeckill.Model;
using KinderGartenSeckill.Service;
using KinderGartenSeckill.Validators;
using KinderGartenSeckill.Web.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace KinderGartenSeckill.Web.Controllers
{
    [AuthFilter(AllowUserType = UserType.Customer)]
    public class CustomerController : KinderGartenSeckillControllerBase
    {
        private readonly IUserService _userService;
        private readonly IChildService _childService;
        public CustomerController(IUserService userService, IChildService childService) : base(userService)
        {
            _userService = userService;
            _childService = childService;
        }

        // GET: Customer
        public ActionResult Child()
        {
            var user = _childService.GetUser(CurrUserId.To<int>());
            var child = _childService.GetChild(CurrUserId.To<int>());
            var parents = _childService.GetParents(CurrUserId.To<int>());
            ViewBag.User = user;
            ViewBag.Child = child;
            ViewBag.Parents = parents;
            return View();
        }



        //[ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Child(bm_child child)
        {
            var model = await _childService.InsertOrUpdateChild(child);
            //return PartialView("_Child", model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> ChildForMobile(bm_child child)
        {
            var comm = await _childService.InsertOrUpdateChildForMobile(child);
            //return PartialView("_Child", model);
            return Json(comm, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Parents(bm_child_parents parents)
        {
            var model = await _childService.InsertOrUpdateChildParents(parents);
            //return PartialView("_Child", model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> ParentsForMobile(bm_child_parents parents)
        {
            var comm = await _childService.InsertOrUpdateChildParentsForMobile(parents);
            //return PartialView("_Child", model);
            return Json(comm, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Info()
        {
            return View();
        }

        [HttpPost]
        public PartialViewResult _MobileInfo(int step = 1)
        {
            object entity = null;
            switch (step)
            {
                case 1:
                    entity = _userService.GetUser(CurrUserId.To<int>());
                    return PartialView("_MobileUser", entity);
                case 2:
                    entity = _childService.GetChild(CurrUserId.To<int>());
                    return PartialView("_MobileChild", entity);
                case 3:
                    entity = _childService.GetParents(CurrUserId.To<int>());
                    return PartialView("_MobileParents", entity);
            }

            return PartialView("_MobileUser", entity);
        }

        [HttpPost]
        public async Task<ActionResult> SaveUserInfo(bm_user user)
        {
            var data = new CommonOutputDto();
            var model = new MobileUserDto
            {
                AlipayNo = user.AlipayNo,
                TaobaoWangWangNo = user.TaobaoWangWangNo,
                TaobaoOrderNo = user.TaobaoOrderNo
            };
            var validator = new MobileUserValidator();
            var result = await validator.ValidateAsync(model);
            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(error =>
                {
                    data.Message += error.ErrorMessage + "\r\n";
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var entity = await _userService.GetUserByMobile(user.Mobile);
                if (entity != null)
                {
                    try
                    {
                        entity.AlipayNo = user.AlipayNo;
                        entity.TaobaoWangWangNo = user.TaobaoWangWangNo;
                        entity.TaobaoOrderNo = user.TaobaoOrderNo;
                        entity.TaobaoOrderTime = user.TaobaoOrderTime;
                        entity.Email = user.Email;
                        entity.FirstRegistrationTime = user.FirstRegistrationTime.HasValue ? user.FirstRegistrationTime.Value : new DateTime?();
                        entity.FirstUrl = user.FirstUrl;
                        entity.SecondRegistrationTime = user.SecondRegistrationTime.HasValue ? user.SecondRegistrationTime : new DateTime?();
                        entity.SecondUrl = user.SecondUrl;
                        entity.VerificationCode = user.VerificationCode;
                        entity.RegistWebAccount = user.RegistWebAccount;
                        entity.RegistWebPassword = user.RegistWebPassword;

                        var userId = _userService.Update(entity);
                        data.IsSuccess = true;
                        data.Url = Url.Action("Info", "Customer");
                    }
                    catch (Exception ex)
                    {
                        data.Message = "信息提交失败，请联系管理员！";
                    }
                }
                else
                    data.Message = "该用户不存在";
            }


            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}