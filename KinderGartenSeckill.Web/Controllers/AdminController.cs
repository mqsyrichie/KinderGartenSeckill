﻿using KinderGartenSeckill.Model;
using KinderGartenSeckill.Service;
using KinderGartenSeckill.Web.Filter;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using KinderGartenSeckill.Dto;

namespace KinderGartenSeckill.Web.Controllers
{
    [AuthFilter(UserType.Actor, AllowUserType = UserType.Admin)]
    public class AdminController : KinderGartenSeckillControllerBase
    {
        private readonly IUserService _userService;
        private readonly IChildService _childService;
        private readonly IParentsService _parentService;
        public AdminController(IUserService userService,
            IChildService childService,
            IParentsService parentService) : base(userService)
        {
            this._userService = userService;
            this._childService = childService;
            this._parentService = parentService;
        }


        // GET: Admin
        public ActionResult Main()
        {
            var model = _userService.GetList();
            var actors = _userService.GetActors();
            ViewBag.Actors = actors;
            return View(model);
        }

        public ActionResult List(string username = null,
            string mobile = null,
            string taobaono = null,
            string garden = null,
            string ftime = null,
            string ttime = null,
            int actorUserId = 0)
        {
            var model = new List<UserListDto>();
            if (CurrUserType == UserType.Actor)
                model = _userService.GetUsers(userId: int.Parse(CurrUserId), username: username, mobile: mobile,
                    taobaono: taobaono, garden: garden,
                    ftime: ftime, ttime: ttime);
            else
                model = _userService.GetUsers(userId: actorUserId, username: username, mobile: mobile,
                    taobaono: taobaono, garden: garden,
                    ftime: ftime, ttime: ttime);
            //if (string.IsNullOrWhiteSpace(username) &&
            //    string.IsNullOrWhiteSpace(mobile) &&
            //    string.IsNullOrWhiteSpace(taobaono) &&
            //    string.IsNullOrWhiteSpace(garden) &&
            //    string.IsNullOrWhiteSpace(ftime) &&
            //    string.IsNullOrWhiteSpace(ttime))
            //{
            //    model = _userService.GetList();
            //}
            //else
            //{
            //    model = _userService.GetUsers(username, mobile, taobaono, garden, ftime, ttime);
            //}

            return PartialView(model);
        }

        public ActionResult SignTime()
        {
            return View();
        }

        public ActionResult Detail(int userId)
        {
            ViewBag.UserId = userId;
            return View();
        }

        public ActionResult _Child(int userId)
        {
            var model = _childService.GetChild(userId);
            return PartialView(model);
        }

        public ActionResult _Parents(int userId)
        {
            var model = _parentService.GetParents(userId);
            return PartialView(model);
        }

        public ActionResult ModifyUser(int userId)
        {
            var model = _userService.GetUser(userId);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "未报上", Value = "false" });
            items.Add(new SelectListItem { Text = "已报上", Value = "true" });
            ViewBag.StatusType = items;

            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ModifyUser(bm_user user)
        {
            var u = _userService.GetUser(user.Id);
            u.UserName = user.UserName;
            u.Mobile = user.Mobile;
            u.Email = user.Email;
            u.FirstKindergarten = user.FirstKindergarten;
            u.FirstUrl = user.FirstUrl;
            u.SecondKindergarten = user.SecondKindergarten;
            u.SecondUrl = user.SecondUrl;
            u.Operator = user.Operator;
            u.Status = user.Status;
            u.VerificationCode = user.VerificationCode;

            var model = _userService.Update(u);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "未报上", Value = "false" });
            items.Add(new SelectListItem { Text = "已报上", Value = "true" });
            ViewBag.StatusType = items;

            //return RedirectToAction("ModifyUser",);
            return View();
        }

        public ActionResult CustomPhrase(int userId)
        {
            bm_child childModel = _childService.GetChild(userId);
            bm_child_parents parentModel = _parentService.GetParents(userId);
            var arr = new ArrayList();
            var result1 = GetClassObj1(childModel);
            var result2 = GetClassObj1(parentModel);
            arr.AddRange(result1);
            arr.AddRange(result2);
            //return PartialView(result);
            return Json(arr, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        public ActionResult Export(int id)
        {
            var userId = id;
            List<string> disNames = new List<string>();

            bm_child childModel = _childService.GetChild(userId);
            bm_child_parents parentModel = _parentService.GetParents(userId);
            bm_user userModel = _userService.GetUser(userId);

            var result = GetClassObj(childModel);
            var folder = $"{userModel?.FirstRegistrationTime.Value.ToString("yyyyMMdd-hhmm")}";
            var file = $"{userModel?.Mobile}-{childModel?.ChildName}.txt";
            string fullpath = Server.MapPath("../../Export/" + folder + "/" + file);

            Encoding code = Encoding.GetEncoding("gb2312");
            StreamWriter sw = null;
            {
                try
                {
                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath(folder));
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }

                    sw = new StreamWriter(fullpath, false, code);
                    sw.Write(result);
                    sw.Flush();

                }
                catch { }
            }
            sw.Close();
            sw.Dispose();
            var url = $"{ ConfigurationManager.AppSettings["website"]}{"Export/" + folder + "/" + file}";
            return Redirect(url);
            //return Json(url,JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 用户分配
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ChildAssignment()
        {
            var operators = await _userService.GetOperators();
            ViewBag.Operators = operators;

            //查询未分配的用户
            var validList = _userService.GetValidUsers();
            return View(validList);
        }

        /// <summary>
        /// 获取已分配的用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetSelectedUsers(int id)
        {
            var users = _userService.GetSelected(id);
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Assignment(int id, List<int> users)
        {
            var result = await _userService.Assignment(id, users);
            return Json(new
            {
                IsSussess = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Js()
        {
            return View();
        }
    }
}