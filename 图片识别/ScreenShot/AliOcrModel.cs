﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;

namespace DemoScreenShot
{
    public class AliOcrModel
    {
        public string image { get; set; }

        public Configure configure { get; set; }
        public class Configure
        {
            public int min_size { get; set; } = 16;

            public bool output_prob { get; set; } = true;
        }
    }

    public class AliOcrReturnModel
    {
        public string request_id { get; set; }

        public bool success { get; set; }

        public List<Ret> ret { get; set; }

        public class Ret
        {
            public string word { get; set; }

        }

    }


    public class AliOcrHelper
    {
        public static string GetBase64FromImage(Bitmap bmp)
        {
            string strbaser64 = "";
            try
            {
                //Bitmap bmp = this.pictureBox1.Image.;
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] arr = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(arr, 0, (int)ms.Length);
                ms.Close();
                strbaser64 = Convert.ToBase64String(arr);
            }
            catch (Exception)
            {
                throw new Exception("Something wrong during convert!");
            }
            return strbaser64;
        }

        public static AliOcrReturnModel GetString(string base64)
        {
            var url = "http://tysbgpu.market.alicloudapi.com/api/predict/ocr_general";
            var appCode = System.Configuration.ConfigurationManager.AppSettings["appCode"].ToString();

            var bodys = JsonConvert.SerializeObject(new AliOcrModel { image = base64 });

            HttpWebRequest httpRequest = null;
            HttpWebResponse httpResponse = null;

            httpRequest = (HttpWebRequest)WebRequest.Create(url);

            httpRequest.Method = "POST";
            httpRequest.Headers.Add("Authorization", "APPCODE " + appCode);
            httpRequest.ContentType = "application/json; charset=UTF-8";
            if (0 < bodys.Length)
            {
                byte[] data = Encoding.UTF8.GetBytes(bodys);
                using (Stream stream = httpRequest.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            try
            {
                httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            }
            catch (WebException ex)
            {
                httpResponse = (HttpWebResponse)ex.Response;
            }

            if (httpResponse.StatusCode != HttpStatusCode.OK)
            {
                //Console.WriteLine("http error code: " + httpResponse.StatusCode);
                //Console.WriteLine("error in header: " + httpResponse.GetResponseHeader("X-Ca-Error-Message"));
                //Console.WriteLine("error in body: ");
                Stream st = httpResponse.GetResponseStream();
                StreamReader reader = new StreamReader(st, Encoding.GetEncoding("utf-8"));
                //Console.WriteLine(reader.ReadToEnd());
                return new AliOcrReturnModel() { success = false };
            }
            else
            {

                Stream st = httpResponse.GetResponseStream();
                StreamReader reader = new StreamReader(st, Encoding.GetEncoding("utf-8"));
                var strResult = reader.ReadToEnd();
                var model = JsonConvert.DeserializeObject<AliOcrReturnModel>(strResult);
                return model;
            }

        }
    }
}
