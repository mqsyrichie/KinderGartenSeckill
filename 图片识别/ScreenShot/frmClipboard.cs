﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DemoScreenShot
{
    public partial class frmClipboard : Form
    {
        public frmClipboard()
        {
            InitializeComponent();
        }

        private void frmClipboard_Load(object sender, EventArgs e)
        {
            nextClipHwnd = SetClipboardViewer(this.Handle);
        }

        private const int WM_DRAWCLIPBOARD = 0x308;

        private const int WM_CHANGECBCHAIN = 0x30D;

        private IntPtr nextClipHwnd;

        [DllImport("user32")]
        private static extern IntPtr SetClipboardViewer(IntPtr hwnd);

        [DllImport("user32")]
        private static extern IntPtr ChangeClipboardChain(IntPtr hwnd, IntPtr hWndNext);

        [DllImport("user32")]
        private static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);


        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_DRAWCLIPBOARD:

                    //将WM_DRAWCLIPBOARD消息传递到下一个观察链中的窗口
                    SendMessage(nextClipHwnd, m.Msg, m.WParam, m.LParam);

                    IDataObject data = Clipboard.GetDataObject();
                    //if (data.GetDataPresent(DataFormats.Text) | data.GetDataPresent(DataFormats.OemText))
                    //{
                    //    this.textBox1.Text = (String)data.GetData(DataFormats.Text);
                    //}
                    //else 
                    if (data.GetDataPresent(DataFormats.Bitmap))
                    {
                        lblMsg.Text = string.Empty;
                        var bitmap = (Bitmap)data.GetData(DataFormats.Bitmap);
                        this.pictureBox1.Image = bitmap;
                        var strbase64 = AliOcrHelper.GetBase64FromImage(bitmap);
                        var model = AliOcrHelper.GetString(strbase64);
                        if (model.success && model.ret != null && model.ret.Any())
                        {
                            var strResult = string.Join(" ", model.ret.Select(p => p.word).ToList());
                            this.textBox1.Text = strResult;
                            textBox1.Select();

                            Clipboard.SetDataObject(strResult);

                        }
                        else
                        {
                            lblMsg.Text = JsonConvert.SerializeObject(model);
                        }
                    }
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        private void frmClipboard_FormClosed(object sender, FormClosedEventArgs e)
        {
            //从观察链中删除本观察窗口
            ChangeClipboardChain(this.Handle, nextClipHwnd);

            //将WM_DRAWCLIPBOARD消息传递到下一个观察链中的窗口 
            SendMessage(nextClipHwnd, WM_CHANGECBCHAIN, this.Handle, nextClipHwnd);
        }
    }
}
