﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinderGartenSeckill.Model
{
    public class bm_user : Entity<int>
    {

        public bm_user()
        {
            CreateDate = DateTime.Now;
        }

        public string LoginName { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string AlipayNo { get; set; }
        public string TaobaoWangWangNo { get; set; }
        public string TaobaoOrderNo { get; set; }
        public string TaobaoOrderTime { get; set; }
        public string FirstKindergarten { get; set; }

        public Nullable<System.DateTime> FirstRegistrationTime { get; set; }

        public string FirstUrl { get; set; }

        public string SecondKindergarten { get; set; }

        public Nullable<System.DateTime> SecondRegistrationTime { get; set; }

        public string SecondUrl { get; set; }

        /// <summary>
        /// 0:管理员 1:注册用户
        /// </summary>
        public int UserType { get; set; }

        public DateTime CreateDate { get; set; }

        public string Remark { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 报名人
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// 报名状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
         public string VerificationCode { get; set; }

        /// <summary>
        /// 报名网站登录账号
        /// </summary>
        public string RegistWebAccount { get; set; }

        /// <summary>
        /// 报名网站登录密码
        /// </summary>
        public string RegistWebPassword { get; set; }

        [ForeignKey("UserId")]
        public virtual ICollection<bm_user_child_mapping> Users { get; set; }
    }
}
