﻿namespace KinderGartenSeckill.Model
{
    public enum UserType
    {
        Admin = 0,
        /// <summary>
        /// 客户
        /// </summary>
        Customer = 1,

        /// <summary>
        /// 参与报名的人
        /// </summary>
        Actor = 2
    }
}
