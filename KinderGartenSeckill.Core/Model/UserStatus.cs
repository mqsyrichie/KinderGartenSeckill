﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Model
{
    public enum UserStatus
    {
        /// <summary>
        /// 已注册
        /// </summary>
        Registered = 0,

        /// <summary>
        /// 已填写幼儿园主信息
        /// </summary>
        Approve = 1
    }
}
