﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;

namespace KinderGartenSeckill.Model
{
    [Table("CityAndDistrict")]
    public class CityAndDistrict : Entity<int>
    {
        public int ParentId { get; set; }

        public int ProvinceId { get; set; }

        public string Name { get; set; }

        public int Ordering { get; set; }

        public bool Status { get; set; }

        public bool Deleted { get; set; }
    }
}
