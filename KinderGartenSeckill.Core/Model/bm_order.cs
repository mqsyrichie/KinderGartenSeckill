﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Model
{
    public class bm_order : Entity<int>
    {
        /// <summary>
        /// 旺旺号
        /// </summary>
        public string TaobaoWangWangNo { get; set; }

        /// <summary>
        /// 淘宝订单号
        /// </summary>
        public string TaobaoOrderNo { get; set; }

        /// <summary>
        /// 订单时间
        /// </summary>
        public string TaobaoOrderTime { get; set; }
    }
}
