﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Model
{
    public class bm_child_parents : Entity<int>
    {

        public bm_child_parents()
        {
            //
        }

    
        public int UserId { get; set; }

        /// <summary>
        /// 父亲姓名
        /// </summary>
        [DisplayName("bb,1")]
        public string BB_Name { get; set; }

        /// <summary>
        /// 爸爸生日
        /// </summary>
        [DisplayName("bbsr,1")]
        public string BB_Birth { get; set; }

        /// <summary>
        /// 父亲手机号
        /// </summary>
        [DisplayName("bbdh,1")]
        public string BB_Mobile { get; set; }

        /// <summary>
        /// 父亲身份证号
        /// </summary>
        [DisplayName("bbsfz,1")]
        public string BB_CardNo { get; set; }

        /// <summary>
        /// 父亲学历
        /// </summary>
        [DisplayName("bbxl,1")]
        public string BB_XueLi { get; set; }

        /// <summary>
        /// 父亲公司
        /// </summary>
        [DisplayName("bbgs,1")]
        public string BB_Company { get; set; }

        /// <summary>
        /// 父亲单位电话
        /// </summary>
        [DisplayName("bbdh,2")]
        public string BB_CompanyTel { get; set; }

        /// <summary>
        /// 父亲邮箱
        /// </summary>
        [DisplayName("bbyx,1")]
        public string BB_Email { get; set; }

        /// <summary>
        /// 父亲年龄
        /// </summary>
        [DisplayName("bbnl,1")]
        public Nullable<int> BB_Age { get; set; }

        /// <summary>
        /// 父亲民族
        /// </summary>
        [DisplayName("bbmz,1")]
        public string BB_MinZu { get; set; }

        /// <summary>
        /// 父亲政治面目
        /// </summary>
        [DisplayName("bbzzmm,1")]
        public string BB_ZhengZhi { get; set; }

        /// <summary>
        /// 父亲家庭住址
        /// </summary>
        [DisplayName("bbdz,1")]
        public string BB_Address { get; set; }

        /// <summary>
        /// 父亲职位
        /// </summary>
        [DisplayName("bbzw,1")]
        public string BB_ZhiWei { get; set; }

        /// <summary>
        /// 妈妈姓名
        /// </summary>
        [DisplayName("mm,1")]
        public string MM_Name { get; set; }

        /// <summary>
        /// 妈妈生日
        /// </summary>
        [DisplayName("mmsr,1")]
        public string MM_Birth { get; set; }

        /// <summary>
        /// 妈妈手机号
        /// </summary>
        [DisplayName("mmdh,1")]
        public string MM_Mobile { get; set; }

        /// <summary>
        /// 妈妈身份证号
        /// </summary>
        [DisplayName("mmsfz,1")]
        public string MM_CardNo { get; set; }

        /// <summary>
        /// 妈妈学历
        /// </summary>
        [DisplayName("mmxl,1")]
        public string MM_XueLi { get; set; }

        /// <summary>
        /// 妈妈公司名称
        /// </summary>
        [DisplayName("mmgs,1")]
        public string MM_Company { get; set; }

        /// <summary>
        /// 妈妈公司电话
        /// </summary>
        [DisplayName("mmdh,2")]
        public string MM_CompanyTel { get; set; }

        /// <summary>
        /// 妈妈邮箱
        /// </summary>
        [DisplayName("mmyx,1")]
        public string MM_Email { get; set; }

        /// <summary>
        /// 妈妈年龄
        /// </summary>
        [DisplayName("mmnl,1")]
        public Nullable<int> MM_Age { get; set; }

        /// <summary>
        /// 妈妈民族
        /// </summary>
        [DisplayName("mmmz,1")]
        public string MM_MinZu { get; set; }

        /// <summary>
        /// 妈妈地址
        /// </summary>
        [DisplayName("mmdz,1")]
        public string MM_Address { get; set; }

        /// <summary>
        /// 妈妈职位
        /// </summary>
        [DisplayName("mmzw,1")]
        public string MM_ZhiWei { get; set; }

        /// <summary>
        /// 妈妈政治面目
        /// </summary>
        [DisplayName("mmzzmm,1")]
        public string MM_ZhengZhi { get; set; }
    }
}
