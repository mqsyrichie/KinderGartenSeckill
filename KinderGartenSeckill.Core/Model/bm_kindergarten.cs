﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;

namespace KinderGartenSeckill.Model
{
    public class bm_kindergarten : Entity<int>
    {
        public int DistrictId { get; set; }

        public string KindergartenName { get; set; }

        public string RegistrationTime { get; set; }

        public string RegistrationUrl { get; set; }

        public bool Status { get; set; }


    }
}
