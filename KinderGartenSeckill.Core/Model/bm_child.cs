﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinderGartenSeckill.Model
{
    public class bm_child : Entity<int>
    {

        public bm_child()
        {
            CreateDate = DateTime.Now;
        }

        
        public int UserId { get; set; }

        /// <summary>
        /// 幼儿姓名
        /// </summary>
        [DisplayName("xm,1")]
        public string ChildName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [DisplayName("xb,1")]
        public string Sex { get; set; }

        /// <summary>
        /// 国籍
        /// </summary>
        [DisplayName("gj,1")]
        public string Guoji { get; set; }

        /// <summary>
        /// 乳名
        /// </summary>
        [DisplayName("rm,1")]
        public string Ruming { get; set; }

        /// <summary>
        /// 幼儿证件号码
        /// </summary>
        [DisplayName("sfz,1")]
        public string ChildCardNo { get; set; }

        /// <summary>
        /// 体重
        /// </summary>
        [DisplayName("tz,1")]
        public string TiZhong { get; set; }

        /// <summary>
        /// 民族
        /// </summary>
        [DisplayName("mz,1")]
        public string MinZu { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        [DisplayName("sg,1")]
        public string ShenGao { get; set; }

        /// <summary>
        /// 出生地
        /// </summary>
        [DisplayName("csd,1")]
        public string ChuShengDi { get; set; }

        /// <summary>
        /// 户口所在地
        /// </summary>
        [DisplayName("hkdz,1")]
        public string HuKouSuoZaiDi { get; set; }

        /// <summary>
        /// 户口性质
        /// </summary>
        [DisplayName("hkxz,1")]
        public string HuKouXingZhi { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        [DisplayName("sr,1")]
        public string Birth { get; set; }

        /// <summary>
        /// 是否独生子女
        /// </summary>
        [DisplayName("dszn,1")]
        public string DuShengZiNv { get; set; }

        /// <summary>
        /// 是否残疾
        /// </summary>
        [DisplayName("cj,1")]
        public string CanJi { get; set; }

        /// <summary>
        /// 现住址
        /// </summary>
        [DisplayName("dz,1")]
        public string Address { get; set; }

        /// <summary>
        /// 出生医院
        /// </summary>
        [DisplayName("csyy,1")]
        public string BirthHospital { get; set; }

        /// <summary>
        /// 出生证号
        /// </summary>
        [DisplayName("cszh,1")]
        public string BirthCardNo { get; set; }

        /// <summary>
        /// 红本儿童编码
        /// </summary>
        [DisplayName("etbm,1")]
        public string ChildNo { get; set; }

        /// <summary>
        /// 免疫编码
        /// </summary>
        [DisplayName("mybm,1")]
        public string MianYiNo { get; set; }

        /// <summary>
        /// 儿童保健手册条形码
        /// </summary>
        [DisplayName("txm,1")]
        public string TiaoXingNo { get; set; }


        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 兴趣爱好
        /// </summary>
        [DisplayName("xq,1")]
        public string XingQu { get; set; }

        /// <summary>
        /// 选择我园的原因
        /// </summary>
        [DisplayName("yy,1")]
        public string Reason { get; set; }

        /// <summary>
        /// 是否有过敏史
        /// </summary>
        [DisplayName("gm,1")]
        public string Guomin { get; set; }

        /// <summary>
        /// 希望孩子在园期间得到哪些方面的关注（培养）
        /// </summary>
        [DisplayName("gz,1")]
        public string Guanzhu { get; set; }

        /// <summary>
        /// 您对我园有什么建议？
        /// </summary>
        [DisplayName("jy,1")]
        public string Jianyi { get; set; }

        /// <summary>
        /// 孩子喜欢（不喜欢）吃的东西
        /// </summary>
        [DisplayName("food,1")]
        public string Food { get; set; }

        /// <summary>
        /// 入园前谁照看孩子
        /// </summary>
        [DisplayName("zkr,1")]
        public string ZhaoKanRen { get; set; }

        /// <summary>
        /// 接送人姓名
        /// </summary>
        [DisplayName("jsxm,1")]
        public string JieSongName { get; set; }

        /// <summary>
        /// 接送人联系电话
        /// </summary>
        [DisplayName("jsdh,1")]
        public string JieSongMobile { get; set; }

        /// <summary>
        /// 接送人与孩子的关系
        /// </summary>
        [DisplayName("jsgx,1")]
        public string JieSongGuanxi { get; set; }

        /// <summary>
        /// 房屋所有权证地址
        /// </summary>
        [DisplayName("fbdz,1")]
        public string FangWuAddress { get; set; }

        /// <summary>
        /// 户口本户主姓名
        /// </summary>
        [DisplayName("hz,1")]
        public string HuKouBenHuZhu { get; set; }

        /// <summary>
        /// 房屋所有权证持证人
        /// </summary>
        [DisplayName("fbxm,1")]
        public string FangBenName { get; set; }

        /// <summary>
        /// 幼儿与户主关系
        /// </summary>
        [DisplayName("hzgx,1")]
        public string HuZhuGuanXi { get; set; }

        /// <summary>
        /// 幼儿与房屋所有权证持证人关系
        /// </summary>
        [DisplayName("fbgx,1")]
        public string FangBenRenGuanXi { get; set; }

        /// <summary>
        /// 所属街道
        /// </summary>
        [DisplayName("jd,1")]
        public string JieDao { get; set; }

        /// <summary>
        /// 所属派出所
        /// </summary>
        [DisplayName("pcs,1")]
        public string PaiChuSuo { get; set; }

        /// <summary>
        /// 独生子女证号
        /// </summary>
        [DisplayName("dszn,1")]
        public string DuShengZiNvZheng { get; set; }

        /// <summary>
        /// 户口本户籍号
        /// </summary>
        [DisplayName("hjh,1")]
        public string HuJiHao { get; set; }

        /// <summary>
        /// 监护人姓名
        /// </summary>
        [DisplayName("jhxm,1")]
        public string JianHuName { get; set; }

        /// <summary>
        /// 监护人身份证号
        /// </summary>
        [DisplayName("jhsfz,1")]
        public string JianHuCardNo { get; set; }

        /// <summary>
        /// 监护人邮箱
        /// </summary>
        [DisplayName("jhyx,1")]
        public string JianHuEmail { get; set; }

        /// <summary>
        /// 监护人性别
        /// </summary>
        [DisplayName("jhxb,1")]
        public string JianHuSex { get; set; }

        /// <summary>
        /// 监护人学历
        /// </summary>
        [DisplayName("jhxl,1")]
        public string JianHuXueLi { get; set; }

        /// <summary>
        /// 监护人政治面目
        /// </summary>
        [DisplayName("jhzz,1")]
        public string JianHuZhengZhi { get; set; }

        /// <summary>
        /// 监护人电话
        /// </summary>
        [DisplayName("jhdh,1")]
        public string JianHuMobile { get; set; }

        /// <summary>
        /// 监护人公司
        /// </summary>
        [DisplayName("jhgs,1")]
        public string JianHuCompany { get; set; }

        /// <summary>
        /// 监护人职位
        /// </summary>
        [DisplayName("jhzw,1")]
        public string JianHuZhiWei { get; set; }

        /// <summary>
        /// 监护人与幼儿关系
        /// </summary>
        [DisplayName("jhgx,1")]
        public string JianHuGuanXi { get; set; }

        /// <summary>
        /// 血型
        /// </summary>
        [DisplayName("xx,1")]
        public string XueXing { get; set; }
    }
}
