﻿using Abp.Domain.Entities;

namespace KinderGartenSeckill.Model
{
    public class bm_user_child_mapping : Entity<int>
    {
        public int UserId { get; set; }

        public int ChildUserId { get; set; }
    }
}
