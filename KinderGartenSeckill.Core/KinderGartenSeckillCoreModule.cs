﻿using System.Reflection;
using Abp.Modules;

namespace KinderGartenSeckill
{
    public class KinderGartenSeckillCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
